<?php

namespace App\Exports;

use Illuminate\Database\Eloquent\Builder;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;


class PaymentInvoiceExport implements FromCollection, WithHeadings, WithMapping, WithColumnFormatting
{
    use Exportable;

    private Builder $paymentInvoice;

    public function __construct(Builder $paymentInvoice)
    {

        $this->paymentInvoice = $paymentInvoice;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return $this->paymentInvoice
            ->orderBy('created_at', 'desc')
            ->get();
    }

    public function map($invoice): array
    {
        return [
            $invoice->id,
            $invoice->merchant_order_id,
            Date::dateTimeToExcel($invoice->created_at),
            price_format($invoice->amount),
            price_format($invoice->amount2pay),
            $invoice->input_currency != 'USD' ? round($invoice->input_amount_value, 2) : null,
            $invoice->input_currency != 'USD' ? strtoupper($invoice->input_currency) : null,
            invoice_status($invoice->status)
        ];
    }

    public function headings(): array
    {
        return ["invoice_id", "order_id", "date", "amount", "amount2pay", "fiat_amount", "fiat_currency", "status"];
    }

    public function columnFormats(): array
    {
        return [
            'C' => 'yyyy-mm-dd hh:mm:ss'
        ];
    }
}
