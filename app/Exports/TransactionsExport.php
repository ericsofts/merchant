<?php

namespace App\Exports;

use App\Repositories\TransactionRepository;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;


class TransactionsExport implements FromCollection, WithHeadings, WithMapping, WithColumnFormatting
{
    use Exportable;

    private TransactionRepository $transactionRepository;
    private array $conditions;
    private $partner;

    public function __construct(TransactionRepository $transactionRepository, array $conditions = [])
    {

        $this->transactionRepository = $transactionRepository;
        $this->conditions = $conditions;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return $this->transactionRepository->transactions($this->conditions)
            ->orderBy('created_at', 'desc')
            ->get();
    }

    public function map($invoice): array
    {
        return [
            $invoice->id,
            $invoice->merchant_order_id,
            invoice_type($invoice->invoice_type),
            Date::dateTimeToExcel($invoice->created_at),
            price_format($invoice->amount),
            $invoice->invoice_type == 1 ? -price_format($invoice->amount) : $invoice->amount,
            $invoice->invoice_type == 1 ? -price_format($invoice->amount2pay) : $invoice->amount2pay,
            invoice_status($invoice->status)
        ];
    }

    public function headings(): array
    {
        return ["invoice_id", "order_id", "type", "date", "volume", "amount", "amount2pay", "status"];
    }

    public function columnFormats(): array {
        return [
            'D' => 'yyyy-mm-dd hh:mm:ss'
        ];
    }
}
