<?php

namespace App\Console\Commands;

use App\Models\Balance;
use App\Models\BalanceHistory;
use App\Models\BalanceQueue as BalanceQueueModel;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class BalanceQueue extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'balance_queue:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check balance queue';

    protected $isSleep = false;


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

    }

    /**
     * Execute the console command.
     *
     */
    public function handle()
    {
        $this->info('Start BalanceQueue');
        while (true) {
            $this->_main();
        }
    }


    private function _main()
    {

        $balanceQueue = BalanceQueueModel::getBalanceQueueWaiting();

        if (!$balanceQueue) {
            sleep(config('app.balance_queue_sleep_time'));
            if (!$this->isSleep) {
                $this->info('Sleep');
                $this->isSleep = true;
            }

            return;
        }
        $this->isSleep = false;
        $this->info('Working');
        $message = __('Start executing a request from the queue [id = :id transaction_id = :transaction_id]', [
            'id' => $balanceQueue->id, 'transaction_id' => $balanceQueue->transaction_id
        ]);
        Log::channel('balance_queue')->info('--------------------------------------------------------');
        Log::channel('balance_queue')->info($message);

        try {
            DB::transaction(function () use ($balanceQueue) {
                Log::channel('balance_queue')->info(__('Opening a transaction'));
                $balanceQueue->status = BalanceQueueModel::STATUS_PROCESSING;
                $balanceQueue->save();

                $message = __('Status of the request changed to STATUS_PROCESSING(2). [id = :id transaction_id = :transaction_id]', [
                    'id' => $balanceQueue->id, 'transaction_id' => $balanceQueue->transaction_id
                ]);
                Log::channel('balance_queue')->info($message);

                $balance = Balance::firstOrCreate(
                    ['user_id' => $balanceQueue->user_id]
                );
                $message = __('User balance [user_id = :user_id id = :id transaction_id = :transaction_id balance = :balance]', [
                    'user_id' => $balanceQueue->user_id, 'id' => $balanceQueue->id, 'transaction_id' => $balanceQueue->transaction_id, 'balance' => $balance->amount
                ]);
                Log::channel('balance_queue')->info($message);
                $balance->amount += $balanceQueue->amount;
                $balance->save();
                $message = __('Update user balance. [user_id = :user_id amount = :amount id = :id transaction_id = :transaction_id]', [
                    'user_id' => $balanceQueue->user_id, 'amount' => $balance->amount, 'id' => $balanceQueue->id, 'transaction_id' => $balanceQueue->transaction_id]);
                Log::channel('balance_queue')->info($message);

                $balance_history = BalanceHistory::create([
                    'transaction_id' => $balanceQueue->transaction_id,
                    'user_id' => $balanceQueue->user_id,
                    'merchant_id' => $balanceQueue->merchant_id,
                    'input_invoice_id' => $balanceQueue->input_invoice_id,
                    'payment_invoice_id' => $balanceQueue->payment_invoice_id,
                    'amount' => $balanceQueue->amount,
                    'type' => $balanceQueue->type,
                    'description' => $balanceQueue->description
                ]);
                $message = __('Update balance history. [id = :id transaction_id = :transaction_id  user_id = :user_id type = :type  amount = :amount description = :description]', [
                    'user_id' => $balanceQueue->user_id, 'amount' => $balanceQueue->amount, 'id' => $balanceQueue->id, 'transaction_id' => $balanceQueue->transaction_id, 'type' => $balanceQueue->type, 'description' => $balanceQueue->description]);
                Log::channel('balance_queue')->info($message);

                $balanceQueue->status = BalanceQueueModel::STATUS_DONE;
                $balanceQueue->done = date('Y-m-d H:i:s');
                $balanceQueue->save();
                $message = __('Status of the request changed to STATUS_DONE(1). [id = :id transaction_id = :transaction_id]', [
                    'id' => $balanceQueue->id, 'transaction_id' => $balanceQueue->transaction_id]);
                Log::channel('balance_queue')->info($message);

                Log::channel('balance_queue')->info('--------------------------------------------------------');

            }, 3);
        } catch (\Exception $exception) {
            $message = __('An error occurred while executing a request from the queue. [id = :id transaction_id = :transaction_id]', [
                'id' => $balanceQueue->id, 'transaction_id' => $balanceQueue->transaction_id]);
            Log::channel('balance_queue')->info($message);
            Log::channel('balance_queue')->info($exception->getMessage());

            $balanceQueue->status = BalanceQueueModel::STATUS_FAILED;
            $balanceQueue->save();
            $message = __('Status of the request changed to STATUS_FAILED(99). [id = :id transaction_id = :transaction_id]', [
                'id' => $balanceQueue->id, 'transaction_id' => $balanceQueue->transaction_id]);
            Log::channel('balance_queue')->info($message);
            Log::channel('balance_queue')->info('--------------------------------------------------------');
        }
        unset($balanceQueue);
    }
}
