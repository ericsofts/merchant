<?php

namespace App\Observers;


use App\Models\Trader;
use App\Models\TraderAd;
use App\Models\TraderBalance;
use App\Models\TraderBalanceQueue;
use App\Models\TraderCurrency;
use App\Models\TraderDeal;
use App\Models\TraderDealHistory;

class TraderDealObserver
{

    private $history = null;

    public function updating(TraderDeal $traderDeal)
    {
        // @TODO история создаётся даже если статус не сменился при ошибке
        if ($traderDeal->isDirty('status')) {
            $this->history = TraderDealHistory::create([
                'deal_id' => $traderDeal->id,
                'customer_id' => $traderDeal->customer_id ?? NULL,
                'customer_type' => $traderDeal->customer_type ?? NULL,
                'status' => $traderDeal->status,
                'description' => $traderDeal->description ?? 'change status deal',
            ]);
            $traderDeal->last_history_id = $this->history->id;
            unset($traderDeal->description);
            unset($traderDeal->customer_id);
            unset($traderDeal->customer_type);
            switch ($traderDeal->status){
                case TraderDeal::STATUS_TRADER_FUNDED:
                    $result = $this->changeDial($traderDeal, 'Funded Deal', 'funded');
                    if(!$result){
                        throw new \Exception("Balance amount error");
                    }
                    break;
                case TraderDeal::STATUS_COMPLETED:
                    $this->changeDial($traderDeal, 'Complete Deal', 'complete');
                    break;
                case TraderDeal::STATUS_CANCELED :
                case TraderDeal::STATUS_CANCELED_SD :
                case TraderDeal::STATUS_CANCELED_TIMEOUT :
                case TraderDeal::STATUS_CANCELED_TRADER :
                case TraderDeal::STATUS_CANCELED_USER :
                    $this->changeDial($traderDeal, 'Cancel Deal', 'cancel');
                    break;
                case TraderDeal::STATUS_CANCELED_BY_REPLY_TIMEOUT :
                    $this->changeDial($traderDeal, 'Cancel Deal', 'cancel');
                    $this->cancelAdsTrader($traderDeal->trader_id);
                    break;
            }
        }
    }

    public function creating(TraderDeal $traderDeal)
    {
        $this->history = TraderDealHistory::create([
            'deal_id' => 0,
            'status' => $traderDeal->status,
            'customer_id' => $traderDeal->customer_id ?? NULL,
            'customer_type' => $traderDeal->customer_type ?? NULL,
            'description' => $traderDeal->description ?? 'create deal',
        ]);
        $traderDeal->last_history_id = $this->history->id;
        unset($traderDeal->description);
        unset($traderDeal->customer_id);
        unset($traderDeal->customer_type);
    }

    public function created(TraderDeal $traderDeal)
    {
        $traderHistory = TraderDealHistory::find($traderDeal->last_history_id);
        $traderHistory->deal_id = $traderDeal->id;
        $traderHistory->save();
    }

    private function changeDial($traderDeal, $desc, $type): bool
    {
        $currency = TraderCurrency::all()->pluck('asset', 'id');
        $trader = Trader::find($traderDeal->trader_id);
        $totalNormalBalance = $trader->getTotalBalance($traderDeal->currency_id, TraderBalance::TYPE_NORMAL);

        switch ($type){
            case 'complete':
                if($traderDeal->type == TraderDeal::TYPE_SELL){
                    $balance = $this->getBalance($traderDeal, $currency, TraderBalance::TYPE_FROZEN);
                    $balanceAmount = $balance->amount - $traderDeal->amount_currency;
                    $this->addBalanceQueue($traderDeal, $balanceAmount, $balance, TraderBalance::TYPE_FROZEN, $desc, TraderBalanceQueue::TYPE_DEBIT);
                }elseif ($traderDeal->type == TraderDeal::TYPE_BUY){
                    $balance = $this->getBalance($traderDeal, $currency, TraderBalance::TYPE_NORMAL);
                    $balanceAmount = $balance->amount + $traderDeal->amount_currency;
                    $this->addBalanceQueue($traderDeal, $balanceAmount, $balance, TraderBalance::TYPE_NORMAL, $desc, TraderBalanceQueue::TYPE_CREDIT);
                }
                break;
            case 'cancel':
                if($traderDeal->type == TraderDeal::TYPE_SELL){
                    if(TraderDealHistory::where(['deal_id' => $traderDeal->id, 'status' => TraderDeal::STATUS_TRADER_FUNDED])->first()){
                        $balance = $this->getBalance($traderDeal, $currency, TraderBalance::TYPE_FROZEN);
                        $balanceAmount = $balance->amount - $traderDeal->amount_currency;
                        $this->addBalanceQueue($traderDeal, $balanceAmount, $balance, TraderBalance::TYPE_FROZEN, $desc, TraderBalanceQueue::TYPE_DEBIT);
                        $balance = $this->getBalance($traderDeal, $currency, TraderBalance::TYPE_NORMAL);
                        $balanceAmount = $balance->amount + $traderDeal->amount_currency;
                        $this->addBalanceQueue($traderDeal, $balanceAmount, $balance, TraderBalance::TYPE_NORMAL, $desc, TraderBalanceQueue::TYPE_CREDIT);
                    }
                }
                break;
            case 'funded':
                if($traderDeal->type == TraderDeal::TYPE_SELL){
                    $balance = $this->getBalance($traderDeal, $currency, TraderBalance::TYPE_NORMAL);
                    if($totalNormalBalance < $traderDeal->amount_currency){
                        return false;
                    }
                    $balanceAmount = $balance->amount - $traderDeal->amount_currency;
                    $this->addBalanceQueue($traderDeal, $balanceAmount, $balance, TraderBalance::TYPE_NORMAL, $desc, TraderBalanceQueue::TYPE_DEBIT);
                    $balance = $this->getBalance($traderDeal, $currency, TraderBalance::TYPE_FROZEN);
                    $balanceAmount = $balance->amount + $traderDeal->amount_currency;
                    $this->addBalanceQueue($traderDeal, $balanceAmount, $balance, TraderBalance::TYPE_FROZEN, $desc, TraderBalanceQueue::TYPE_CREDIT);
                }
                break;
        }
        return true;
    }

    private function getBalance($traderDeal, $currency, $type){
        return TraderBalance::firstOrCreate([
            'trader_id' => $traderDeal->trader_id,
            'type' => $type,
            'currency_id' => $traderDeal->currency_id,
            'currency' => $currency[$traderDeal->currency_id]
        ]);
    }

    private function addBalanceQueue($traderDeal, $amount, $balance, $typeBalance, $desc, $type){
        TraderBalanceQueue::create([
            'amount' => $traderDeal->amount_currency,
            'balance' => $amount,
            'balance_id' => $balance->id,
            'balance_type' => $typeBalance,
            'currency' => $traderDeal->currency->asset,
            'currency_id' => $traderDeal->currency_id,
            'description' => $desc,
            'invoice_id' => $traderDeal->id,
            'invoice_type' => TraderBalanceQueue::DEAL,
            'status' => TraderBalanceQueue::STATUS_WAITING,
            'trader_id' => $traderDeal->trader_id,
            'type' => $type,
        ]);
    }

    private function cancelAdsTrader($traderDealId){
        $traderAds = TraderAd::where('trader_id', '=', $traderDealId)->where('status', '=', TraderAd::STATUS_ACTIVE);
        $traderAds->update(['status' => TraderAd::STATUS_INACTIVE]);
    }
}
