<?php

namespace App\Observers;

use App\Models\MerchantProperty;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;


class MerchantPropertyObserver
{
    public function updating(MerchantProperty $property)
    {
        Log::channel('merchant_properties')->info('UPDATE|MERCHANT_ID' . Auth::user()->id, [
            'before' => $property->getOriginal(),
            'after' => $property,
            'different' => $property->getDirty(),
        ]);
    }

}
