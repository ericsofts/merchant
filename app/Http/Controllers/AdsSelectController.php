<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Libs\Api\AdsSelectAvailable;

class AdsSelectController extends Controller
{

    public function index(Request $request)
    {
        $merchant = $request->user();
        $ads = (new AdsSelectAvailable)->get(['id' => $merchant->id]);
        return view('available_ads.index', [
            'ads' => $ads
        ]);
    }

}
