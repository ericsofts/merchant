<?php

namespace App\Http\Controllers;

use App\Exports\PaymentInvoiceExport;
use App\Lib\Chatex\ChatexApiLBC;
use App\Models\Merchant;
use App\Models\MerchantCommission;
use App\Models\MerchantProperty;
use App\Models\PaymentInvoice;
use App\Models\PaymentSystemType;
use App\Models\Property;
use App\Models\TraderAd;
use App\Models\TraderDeal;
use App\Models\TraderDealMessage;
use App\Services\AdsSelector;
use App\Services\ContractManager;
use App\Traits\Contract;
use App\Traits\Invoice;
use BaconQrCode\Renderer\Image\ImagickImageBackEnd;
use BaconQrCode\Renderer\ImageRenderer;
use BaconQrCode\Renderer\RendererStyle\RendererStyle;
use BaconQrCode\Writer;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use NumberFormatter;


class PaymentInvoiceController extends BaseController
{

    use Invoice;
    use Contract;

    public function index(Request $request)
    {
        $limit = config('app.default.pagination_limit');

        $from_date = $request->input('from_date');
        $to_date = $request->input('to_date');
        $status = $request->input('status');

        $invoicesQuery = PaymentInvoice::where([
            'merchant_id' => $request->user()->id
        ])
            ->orderBy('id', 'desc');

        if (!empty($from_date)) {
            try {
                $from_date = Carbon::parse($from_date, config('app.default.timezone'));
                $from_date = $from_date->timezone('UTC');
            } catch (Exception $e) {
                $from_date = null;
            }
        }
        if (!empty($to_date)) {
            try {
                $to_date = Carbon::parse($to_date, config('app.default.timezone'));
                $to_date = $to_date->timezone('UTC');
            } catch (Exception $e) {
                $to_date = null;
            }
        }
        if ($from_date) {
            $invoicesQuery->where('created_at', '>=', $from_date);
        }

        if ($to_date) {
            $invoicesQuery->where('created_at', '<=', $to_date);
        }
        if (!is_null($status) && in_array($status, [
                PaymentInvoice::STATUS_PAYED,
                PaymentInvoice::STATUS_CREATED,
                PaymentInvoice::STATUS_CANCELED,
                PaymentInvoice::STATUS_CANCELED_BY_TIMEOUT
            ])) {
            $invoicesQuery->where('status', '=', $status);
        }

        if($request->has('btnExport')){
            return (new PaymentInvoiceExport($invoicesQuery))->download('invoices.xlsx');
        }

        $invoices = $invoicesQuery->paginate($limit)->withQueryString();

        $total = PaymentInvoice::where([
            'merchant_id' => $request->user()->id,
            'status' => PaymentInvoice::STATUS_PAYED
        ])
            ->select(
                DB::raw('SUM(amount) as total_amount'),
                DB::raw('SUM(amount2pay) as total_amount2pay')
            )
            ->first();

        $fmt = new NumberFormatter('en_US', NumberFormatter::CURRENCY);

        return view('payment_invoice.index', [
            'status' => $status,
            'from_date' => $from_date ? $from_date->timezone(config('app.default.timezone')) : null,
            'to_date' => $to_date ? $to_date->timezone(config('app.default.timezone')) : null,
            'invoices' => $invoices,
            'fmt' => $fmt,
            'total' => $total,
            'total_page' => [
                'amount' => $invoices->getCollection()->where('status', '=', PaymentInvoice::STATUS_PAYED)->sum('amount'),
                'amount2pay' => $invoices->getCollection()->where('status', '=', PaymentInvoice::STATUS_PAYED)->sum('amount2pay')
            ]
        ]);
    }

    public function addPayment(Request $request){
        $ps = Property::getProperty('system', 0, 'available_payment_systems');
        $ps = json_decode($ps, true);
        return view('payment_invoice.add', [
            'ps' => $ps ?? []
        ]);
    }

    public function checkoutPayment(Request $request){
        $rules = [
            'amount' => 'required|numeric|gt:0|regex:/^\d+(\.\d{1,2})?$/',
            'payment_method' => 'required'
        ];

        $validator = Validator::make($request->input(), $rules, [], [
            'amount' => __('amount'),
            'payment_method' => __('payment method')
        ]);

        if ($validator->fails()) {
            return redirect()->route('payment_invoice.add')
                ->withErrors($validator)
                ->withInput();
        }
        $payment_method = $request->input('payment_method');
        $amount = $request->input('amount');

        $merchant = $this->getCurrentMerchant();
        $checkAmount = $this->checkMinInvoiceAmount($merchant, $request->input('amount'));
        if(!$checkAmount['status']){
            return redirect()->route('payment_invoice.add')->with('status', $checkAmount['message']);
        }

        if ($payment_method == ChatexApiLBC::PM_NAME) {
            if (!$available_coins = $this->getAvailableMerchantCoins($merchant->id)) {
                return redirect()->route('payment_invoice.add')->with('status',
                    __('Whoops! Something went wrong. Contact Support')
                );
            }

            $rates = $this->getCoinRates($available_coins);
            if (!$rates) {
                return redirect()->route('payment_invoice.add')->with('status',
                    __('Whoops! Something went wrong. Contact Support')
                );
            }

//            $ad_list = $sell_coins = $this->getAdList($available_coins);
            $service_provider = $merchant->default_service_provider();
            $fee = $this->merchantModel->getFeeCommission($merchant->id, $amount, PaymentInvoice::API_TYPE_3, MerchantCommission::PROPERTY_TYPE_COMMISSION, $service_provider->id);
            $merchantAmount =  $amount - $fee['total'];

            $currency = 'USD';
            $currency2currency = false;
            $expense = false;

            $sell_coins = $this->adsService->getAdsByMerchant($merchant, [
                'amount' => $amount,
                'amount2pay' => $merchantAmount,
                'currency' => $currency,
                'input_amount' => floatval($request->input('amount')),
                'currency2currency' => $currency2currency,
                'expense' => $expense,
                'type' => TraderAd::TYPE_SELL,
                'api' => PaymentInvoice::API_TYPE_3
            ]);

            if(!$sell_coins){
                return redirect()->route('payment_invoice.add')->with('status',
                    __('There are no payment systems available.')
                );
            }

            $currencies = array_keys($sell_coins);

            return view('payment_invoice.checkout', [
                'currencies' => $currencies,
                'sell_coins' => $sell_coins,
                'amount' => $amount,
                'fmt' => new NumberFormatter('en_US', NumberFormatter::CURRENCY),
                'merchant_amount' => $merchantAmount
            ]);
        }
        return redirect()->route('payment_invoice.add')->with('status',
            __('There are no payment systems available')
        );
    }

    public function checkoutConfirm(Request $request){
        $data['amount'] = $request->input('amount');
        $data['payment_system_id'] = $request->input('payment_system_id');
        $data['currency'] = $request->input('currency');

        $rules = [
            'payment_system_id' => 'required|string',
            'currency' => 'required',
            'amount' => 'required|numeric|gt:0|regex:/^\d+(\.\d{1,2})?$/'
        ];

        $validator = Validator::make($data, $rules, [], [
            'currency' => __('currency'),
            'payment_system_id' => __('payment system')
        ]);

        if ($validator->fails()) {
            $request->merge(['payment_method' => ChatexApiLBC::PM_NAME]);
            return redirect()->route('payment_invoice.checkout', [ChatexApiLBC::PM_NAME])
                ->withErrors($validator)
                ->withInput();
        }

        $merchant = $this->getCurrentMerchant();

        $checkAmount = $this->checkMinInvoiceAmount($merchant, $request->input('amount'));
        if(!$checkAmount['status']){
            return back()->with('status', $checkAmount['message']);
        }

        $ad = $this->adsService->getAd($data['payment_system_id']);
        if(!$ad || $ad['type'] != 'SELL'){
            return back()->with('status', 'Error AD');
        }

        $available_coins = $this->getAvailableMerchantCoins($merchant->id);
        if (!$this->isAvailableCoins($ad, $available_coins)) {
            Log::channel($ad['log'])->info('AD|COIN|ERROR|MERCHANT_ID|' . $merchant->id, ['ad' => $ad, 'available_coins' => $available_coins]);
            return back()->with('status', __('Whoops! Something went wrong. Contact Support'));
        }
        $coin_rate = $this->getCoinRate(strtolower($ad['coin']));
        if (!$this->isAvalableCoinRate($ad)) {
            Log::channel($ad['log'])->info('AD|COIN_RATE|ERROR|MERCHANT_ID|' . $merchant->id, ['ad' => $ad, 'coin_rate' => $coin_rate]);
            return back()->with('status', __('Whoops! Something went wrong. Contact Support'));
        }

        $coin_amount = $data['amount'] * $coin_rate;
        $rounding_input_amount = MerchantProperty::getProperty($merchant->id, 'rounding_input_amount');
        if ($rounding_input_amount) {
            $currency_amount = ceil($ad['temp_price'] * $coin_amount);
        } else {
            $currency_amount = round($ad['temp_price'] * $coin_amount, 2);
        }

        $contract = $this->contractManager->contractCreate($ad, $currency_amount, $merchant);

        if ($contract) {
            $contractInfo = $this->contractManager->contractInfoByContract($contract);

            if ($contractInfo) {
                $fee = $this->merchantModel->getFeeCommission(
                    $merchant->id,
                    $data['amount'],
                    PaymentInvoice::API_TYPE_3,
                    MerchantCommission::PROPERTY_TYPE_COMMISSION,
                    $ad['service_provider_id']
                );
                $amount2pay = $data['amount'] - $fee['total'];

                $ps_amount = $contractInfo['amount_btc'] ?? null;
                $account_info = $contractInfo['account_info'] ?? null;
                if(!$account_info){
                    $account_info = $ad['account_info'] ?? null;
                }

                $ps_currency = strtolower($ad['coin']) ?? null;
                $invoice = PaymentInvoice::create([
                    'invoice_number' => PaymentInvoice::generateOrderNumber(),
                    'merchant_id' => $merchant->id,
                    'isMerchant' => true,
                    'amount' => $data['amount'],
                    'amount2pay' => $amount2pay,
                    'amount2service' => $fee['service']['amount'],
                    'amount2agent' => $fee['agent']['amount'],
                    'amount2grow' => $fee['grow']['amount'],
                    'commission_grow' => $fee['grow']['commission'],
                    'commission_agent' => $fee['agent']['commission'],
                    'commission_service' => $fee['service']['commission'],
                    'payment_id' => $contractInfo['id'],
                    'ps_amount' => $ps_amount,
                    'ps_currency' => $ps_currency,
                    'fiat_amount' => $contractInfo['amount'],
                    'fiat_currency' => strtolower($contractInfo['currency']),
                    'addition_info' => json_encode([
                        'contact' => $contractInfo,
                        'ad' => $ad
                    ]),
                    'account_info' => $account_info,
                    'service_provider_id' => $ad['service_provider_id'],
                    'status' => PaymentInvoice::STATUS_USER_SELECTED,
                    'api_type' => PaymentInvoice::API_TYPE_0
                ]);

                return redirect()->route('payment_invoice.apply', $invoice->id);

            }
        }
        return back()->with('status',
            __('Whoops! Something went wrong.')
        );
    }

    public function checkoutApply(Request $request, $payment_invoice_id){
        $invoice = PaymentInvoice::where([
            'id' => $payment_invoice_id,
            'isMerchant' => true,
            'merchant_id' => $request->user()->id,
        ])
            ->whereIn('status', [PaymentInvoice::STATUS_CREATED, PaymentInvoice::STATUS_USER_SELECTED, PaymentInvoice::STATUS_TRADER_CONFIRMED])
            ->first();

        if (!$invoice) {
            return redirect()->route('payment_invoice.checkout')->with('status',
                __('Whoops! Something went wrong.')
            );
        }

        $contractInfo = $this->contractManager->contractInfoByProvider(['id' => $invoice['payment_id'], 'service_provider_id' => $invoice['service_provider_id']]);
//        $contact_res = $this->chatex->contactInfo($invoice['payment_id']);
        $deal_accepted = false;

        if ($contractInfo) {
            if ($this->isContractCancel($contractInfo, $invoice)) {
                return redirect()->route('payment_invoice.checkout')->with('status',
                    __('Payment canceled')
                );
            }

            if ($this->isContractComplete($contractInfo, $invoice)) {
                return redirect()->route('payment_invoice.checkout')->with('status',
                    __('Payment closed successfully')
                );
            }

            if ($this->isContractClosed($contractInfo, $invoice)) {
                return redirect()->route('payment_invoice.checkout')->with('status',
                    __('Payment closed')
                );
            }

            if (!empty($contractInfo['funded_at'])) {
                try {
                    $expired_at = Carbon::parse($contractInfo['funded_at']);
                    if(isset($contractInfo['expired_at']) && $contractInfo['expired_at']){
                        $expired_at = $contractInfo['expired_at'];
                    }else{
                        $expired_at = $expired_at->addSeconds(config('app.chatex_deal_expiry'));
                    }

                    $expired = now()->diffInSeconds($expired_at, false);
                    if ($expired < 0) {
                        $expired = 0;
                    }
                } catch (Exception $e) {
                    $expired = 0;
                }
                $deal_accepted = true;
            }
        }

        $addition_info = json_decode($invoice['addition_info'], true);
        return view('payment_invoice.apply', [
            'id' => $invoice->id,
            'deal_accepted' => $deal_accepted,
            'amount' => $contractInfo['amount'],
            'address' => $contractInfo['address'] ?? null,
            'account_info' => $invoice->account_info ?? null,
            'clean_card_number' => $contractInfo['account_info'] ?? null,
            'bank_name' => $addition_info['ad']['bank_name'] ?? null,
            'currency' => $addition_info['ad']['currency'] ?? null,
            'payment_system_type' => $addition_info['ad']['payment_system_type'] ?? PaymentSystemType::CARD_NUMBER,
            'expired' => $expired ?? 0
        ]);

    }

    public function checkoutStatusCheck(Request $request){
        $result = ['status' => 0];
        $invoice_id = $request->input('id');
        $invoice = PaymentInvoice::where([
            'id' => $invoice_id,
        ])
            ->whereIn('status', [
                PaymentInvoice::STATUS_PAYED,
                PaymentInvoice::STATUS_CREATED,
                PaymentInvoice::STATUS_CANCELED,
                PaymentInvoice::STATUS_CANCELED_BY_TIMEOUT,
                PaymentInvoice::STATUS_TRADER_CONFIRMED,
                PaymentInvoice::STATUS_USER_CONFIRMED,
                PaymentInvoice::STATUS_USER_SELECTED,
            ])
            ->first();

        if ($invoice) {
            if($invoice->status == PaymentInvoice::STATUS_CANCELED){
                $result['status'] = -1;
            }else{
                $contractInfo = $this->contractManager->contractInfoByProvider(['id' => $invoice['payment_id'], 'service_provider_id' => $invoice['service_provider_id']]);
                if ($this->isContractCancel($contractInfo)) {
                    $result['status'] = -1;
                }
                if (!empty($contractInfo['funded_at']) && !$this->isContractCancel($contractInfo)) {
                    try {
                        $expired_at = Carbon::parse($contractInfo['funded_at']);
                        if(isset($contractInfo['expired_at']) && $contractInfo['expired_at']){
                            $expired_at = $contractInfo['expired_at'];
                        }else{
                            $expired_at = $expired_at->addSeconds(config('app.chatex_deal_expiry'));
                        }
                        $expired = now()->diffInSeconds($expired_at, false);
                        if ($expired < 0) {
                            $expired = 0;
                        }
                    } catch (Exception $e) {
                        $expired = null;
                    }
                    $result['status'] = 1;
                    $result['expired'] = $expired;
                    $addition_info = json_decode($invoice['addition_info'], true);
                    if(isset($addition_info['ad']['payment_system_type']) && $addition_info['ad']['payment_system_type'] == PaymentSystemType::CRYPTO){
                        $result['address_amount'] = $contractInfo['address_amount'];
                    }
                    if($contractInfo['account_info'] ?? null){
                        $payment_system_type = $addition_info['ad']['payment_system_type'] ?? PaymentSystemType::CARD_NUMBER;
                        if($payment_system_type == PaymentSystemType::IBAN || $payment_system_type == PaymentSystemType::CARD_NUMBER){
                            $result['account_info'] = implode('<span>&nbsp;</span>', str_split($contractInfo['account_info'], 4));
                        }else{
                            $result['account_info'] = $contractInfo['account_info'];
                        }
                    }
                }
                if ($this->isContractComplete($contractInfo)) {
                    $addition_info = json_decode($invoice['addition_info'], true);
                    if(isset($addition_info['ad']['payment_system_type']) && $addition_info['ad']['payment_system_type'] == PaymentSystemType::CRYPTO){
                        $result['status'] = 2;
                    }
                }
            }
        }
        return response()->json($result);
    }

    public function checkoutStatus(Request $request){
        $rules = [
            'id' => 'required|numeric',
            'action' => 'required|numeric',
            'document' => 'nullable|image|mimes:jpeg,png,jpg,gif'
        ];

        $validator = Validator::make($request->only(['id', 'action', 'document']), $rules);

        if ($validator->fails()) {
            return back()->withErrors($validator);
        }

        $invoice = PaymentInvoice::where([
            'id' => $request->input('id'),
            'status' => PaymentInvoice::STATUS_TRADER_CONFIRMED
        ])->first();

        if (!$invoice) {
            return redirect()->route('payment_invoice.add')->with('status',
                __('Whoops! Something went wrong.')
            );
        }

        $contractInfo = $this->contractManager->contractInfoByProvider(['id' => $invoice['payment_id'], 'service_provider_id' => $invoice['service_provider_id']]);
        $channel = $this->getLogChannel($invoice);

        if ($contractInfo) {
            Log::channel($channel)->info('CONTACT_CONFIRM|CHECK|INPUT_INVOICE|' . $invoice->id, ['contact_res' => [$contractInfo]]);
            if ($this->isContractCancel($contractInfo, $invoice)) {
                return redirect()->route('payment_invoice.checkout')->with('status',
                    __('Payment canceled')
                );
            }

            if ($this->isContractComplete($contractInfo, $invoice)) {
                return redirect()->route('payment_invoice.checkout')->with('status',
                    __('Payment closed successfully')
                );
            }

            if ($this->isContractClosed($contractInfo, $invoice)) {
                return redirect()->route('payment_invoice.checkout')->with('status',
                    __('Payment closed')
                );
            }

            if ($request->input('action') == 0) {
                $res = $this->contractManager->contractCancelByProvider(['id' => $invoice['payment_id'], 'service_provider_id' => $invoice['service_provider_id']]);
                if ($res) {
                    $message =  __('Payment canceled');
                    $invoice->status = PaymentInvoice::STATUS_CANCELED;
                    $invoice->save();
                    Log::channel($channel)->info('CONTACT_CONFIRM|CANCEL|INPUT_INVOICE|' . $invoice->id, [$res]);
                    return redirect()->route('payment_invoice.add')->with('status', $message);
                } else {
                    Log::channel($channel)->info('CONTACT_CONFIRM|CANCEL|ERROR|INPUT_INVOICE|' . $invoice->id, [$res]);
                }
            }

            if (!$this->isContractFunded($contractInfo)) {
                return back()->with('status',
                    __('Request processing ...')
                );
            }

            if ($request->input('action') == 1) {
                $res = $this->contractManager->contactMarkAsPaidByProvider(['id' => $invoice['payment_id'], 'service_provider_id' => $invoice['service_provider_id']]);
                if ($res) {
                    $invoice->status = PaymentInvoice::STATUS_USER_CONFIRMED;
                    $invoice->save();
                    $message = __('Payment in progress ...');
                    Log::channel($channel)->info('CONTACT_CONFIRM|MARK_AS_PAID|SUCCESS|INPUT_INVOICE|' . $invoice->id, [$res]);
                    if ($request->hasFile('document')) {
                        $requestMessage = $this->contractManager->contactMessagePostByProvider([
                            'id' => $invoice['payment_id'],
                            'service_provider_id' => $invoice['service_provider_id'],
                            'document' => $request->file('document'),
                            'user_id' =>  $invoice->merchant_id,
                            'remote' => true
                        ]);
                    }
                } else {
                    Log::channel($channel)->info('CONTACT_CONFIRM|MARK_AS_PAID|ERROR|INPUT_INVOICE|' . $invoice->id, [$res]);
                    return back()->with('status',
                        __('Whoops! Something went wrong.')
                    );
                }
            }
        }

        if (empty($message)) {
            $message = __('Whoops! Something went wrong.');
        }
        return redirect()->route('payment_invoice.add')->with('status', $message);


    }

    public function getQr(Request $request, $id)
    {
        $invoice = PaymentInvoice::where([
            'id' => $id,
        ])
            ->with('crypto_address')
            ->firstOrFail();

        if(empty($invoice->crypto_address->address)){
            abort(404);
        }
        $size = 200;
        $imageBackEnd = new ImagickImageBackEnd();
        $renderer = new ImageRenderer(
            (new RendererStyle($size))->withSize($size),
            $imageBackEnd
        );

        $bacon = new Writer($renderer);

        $data = $bacon->writeString(
            $invoice->crypto_address->address,
            'utf-8'
        );

        $response = Response::make($data, 200);
        $response->header("Content-Type", "image/png");
        return $response;
    }

    public function invoicePostCheckoutComplete(Request $request)
    {
        return redirect()->route('payment_invoice.add')->with('status',
            __('Payment closed successfully')
        );
    }

    public function invoicePostCheckoutDecline(Request $request)
    {
        return redirect()->route('payment_invoice.add')->with('status',
        __('Payment canceled')
        );
    }
}
