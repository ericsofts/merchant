<?php

namespace App\Http\Controllers;

use App\Models\PaymentInvoice;
use Illuminate\Http\Request;
use NumberFormatter;

class DashboardController extends Controller
{
    public function index(Request $request)
    {

        $invoice_histories = PaymentInvoice::where([
            'merchant_id' => $request->user()->id
        ])
            ->orderBy('id', 'desc')
            ->limit(5)
            ->get();
        $fmt = new NumberFormatter('en_US', NumberFormatter::CURRENCY);

        $balance_total = $request->user()->balance_total();

        return view('dashboard', [
            'invoice_histories' => $invoice_histories,
            'fmt' => $fmt,
            'balance_total' => $balance_total
        ]);
    }
}
