<?php

namespace App\Http\Controllers;

use App\Models\MerchantProperty;
use App\Models\MerchantWithdrawalInvoice;
use App\Models\PaymentInvoice;
use App\Models\Property;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Rule;
use PragmaRX\Google2FALaravel\Facade as Google2FA;

class AccountSettingsController extends Controller
{
    public function index(Request $request)
    {
        $available_merchant_coins = json_decode(MerchantProperty::getProperty($request->user()->id, 'available_coins', config('app.available_merchant_coins')), true);
        $available_coins = json_decode(Property::getProperty('currency', 0, 'available_coins', config('app.available_coins')), true);
        $balance_total = $request->user()->balance_total();

        $countActivePI = PaymentInvoice::where([
            'merchant_id' => $request->user()->id
        ])
            ->whereNotIn('status', [PaymentInvoice::STATUS_PAYED, PaymentInvoice::STATUS_CANCELED, PaymentInvoice::STATUS_CANCELED_BY_TIMEOUT])
            ->count();

        $countActiveWI = MerchantWithdrawalInvoice::where([
            'merchant_id' => $request->user()->id
        ])
            ->whereNotIn('status', [MerchantWithdrawalInvoice::STATUS_PAYED, MerchantWithdrawalInvoice::STATUS_CANCELED, MerchantWithdrawalInvoice::STATUS_CANCELED_BY_TIMEOUT])
            ->count();

        return view('account.settings',[
            'available_coins' => $available_coins,
            'merchant_coin' => $available_merchant_coins[0],
            'enable_2fa' => $request->user()->enable_2fa,
            'activeCountInvoices' => $countActivePI + $countActiveWI,
            'balance_total' => $balance_total
        ]);
    }

    public function update(Request $request)
    {
        $available_coins = json_decode(Property::getProperty('currency', 0, 'available_coins', config('app.available_coins')), true);
        $secret2fa = $request->user()->secret_2fa;
        $enable_2fa = $request->user()->enable_2fa;

        $request->validate([
            'currency' => ['required', Rule::in($available_coins)],
            'code' => ['required', 'string', function ($attribute, $value, $fail) use ($secret2fa, $enable_2fa) {
                if (!$enable_2fa) {
                    return $fail(__('The 2FA is not enabled.'));
                }
                if (!Google2FA::verifyKey($secret2fa, $value)) {
                    $fail(__('The code you provided is not valid.'));
                }
            }]
        ]);

        $balance_total = $request->user()->balance_total();

        $countActivePI = PaymentInvoice::where([
            'merchant_id' => $request->user()->id
        ])
            ->whereNotIn('status', [PaymentInvoice::STATUS_PAYED, PaymentInvoice::STATUS_CANCELED, PaymentInvoice::STATUS_CANCELED_BY_TIMEOUT])
            ->count();

        $countActiveWI = MerchantWithdrawalInvoice::where([
            'merchant_id' => $request->user()->id
        ])
            ->whereNotIn('status', [MerchantWithdrawalInvoice::STATUS_PAYED, MerchantWithdrawalInvoice::STATUS_CANCELED, MerchantWithdrawalInvoice::STATUS_CANCELED_BY_TIMEOUT])
            ->count();

        if($balance_total > 0 || $countActivePI > 0 || $countActiveWI > 0){
            return redirect()->back()->with(['alert-type' => 'error', 'message' => __('You can switch the account currency when the balance is zero and there are no active trades')]);
        }

        $property = MerchantProperty::firstOrCreate([
            'merchant_id' => $request->user()->id,
            'property' => 'available_coins'
        ],
            ['value' => json_encode([$request->input('currency')])]
        );
        $property->value = json_encode([$request->input('currency')]);
        $property->save();

        return redirect()->route('merchant.settings.account')
            ->with(['alert-type' => 'success', 'message' => __('Successfully saved!')]);
    }

}
