<?php

namespace App\Http\Controllers;

use App\Exports\MerchantWithdrawalInvoiceExport;
use App\Http\Requests\WithdrawalCreateRequest;
use App\Lib\Chatex\ChatexApi;
use App\Lib\Chatex\ChatexApiLBC;
use App\Models\AccountingEntrie;
use App\Models\InvoicesHistory;
use App\Models\Merchant;
use App\Models\MerchantCommission;
use App\Models\MerchantProperty;
use App\Models\MerchantWithdrawalInvoice;
use App\Models\MerchantWithdrawalInvoiceAttachment;
use App\Models\Property;
use App\Models\ServiceProvider;
use App\Models\ServiceProvidersProperty;
use App\Models\TraderAd;
use App\Services\AdsSelector;
use App\Services\ContractManager;
use App\Traits\Contract;
use App\Traits\Invoice;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use NumberFormatter;

class WithdrawalController extends Controller
{
    use Invoice;
    use Contract;

    private $adsService;
    private $merchantModel;
    protected $contractManager = null;

    public function __construct(
        Merchant $merchantModel,
        AdsSelector $adsService,
        ContractManager $contractManager
    )
    {
        $this->merchantModel = $merchantModel;
        $this->adsService = $adsService;
        $this->contractManager = $contractManager;
    }

    public function index(Request $request)
    {
        $limit = config('app.default.pagination_limit');

        $from_date = $request->input('from_date');
        $to_date = $request->input('to_date');
        $status = $request->input('status');

        $invoicesQuery = MerchantWithdrawalInvoice::where([
            'merchant_id' => $request->user()->id
        ])
            ->orderBy('id', 'desc');

        if (!empty($from_date)) {
            try {
                $from_date = Carbon::parse($from_date, config('app.default.timezone'));
                $from_date = $from_date->timezone('UTC');
            } catch (Exception $e) {
                $from_date = null;
            }
        }
        if (!empty($to_date)) {
            try {
                $to_date = Carbon::parse($to_date, config('app.default.timezone'));
                $to_date = $to_date->timezone('UTC');
            } catch (Exception $e) {
                $to_date = null;
            }
        }
        if ($from_date) {
            $invoicesQuery->where('created_at', '>=', $from_date);
        }

        if ($to_date) {
            $invoicesQuery->where('created_at', '<=', $to_date);
        }
        if (!is_null($status) && in_array($status, [MerchantWithdrawalInvoice::STATUS_PAYED, MerchantWithdrawalInvoice::STATUS_CREATED, MerchantWithdrawalInvoice::STATUS_TRADER_CONFIRMED_PAYMENT])) {
            $invoicesQuery->where('status', '=', $status);
        }

        if ($request->has('btnExport')) {
            return (new MerchantWithdrawalInvoiceExport($invoicesQuery))->download('invoices.xlsx');
        }

        $invoices = $invoicesQuery->paginate($limit)->withQueryString();

        $total = MerchantWithdrawalInvoice::where([
            'merchant_id' => $request->user()->id,
            'status' => MerchantWithdrawalInvoice::STATUS_PAYED
        ])->sum('amount');

        $fmt = new NumberFormatter('en_US', NumberFormatter::CURRENCY);

        return view('withdrawal.index', [
            'status' => $status,
            'from_date' => $from_date ? $from_date->timezone(config('app.default.timezone')) : null,
            'to_date' => $to_date ? $to_date->timezone(config('app.default.timezone')) : null,
            'invoices' => $invoices,
            'fmt' => $fmt,
            'total' => $total,
            'total_page' => $invoices->getCollection()->where('status', '=', MerchantWithdrawalInvoice::STATUS_PAYED)->sum('amount')
        ]);
    }

    public function view(Request $request, $id)
    {
        $invoice = MerchantWithdrawalInvoice::where([
            'merchant_id' => $request->user()->id,
            'id' => $id
        ])
            ->firstOrFail();

        MerchantWithdrawalInvoiceAttachment::addAttachments($this->contractManager, $invoice, true);
        $attachments = MerchantWithdrawalInvoiceAttachment::where(['invoice_id' => $invoice->id])->orderBy('id')->get();

        return view('withdrawal.view', [
            'invoice' => $invoice,
            'attachments' => $attachments
        ]);
    }

    public function create(Request $request)
    {
        return view('withdrawal.create', [
            'balance' => $request->user()->balance_total(),
            'fmt' => new NumberFormatter('en_US', NumberFormatter::CURRENCY)
        ]);
    }

    public function paymentMethod(WithdrawalCreateRequest $request)
    {
        $rules = [
            'amount' => 'required|numeric|gt:0|regex:/^\d+(\.\d{1,2})?$/',
            'payment_method' => ['required', Rule::in(['bank_card'])],
            'comment' => 'sometimes'
        ];

        $validator = Validator::make($request->input(), $rules, [], [
            'amount' => __('amount'),
            'payment_method' => __('payment method'),
            'comment' => __('comment')
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }


        $merchant = $request->user();
        $service_provider = $merchant->default_service_provider();
        $count_service_provider = $merchant->service_providers()->count();
        $min_invoice_amount = config('app.min_merchant_withdrawal_amount');
        if($count_service_provider == 1){
            $min_invoice_amount = MerchantProperty::getProperty($merchant->id, 'min_merchant_withdrawal_amount', $min_invoice_amount, $service_provider->id);
        }else{
            $props_min_amount = MerchantProperty::getPropertiesByName($merchant->id, 'min_merchant_withdrawal_amount');
            foreach ($props_min_amount as $value) {
                $min_invoice_amount = ($value < $min_invoice_amount) ? $value : $min_invoice_amount;
            }
        }

        $payment_method = $request->input('payment_method');
        $amount2merchant = $amount = $request->input('amount');
        if ($request->input('merchant_expense')) {
            $fee = $this->merchantModel->getRevertFeeCommission($request->user()->id, $amount, MerchantWithdrawalInvoice::API_TYPE_3, MerchantCommission::PROPERTY_TYPE_WITHDRAWAL_COMMISSION, $service_provider->id);
            $amount2pay = $amount;
            $amount2merchant = $amount + $fee['total'];
        } else {
            $fee = $this->merchantModel->getFeeCommission($request->user()->id, $amount, MerchantWithdrawalInvoice::API_TYPE_3, MerchantCommission::PROPERTY_TYPE_WITHDRAWAL_COMMISSION, $service_provider->id);
            $amount2pay = $amount - $fee['total'];
        }

        if ($amount < $min_invoice_amount) {
            $validator->after(function ($validator) use ($min_invoice_amount) {
                $validator->errors()->add('amount', __('The minimum amount must be greater than or equal to :amount', ['amount' => $min_invoice_amount]));
            });
            $validator->validate();
        }
        $merchantBalance = $request->user()->balance_total();
        if ($merchantBalance <= 0) {
            $validator->after(function ($validator) {
                $validator->errors()->add('amount', __('Whoops! Something went wrong. Contact Support. Error: :error', ['error' => 'I10007']));
            });
            $validator->validate();
        }

        if ($amount > $merchantBalance) {
            $validator->after(function ($validator) use ($merchantBalance) {
                $validator->errors()->add('amount', __('The maximum amount must be less than or equal to :amount', ['amount' => $merchantBalance]));
            });
            $validator->validate();
        }

        if ($payment_method == ChatexApiLBC::PM_NAME) { // chatex_lbc

            if (!$available_coins = $this->getAvailableMerchantCoins($request->user()->id)) {
                $validator->after(function ($validator) {
                    $validator->errors()->add('amount', __('Whoops! Something went wrong. Contact Support.  Error: :error', ['error' => 'I10002']));
                });
                $validator->validate();
            }

            if (!$this->getCoinRates($available_coins)) {
                $validator->after(function ($validator) {
                    $validator->errors()->add('amount', __('Whoops! Something went wrong. Contact Support.  Error: :error', ['error' => 'I10004']));
                });
                $validator->validate();
            }
            $currency = 'USD';
            $buy_coins = $this->adsService->getAdsByMerchant($merchant, [
                'amount' => $amount,
                'amount2pay' => $amount2pay,
                'currency' => $currency,
                'input_amount' => floatval($request->input('amount')),
                'currency2currency' => false,
                'expense' => $request->input('merchant_expense'),
                'type' => TraderAd::TYPE_BUY,
                'api' => MerchantWithdrawalInvoice::API_TYPE_3
            ]);

            if (!$buy_coins) {
                return back()->with('status',
                    __('There are no payment systems available.')
                );
            }

            $currencies = array_keys($buy_coins);

            return view('withdrawal.create_chatex_lbc', [
                'currencies' => $currencies,
                'buy_coins' => $buy_coins,
                'amount' => $amount,
                'amount2merchant' => $amount2merchant,
                'amount2pay' => $amount2pay
            ]);
        }

        return back()->with('status',
            __('There are no payment systems available')
        );
    }

    public function chatex_lbc_contact_create(Request $request)
    {
        $amount = $data['amount'] = $request->input('amount');
        $data['payment_system_id'] = $request->input('payment_system_id');
        $data['currency'] = $request->input('currency');
        $data['message'] = $request->input('message');

        $rules = [
            'payment_system_id' => 'required|string',
            'currency' => 'required',
            'amount' => 'required|numeric|gt:0|regex:/^\d+(\.\d{1,2})?$/',
            'message' => 'required|regex:/^\d{16,19}$/',
        ];

        $validator = Validator::make($data, $rules, [], [
            'currency' => __('currency'),
            'payment_system_id' => __('payment system'),
            'message' => __('bankcard number')
        ]);

        if ($validator->fails()) {
            $request->merge(['payment_method' => ChatexApiLBC::PM_NAME]);
            return redirect()->route('withdrawal.create.payment_method1', ['payment_method' => ChatexApiLBC::PM_NAME])
                ->withErrors($validator)
                ->withInput();
        }
        $merchant = $request->user();
        $service_provider = $merchant->default_service_provider();
        $count_service_provider = $merchant->service_providers()->count();
        $min_invoice_amount = config('app.min_merchant_withdrawal_amount');
        if($count_service_provider == 1){
            $min_invoice_amount = MerchantProperty::getProperty($merchant->id, 'min_merchant_withdrawal_amount', $min_invoice_amount, $service_provider->id);
        }else{
            $props_min_amount = MerchantProperty::getPropertiesByName($merchant->id, 'min_merchant_withdrawal_amount');
            foreach ($props_min_amount as $value) {
                $min_invoice_amount = ($value < $min_invoice_amount) ? $value : $min_invoice_amount;
            }
        }
        $ad = $this->adsService->getAd($data['payment_system_id']);

        if ($request->input('merchant_expense')) {
            $fee = $this->merchantModel->getRevertFeeCommission($request->user()->id, $amount, MerchantWithdrawalInvoice::API_TYPE_3, MerchantCommission::PROPERTY_TYPE_WITHDRAWAL_COMMISSION, $ad['service_provider_id']);
            $amount2pay = $amount;
            $amount += $fee['total'];
        } else {
            $fee = $this->merchantModel->getFeeCommission($request->user()->id, $amount, MerchantWithdrawalInvoice::API_TYPE_3, MerchantCommission::PROPERTY_TYPE_WITHDRAWAL_COMMISSION, $ad['service_provider_id']);
            $amount2pay = $amount - $fee['total'];
        }

        if ($amount < $min_invoice_amount) {
            return back()->with('status',
                __('The minimum amount must be greater than or equal to :amount', ['amount' => $min_invoice_amount])
            );
        }
        $merchantBalance = $request->user()->balance_total();
        if ($merchantBalance <= 0) {
            return back()->with('status',
                __('Whoops! Something went wrong. Contact Support. Error: :error', ['error' => 'I10007'])
            );
        }

        if ($amount > $merchantBalance) {
            return back()->with('status',
                __('The maximum amount must be less than or equal to :amount', ['amount' => $merchantBalance])
            );
        }

        if (!$ad || $ad['type'] != 'BUY') {
            Log::channel('chatex_lbc_requests')->info('BUY|AD|NOT_FOUND|ERROR|USER_ID|' . $request->user()->id, [$data]);
            Log::channel('chatex_lbc_requests')->info('BUY|AD|NOT_FOUND|ERROR|USER_ID|' . $request->user()->id, [$ad]);
            return redirect()->route('withdrawal.create')->with('status',
                __('Whoops! Something went wrong.')
            );
        }

        $coin = strtolower($ad['coin']);
        $available_coins = $this->getAvailableMerchantCoins($request->user()->id);
        if (!$this->isAvailableCoins($ad, $available_coins)) {
            Log::channel($ad['log'])->info('BUY|AD|COIN|ERROR|MERCHANT_ID|' . $request->user()->id, ['ad' => $ad, 'available_coins' => $available_coins]);
            return back()->with('status',
                __('Whoops! Something went wrong. Contact Support. Error: :error', ['error' => 'I10002'])
            );
        }

        if (!$coin_rate = $this->getCoinRate($coin)) {
            Log::channel($ad['log'])->info('BUY|AD|COIN_RATE|ERROR|MERCHANT_ID|' . $request->user()->id, ['ad' => $ad, 'coin_rate' => $coin_rate]);
            return back()->with('status',
                __('Whoops! Something went wrong. Contact Support. Error: :error', ['error' => 'I10004'])
            );
        }

        if($ad['service_provider_type'] == ServiceProvider::CHATEX_PROVIDER) {
            $chatexProperties = ServiceProvidersProperty::getProperties(ChatexApi::OBJECT_NAME);
            $chatex = new ChatexApi($chatexProperties['base_url'], $chatexProperties['api_token']);
            $wallet = $chatex->wallet($coin);
            $walletBalance = $wallet['amount'] ?? 0;
            if ($walletBalance < $amount2pay) {
                return back()->with('status',
                    __('Whoops! Something went wrong. Contact Support. Error: :error', ['error' => 'W00101'])
                );
            }
        }

        $coin_amount = $amount2pay * $coin_rate;
        $currency_amount = round($ad['temp_price'] * $coin_amount, 2);
        $contract = $this->contractManager->contractCreate($ad, $currency_amount, $request->user(), false, $data['message']);
        if ($contract) {
            $contractInfo = $this->contractManager->contractInfoByContract($contract);
            if ($contractInfo) {
                $ps_amount = $contractInfo['amount_btc'] ?? null;
                $ps_currency = $ad['coin'] ?? null;
                $fiat_currency = $contractInfo['currency'] ?? null;
                $invoice = MerchantWithdrawalInvoice::create([
                    'merchant_id' => $request->user()->id,
                    'amount' => $amount,
                    'amount2pay' => $amount2pay,
                    'amount2service' => $fee['service']['amount'],
                    'amount2agent' => $fee['agent']['amount'],
                    'amount2grow' => $fee['grow']['amount'],
                    'commission_grow' => $fee['grow']['commission'],
                    'commission_agent' => $fee['agent']['commission'],
                    'commission_service' => $fee['service']['commission'],
                    'payment_system' => MerchantWithdrawalInvoice::PAYMENT_SYSTEM_CHATEX_LBC,
                    'service_provider_id' => $ad['service_provider_id'],
                    'payment_id' => $contractInfo['id'],
                    'ps_amount' => $ps_amount,
                    'ps_currency' => strtolower($ps_currency),
                    'fiat_amount' => $contractInfo['amount'] ?? null,
                    'fiat_currency' => strtolower($fiat_currency),
                    'account_info' => $contractInfo['account_info'] ?? null,
                    'addition_info' => json_encode([
                        'contact' => $contractInfo,
                        'ad' => $ad
                    ]),
                    'status' => MerchantWithdrawalInvoice::STATUS_USER_SELECTED,
                    'api_type' => MerchantWithdrawalInvoice::API_TYPE_0,
                    'comment' => $request->input('comment'),
                    'merchant_expense' => $request->input('merchant_expense')
                ]);

                return redirect()->route('withdrawal.create.chatex_lbc_contact_confirm_show', $invoice->id);
            }
        }

        Log::channel($ad['log'])->info('BUY|CONTACT_CREATE|CREATE|ERROR|USER_ID|' . $request->user()->id, [$data]);
        Log::channel($ad['log'])->info('BUY|CONTACT_CREATE|CREATE|ERROR|USER_ID|' . $request->user()->id, ['amount' => $currency_amount]);
        Log::channel($ad['log'])->info('BUY|CONTACT_CREATE|CREATE|ERROR|USER_ID|' . $request->user()->id, [$contract]);

        return redirect()->route('withdrawal.create')->with('status',
            __('Whoops! Something went wrong. Contact Support. Error: :error', ['error' => 'I10006'])
        );
    }

    public function chatex_lbc_contact_confirm_show(Request $request, $withdraw_id)
    {
        $invoice = MerchantWithdrawalInvoice::where([
            'id' => $withdraw_id,
            'merchant_id' => $request->user()->id,
            'payment_system' => MerchantWithdrawalInvoice::PAYMENT_SYSTEM_CHATEX_LBC,
            'api_type' => MerchantWithdrawalInvoice::API_TYPE_0
        ])
            ->whereIn('status', [
                MerchantWithdrawalInvoice::STATUS_CREATED,
                MerchantWithdrawalInvoice::STATUS_TRADER_CONFIRMED,
                MerchantWithdrawalInvoice::STATUS_TRADER_CONFIRMED_PAYMENT,
                MerchantWithdrawalInvoice::STATUS_USER_SELECTED
            ])
            ->first();

        if (!$invoice) {
            return redirect()->route('withdrawal.create')->with('status',
                __('Whoops! Something went wrong. Contact Support. Error: :error', ['error' => 'I10000'])
            );
        }

        if ($invoice->status == MerchantWithdrawalInvoice::STATUS_TRADER_CONFIRMED_PAYMENT) {
            MerchantWithdrawalInvoiceAttachment::addAttachments($this->contractManager,$invoice, true);
        }

        $attachments = MerchantWithdrawalInvoiceAttachment::where(['invoice_id' => $invoice->id])->orderBy('id')->get();

        $contractInfo = $this->contractManager->contractInfoByProvider(['id' => $invoice['payment_id'], 'service_provider_id' => $invoice['service_provider_id']]);
        $channel = $this->getLogChannel($invoice);
        $deal_accepted = $payment_completed = false;
        if ($contractInfo) {
            Log::channel($channel)->info('BUY|CONTACT_CONFIRM|CHECK|USER_ID|' . $request->user()->id . '|WITHDRAWAL|' . $invoice->id, [$contractInfo]);
            if ($this->isContractCancel($contractInfo, $invoice)) {
                $invoice->status = MerchantWithdrawalInvoice::STATUS_CANCELED;
                $invoice->save();
                return redirect()->route('withdrawal.create')->with('status',
                    __('The withdrawal canceled')
                );
            }
            if ($this->isContractComplete($contractInfo, $invoice)) {
                return redirect()->route('withdrawal.create')->with('status',
                    __('The withdrawal has been successfully completed')
                );
            }
            if ($this->isContractClosed($contractInfo, $invoice)) {
                return redirect()->route('withdrawal.create')->with('status',
                    __('The withdrawal closed')
                );
            }
            if (!empty($contractInfo['funded_at'])) {
                $deal_accepted = true;
            }
            if ($this->isContractPaymentCompleted($contractInfo, $invoice)) {
                $payment_completed = true;
                if ($this->isContractPaymentCompleted($contractInfo, $invoice) && $invoice->status == MerchantWithdrawalInvoice::STATUS_CREATED) {
                    $invoice->status = MerchantWithdrawalInvoice::STATUS_TRADER_CONFIRMED_PAYMENT;
                    $invoice->save();
                }
            }
            $addition_info = json_decode($invoice['addition_info'], true);
            return view('withdrawal.confirm_chatex_lbc', [
                'id' => $invoice->id,
                'invoice' => $invoice,
                'attachments' => $attachments,
                'deal_accepted' => $deal_accepted,
                'payment_completed' => $payment_completed,
                'amount' => $contractInfo['amount'],
                'bank_name' => $addition_info['ad']['bank_name'] ?? null,
                'currency' => $addition_info['ad']['currency'] ?? null,
            ]);
        } else {
            Log::channel($channel)->info('BUY|CONTACT_CONFIRM_SHOW|ERROR|USER_ID|' . $request->user()->id . '|WITHDRAWAL|' . $invoice->id, [$contractInfo]);
        }
        return redirect()->route('withdrawal.create')->with('status',
            __('Whoops! Something went wrong. Contact Support. Error: :error', ['error' => 'I10006'])
        );
    }

    public function chatex_lbc_contact_confirm(Request $request)
    {
        $rules = [
            'id' => 'required|numeric',
            'action' => 'required|numeric'
        ];

        $validator = Validator::make($request->only(['id', 'action']), $rules);

        if ($validator->fails()) {
            return back()->withErrors($validator);
        }

        $invoice = MerchantWithdrawalInvoice::where([
            'id' => $request->input('id'),
            'merchant_id' => $request->user()->id,
            'payment_system' => MerchantWithdrawalInvoice::PAYMENT_SYSTEM_CHATEX_LBC,
            'api_type' => MerchantWithdrawalInvoice::API_TYPE_0
        ])
            ->whereIn('status', [
                MerchantWithdrawalInvoice::STATUS_CREATED,
                MerchantWithdrawalInvoice::STATUS_TRADER_CONFIRMED,
                MerchantWithdrawalInvoice::STATUS_TRADER_CONFIRMED_PAYMENT,
                MerchantWithdrawalInvoice::STATUS_USER_SELECTED
            ])
            ->first();

        if (!$invoice) {
            return back()->with('status',
                __('Whoops! Something went wrong. Contact Support. Error: :error', ['error' => 'I10000'])
            );
        }

        if ($request->input('action') == 1) {
            $attachments = MerchantWithdrawalInvoiceAttachment::where(['invoice_id' => $invoice->id])->count();
            if (!$attachments) {
                return back()->with('status',
                    __('Whoops! Something went wrong. Contact Support. Error: :error', ['error' => 'I10010'])
                );
            }
        }

        $channel = $this->getLogChannel($invoice);
        $contractInfo = $this->contractManager->contractInfoByProvider(['id' => $invoice['payment_id'], 'service_provider_id' => $invoice['service_provider_id']]);
        if ($contractInfo) {
            Log::channel($channel)->info('BUY|CONTACT_CONFIRM|CHECK|USER_ID|' . $request->user()->id . '|WITHDRAWAL|' . $invoice->id, [$contractInfo]);
            if ($this->isContractCancel($contractInfo, $invoice)) {
                $invoice->status = MerchantWithdrawalInvoice::STATUS_CANCELED;
                $invoice->save();
                return redirect()->route('withdrawal.create')->with('status',
                    __('The withdrawal canceled')
                );
            }

            if ($this->isContractComplete($contractInfo, $invoice)) {
                return redirect()->route('withdrawal.create')->with('status',
                    __('The withdrawal has been successfully completed')
                );
            }

            if ($this->isContractClosed($contractInfo, $invoice)) {
                return redirect()->route('withdrawal.create')->with('status',
                    __('The withdrawal closed')
                );
            }

            if ($request->input('action') == 0) {
                if (empty($contractInfo['funded_at'])) {
                    $res = $this->contractManager->contractCancelByProvider(['id' => $invoice['payment_id'], 'service_provider_id' => $invoice['service_provider_id']]);
                    if ($res) {
                        $message = __('The withdrawal canceled');
                        $invoice->status = MerchantWithdrawalInvoice::STATUS_CANCELED;
                        $invoice->save();
                        Log::channel($channel)->info('BUY|CONTACT_CONFIRM|CANCEL|USER_ID|' . $request->user()->id . '|WITHDRAWAL|' . $invoice->id, [$res]);
                        return redirect()->route('withdrawal.create')->with('status', $message);
                    } else {
                        Log::channel($channel)->info('BUY|CONTACT_CONFIRM|CANCEL|ERROR||USER_ID|' . $request->user()->id . '|WITHDRAWAL|' . $invoice->id, [$res]);
                        return back()->with('status',
                            __('Whoops! Something went wrong. Contact Support. Error: :error', ['error' => 'I10006'])
                        );
                    }
                } else {
                    return back()->with('status',
                        __("You can't cancel the withdrawal because the request is already confirmed")
                    );
                }
            }

            if (empty($contractInfo['funded_at'])) {
                return back()->with('status',
                    __('Request processing ...')
                );
            }

            if ($request->input('action') == 1) {
                if (!$this->isContractPaymentCompleted($contractInfo, $invoice)) {
                    return back()->with('status',
                        __('Payment has not been paid yet')
                    );
                }
                $res = $this->contractManager->contactReleaseByProvider(['id' => $invoice['payment_id'], 'service_provider_id' => $invoice['service_provider_id']]);
                if ($res) {
                    $invoice->status = MerchantWithdrawalInvoice::STATUS_PAYED;
                    $invoice->payed = now();
                    $invoice->save();
                    $message = __('The withdrawal has been successfully completed');
                    Log::channel($channel)->info('BUY|CONTACT_CONFIRM|RELEASE|SUCCESS|MERCHANT_ID|' . $request->user()->id . '|WITHDRAWAL|' . $invoice->id, [$res]);
                } else {
                    Log::channel($channel)->info('BUY|CONTACT_CONFIRM|RELEASE|ERROR|MERCHANT_ID|' . $request->user()->idd . '|WITHDRAWAL|' . $invoice->id, [$res]);
                }
            }
            if (empty($message)) {
                $message = __('Whoops! Something went wrong. Contact Support. Error: :error', ['error' => 'I10099']);
            }
            return redirect()->route('withdrawal.create')->with('status', $message);
        }
        Log::channel($channel)->info('BUY|CONTACT_CONFIRM|ERROR|USER_ID|' . $request->user()->id . '|WITHDRAWAL|' . $invoice->id, [$contractInfo]);
        return back()->with('status',
            __('Whoops! Something went wrong. Contact Support. Error: :error', ['error' => 'I10006'])
        );
    }

    public function chatex_lbc_contact_check(Request $request, $input_invoice_id)
    {
        $result = ['status' => 0];
        $invoice = MerchantWithdrawalInvoice::where([
            'id' => $input_invoice_id,
            'merchant_id' => $request->user()->id,
            'payment_system' => MerchantWithdrawalInvoice::PAYMENT_SYSTEM_CHATEX_LBC,
            'api_type' => MerchantWithdrawalInvoice::API_TYPE_0,
        ])
            ->whereIn('status', [
                MerchantWithdrawalInvoice::STATUS_CREATED,
                MerchantWithdrawalInvoice::STATUS_TRADER_CONFIRMED,
                MerchantWithdrawalInvoice::STATUS_TRADER_CONFIRMED_PAYMENT,
                MerchantWithdrawalInvoice::STATUS_USER_SELECTED
            ])
            ->first();

        if ($invoice) {
            $contractInfo = $this->contractManager->contractInfoByProvider(['id' => $invoice['payment_id'], 'service_provider_id' => $invoice['service_provider_id']]);
            if ($this->isContractCancel($contractInfo, $invoice)) {
                $result['status'] = -1;
            } else if ($this->isContractPaymentCompleted($contractInfo, $invoice)) {
                $result['status'] = 2;
                if ($this->isContractPaymentCompleted($contractInfo, $invoice)) {
                    if ($this->isContractPaymentCompleted($contractInfo, $invoice)
                        && in_array($invoice->status, [MerchantWithdrawalInvoice::STATUS_CREATED, MerchantWithdrawalInvoice::STATUS_TRADER_CONFIRMED])
                    ) {
                        $invoice->status = MerchantWithdrawalInvoice::STATUS_TRADER_CONFIRMED_PAYMENT;
                        $invoice->save();
                    }
                }
            } else if ($this->isContractFunded($contractInfo, $invoice)) {
                $result['status'] = 1;
            }
            $new_attachments = [];
            if ($invoice->status == MerchantWithdrawalInvoice::STATUS_TRADER_CONFIRMED_PAYMENT) {
                foreach (MerchantWithdrawalInvoiceAttachment::addAttachments($this->contractManager, $invoice, true) as $attachment) {
                    $new_attachments[] = route('withdrawal.attachment', $attachment->id);
                }
            }
            $result['attachments'] = MerchantWithdrawalInvoiceAttachment::where(['invoice_id' => $invoice->id])->orderBy('id')->get()->pluck('id');
            $result['new_attachments'] = $new_attachments;
        }
        return response()->json($result);
    }

    public function attachment(Request $request, $id)
    {
        $attachment = MerchantWithdrawalInvoiceAttachment::where(['id' => $id])
            ->whereHas('invoice', function ($invoice) use ($request) {
                return $invoice->where(['merchant_id' => $request->user()->id]);
            })
            ->firstOrFail();

        return response(file_get_contents($attachment->url), 200)->header('Content-type', $attachment->type);
    }
}
