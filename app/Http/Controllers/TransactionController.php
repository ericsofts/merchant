<?php

namespace App\Http\Controllers;

use App\Exports\TransactionsExport;
use App\Repositories\TransactionRepository;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class TransactionController extends Controller
{
    private TransactionRepository $transactionRepository;

    public function __construct(TransactionRepository $transactionRepository)
    {
        $this->transactionRepository = $transactionRepository;
    }

    public function allInvoices(Request $request)
    {
        $limit = config('app.default.pagination_limit');
        $from_date = $request->input('from_date');
        $to_date = $request->input('to_date');
        $status = $request->input('status');
        $invoice_types = $request->input('invoice_types', []);

        if (!empty($from_date)) {
            try {
                $from_date = Carbon::parse($from_date, config('app.default.timezone'));
                $from_date = $from_date->timezone('UTC');
            } catch (Exception $e) {
                $from_date = null;
            }
        }
        if (!empty($to_date)) {
            try {
                $to_date = Carbon::parse($to_date, config('app.default.timezone'));
                $to_date = $to_date->timezone('UTC');
            } catch (Exception $e) {
                $to_date = null;
            }
        }

        if($request->has('btnExport')){
            return (new TransactionsExport($this->transactionRepository, [
                'invoice_types' => $invoice_types,
                'status' => $status,
                'from_date' => $from_date,
                'to_date' => $to_date,
                'merchant_id' => $request->user()->id
            ]))->download('transactions.xlsx');
        }

        $paymentInvoiceQuery = $this->transactionRepository->transactions([
            'invoice_types' => $invoice_types,
            'status' => $status,
            'from_date' => $from_date,
            'to_date' => $to_date,
            'merchant_id' => $request->user()->id
        ]);
        $paymentInvoiceQuery->orderBy('created_at', 'desc');
        $invoices = $paymentInvoiceQuery->paginate($limit)->withQueryString();
        $total = $this->transactionRepository->transactions_total([
            'merchant_id' => $request->user()->id,
            'invoice_types' => $invoice_types,
            'from_date' => $from_date,
            'to_date' => $to_date
        ]);

        return view('transaction.all_invoices', [
            'status' => $status,
            'from_date' => $from_date ? $from_date->timezone(config('app.default.timezone')) : null,
            'to_date' => $to_date ? $to_date->timezone(config('app.default.timezone')) : null,
            'invoices' => $invoices,
            'total' => $total
        ]);
    }
}
