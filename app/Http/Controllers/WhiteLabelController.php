<?php

namespace App\Http\Controllers;


use App\Models\MerchantProperty;
use App\Models\WhiteLabel;
use App\Models\MerchantViewTheme;
use App\Traits\UploadTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class WhiteLabelController extends Controller
{
    use UploadTrait;

    private WhiteLabel $whiteLabel;
    private Request $request;

    public function __construct(
        WhiteLabel $whiteLabel,
        Request $request
    )
    {
        $this->whiteLabel = $whiteLabel;
        $this->request = $request;
    }

    public function whitelabel(Request $request)
    {
        $themes = MerchantViewTheme::all();

        $whitelabel = $this->whiteLabel::firstOrCreate([
            'merchant_id' => $request->user()->id
        ]);
        $disableName = MerchantProperty::getProperty($request->user()->id, 'disable_show_name', 0);
        return view('merchant.whitelabel', [
            'themes' => $themes,
            'disableName' => $disableName,
            'whitelabel' => $whitelabel
        ]);
    }

    public function preview(Request $request)
    {
        $whitelabel = $this->whiteLabel::firstOrCreate([
            'merchant_id' => $request->user()->id
        ]);
        $disableName = MerchantProperty::getProperty($request->user()->id, 'disable_show_name', 0);
        return view('merchant.whitelabel_preview')
            ->with('disableName',$disableName)
            ->with('whiteLabelModel',$whitelabel)
            ->with('merchant', $request->user()->id);
    }

    public function whitelabel_update(Request $request)
    {
        $postData = $request->input();
        $whitelabel = $this->whiteLabel->getWhiteLabelById($request->user()->id);

        if($request->input('action') == 'reset'){
            $whitelabel->style = $whitelabel->generateDefaultStyle();
            $whitelabel->enable = false;
            $whitelabel->save();
            return redirect()->route('merchant.whitelabel')
                ->with(['alert-type' => 'success', 'message' => __('Whitelabel has been successfully reset!')])
                ->with('whitelabel',$whitelabel);
        }
        $color = $this->getColorFieldRule($postData);
        $messages = [
            'page-image_logo.dimensions' => __('The maximum image size is over 500px wide')
        ];

        $rules = [
            'page-image_logo' => 'nullable|image|mimes:jpeg,png,jpg,gif|max:2048|dimensions:max_width=500',
            'page-header_label' => 'nullable|string',
            'page-header_text' => 'nullable|string',
            'enable' => 'nullable',
            'disable-name' => 'nullable',
            'page-footer_text' => 'nullable|string',
            'modal-btn_secondary_underline'  => ["required" , "in:underline,none"]
        ];

        $rules = array_merge($rules, $color);

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }

        $updateData = $this->preparePostData($postData, $whitelabel);

        $whitelabel->update($updateData);
        if($request->input('page-view_theme', false) !== false){
            $theme = MerchantProperty::firstOrCreate([
                'merchant_id' => $request->user()->id,
                'property' => 'viewTheme'
            ],
                ['value' => $request->input('page-view_theme')]
            );
            $theme->value = $request->input('page-view_theme');
            $theme->save();
        }

        $disableName = MerchantProperty::firstOrCreate([
            'merchant_id' => $request->user()->id,
            'property' => 'disable_show_name'
        ],
            ['value' => $request->input('disable-name')]
        );
        $disableName->value = $request->input('disable-name', 0);
        $disableName->save();

        return redirect()->route('merchant.whitelabel')
            ->with(['alert-type' => 'success', 'message' => __('Whitelabel has been successfully saved!')])
            ->with('whitelabel',$whitelabel);
    }

    protected function preparePostData($postData , $whitelabel){
        if(isset($postData['enable'])){
            $result['enable'] = $postData['enable'] ? true: false;
        }else{
            $result['enable'] = false;
        }
        $result['merchant_id'] = $this->request->user()->id;
        $result['style'] = $this->getStyleJson($postData , $whitelabel);
        return $result;
    }

    protected function getStyleJson($postData, $whitelabel){
        $result['page'] = $this->getStyleSection($postData, 'page');
        $result['modal'] = $this->getStyleSection($postData, 'modal');
        if ($this->request->has('page-image_logo')) {
            $result['page']['image_logo'] = $this->uploadImage();
        }else{
            $result['page']['image_logo'] = $whitelabel->getPageValue('image_logo');
        }

        return json_encode($result);
    }

    protected function getStyleSection($arr_main_array, $section){
        foreach($arr_main_array as $key => $value){
            $exp_key = explode('-', $key);
            if($exp_key[0] == $section){
                $arr_result[$exp_key[1]] = $value;
            }
        }
        return $arr_result;
    }

    protected function getColorFieldRule($postData){
        foreach($postData as $key => $value){
            $exp_key = explode('_', $key);
            if(end($exp_key) == 'color'){
                $arr_result[$key] = ['nullable', 'regex:/^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$/'];
            }
        }
        return $arr_result;
    }

    protected function uploadImage(){
        $image = $this->request->file('page-image_logo');
        $name = Str::slug($this->request->input('page-image_logo')).'_'.time();
        $folder = '';
        $filePath = $folder . $name. '.' . $image->getClientOriginalExtension();
        $this->uploadOne($image, $folder, 'whitelabel', $name, true);
        return $filePath;
    }
}
