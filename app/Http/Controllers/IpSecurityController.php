<?php

namespace App\Http\Controllers;



use App\Models\MerchantProperty;
use App\Rules\IpList;
use Illuminate\Http\Request;

class IpSecurityController extends Controller
{

    public function index(Request $request)
    {
        $listIp = MerchantProperty::getProperty($request->user()->id, 'permitted_ip_list');
        $enable = MerchantProperty::getProperty($request->user()->id, 'enable_ip_list');
        return view('merchant.ip_security',[
            'ip_list' => $listIp,
            'enable' => $enable
        ]);
    }



    public function update(Request $request)
    {
        $request->validate([
            'ip-list' => ['nullable', 'string', new IpList],
        ]);

        $property = MerchantProperty::firstOrCreate([
            'merchant_id' => $request->user()->id,
            'property' => 'enable_ip_list'
        ],
            ['value' => $request->input('enable')]
        );
        $property->value = $request->input('enable') ?? 0;
        $property->save();

        $property = MerchantProperty::firstOrCreate([
            'merchant_id' => $request->user()->id,
            'property' => 'permitted_ip_list'
        ],
            ['value' => $request->input('ip-list')]
        );
        $property->value = $request->input('ip-list');
        $property->save();

        return redirect()->route('merchant.ip_security')
            ->with(['alert-type' => 'success', 'message' => __('Successfully saved!')]);
    }


}
