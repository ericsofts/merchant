<?php

namespace App\Http\Controllers;

use App\Models\MerchantProperty;
use Illuminate\Http\Request;

class MerchantController extends Controller
{
    public function profile(Request $request)
    {
        $rounding_input_amount = MerchantProperty::getProperty($request->user()->id, 'rounding_input_amount');
        return view('merchant.profile', ['rounding_input_amount' => $rounding_input_amount]);
    }

    public function profile_update(Request $request)
    {
        $postData = $request->only([
            'name'
        ]);
        $request->validate([
            'name' => 'required|max:255',
            'rounding_input_amount' => 'sometimes|boolean'
        ]);
        $request->user()->update($postData);
        if($request->has('rounding_input_amount')){
            $property = MerchantProperty::firstOrCreate([
                'merchant_id' => $request->user()->id,
                'property' => 'rounding_input_amount'
            ],
                ['value' => $request->input('rounding_input_amount')]
            );
            $property->value = $request->input('rounding_input_amount');
            $property->save();
        }

        return redirect()->route('merchant.profile')->with(['alert-type' => 'success', 'message' => __('Profile has been successfully saved!')]);
    }
}
