<?php

namespace App\Http\Controllers;

use App\Lib\Chatex\ChatexApiLBC;
use App\Models\Merchant;
use App\Models\MerchantProperty;
use App\Models\PaymentInvoice;
use App\Models\Property;
use App\Services\AdsSelector;
use App\Services\ContractManager;
use App\Traits\Invoice;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class BaseController extends Controller
{
    use Invoice;

    protected Request $request;
    protected Merchant $merchantModel;
    protected ChatexApiLBC $chatex;
    protected $adsService;
    protected $contractManager = null;

    public function __construct(
        Request $request,
        Merchant $merchantModel,
        AdsSelector $adsService,
        ContractManager $contractManager
    )
    {
        $this->request = $request;
        $this->merchantModel = $merchantModel;
        $chatexLBCProperties = Property::getProperties(ChatexApiLBC::OBJECT_NAME);
        $this->chatex = new ChatexApiLBC($chatexLBCProperties['server'], $chatexLBCProperties['hmac_key']);
        $this->adsService = $adsService;
        $this->contractManager = $contractManager;
    }

    protected function getCurrentMerchant(){
        return $this->request->user();
    }

}
