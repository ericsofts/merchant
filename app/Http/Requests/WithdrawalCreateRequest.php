<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class WithdrawalCreateRequest extends FormRequest
{
    protected $redirectRoute = 'withdrawal.create';
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'amount' => ['required', 'numeric'],
            'payment_method' => ['required', Rule::in(['bank_card'])],
        ];
    }

    protected function prepareForValidation()
    {
        $this->merge([
            'amount' => $this->input('amount', $this->old('amount')),
            'payment_method' => $this->input('payment_method', $this->old('payment_method')),
            'message' => $this->input('message', $this->old('message')),
            'comment' => $this->input('comment', $this->old('comment'))
        ]);
    }
}
