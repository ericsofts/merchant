<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Response;
use function GuzzleHttp\Promise\all;

class LogAfterRequest
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        return $next($request);
    }

    public function terminate(Request $request, $response)
    {
        $data = [];
        foreach ($request->all() as $k => $v){
            $data[$k] = $v instanceof Model ? $v->getAttributes() : $v;
        }
        Log::channel('after-request')->log('info', PHP_EOL , [
            'request' =>[
                'method' => $request->method(),
                'url' => $request->fullUrl(),
                'headers' => $request->headers->all(),
                'request' => $data,
                'content' => $request->getContent(),
            ],
            'response' => [
                'code' => $response->getStatusCode(),
                'response' => json_decode($response->getContent(), true)
            ],
        ]);
    }
}
