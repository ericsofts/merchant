<?php
namespace App\Http\Libs\Api;

use Illuminate\Support\Facades\Storage;

class AdsSelectAvailable extends Base
{
    public function __construct($token = null)
    {
        if (is_null($token))
            $token = config('app.payment_system_host_token');
        parent::__construct($token);
    }

    public function get($params = [])
    {
        $response = $this->httpClient->get(
            config('app.payment_system_host') . '/api/v1/get/ads',
            $this->prepare_params('ads@get')
        );
        if ($response->ok()) {
            return $response->json();
        }
        return false;
    }
}
