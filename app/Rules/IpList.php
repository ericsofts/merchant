<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class IpList implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $list = explode(';',$value);
        $result = true;
        foreach ($list as $ip){
            if(filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4) == false){
                $result = false;
            }
        }
        return $result;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('The :attribute must contain only IP addresses.');
    }
}
