<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\URL;

class PaymentInvoice extends Model
{
    use HasFactory;

    const STATUS_CREATED = 0;
    const STATUS_PAYED = 1;
    const STATUS_USER_CONFIRMED = 2;
    const STATUS_TRADER_CONFIRMED = 3;
    const STATUS_USER_SELECTED = 5;
    const STATUS_CANCELED_BY_TIMEOUT = 98;
    const STATUS_CANCELED = 99;

    const API_TYPE_0 = 0;
    const API_TYPE_1 = 1;
    const API_TYPE_2 = 2;
    const API_TYPE_3 = 3;
    const API_TYPE_4 = 4;
    const API_TYPE_5 = 5;

    protected $fillable = [
        'invoice_number', 'user_id', 'amount', 'amount2pay', 'amount2merchant', 'amount2service', 'amount2agent', 'amount2grow', 'commission_grow',
        'commission_agent', 'commission_service', 'merchant_id', 'otp_id', 'addition_info', 'status', 'payed', 'merchant_order_id', 'merchant_order_desc',
        'merchant_response_url', 'merchant_server_url', 'api_type', 'payment_id', 'checked_at', 'ps_amount', 'ps_currency', 'fiat_amount', 'fiat_currency',
        'input_currency', 'input_rate', 'input_amount_value', 'comments','isMerchant', 'account_info', 'service_provider_id'
    ];

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public static function generateOrderNumber(): int
    {
        $num = mt_rand(100000000000, 999999999999);
        if (self::where('invoice_number', '=', $num)->exists()) {
            return self::generateOrderNumber();
        }

        return $num;
    }

    public function isLinked($merchant){
        if(($this->merchant_id == $merchant->id) && $this->isMerchant && ($this->status == self::STATUS_CREATED)){
            return true;
        }
        return false;
    }

    public function getLink(){
        $url = URL::route('payment_invoice.apply', ['payment_invoice_id' => $this->id]);
        return $url;
    }

    public static function isCancelStatus($status): bool
    {
        return in_array($status, [self::STATUS_CANCELED, self::STATUS_CANCELED_BY_TIMEOUT]);
    }

    public static function getCancelStatuses()
    {
        return [self::STATUS_CANCELED, self::STATUS_CANCELED_BY_TIMEOUT];
    }

    public function service_provider()
    {
        return $this->hasOne(ServiceProvider::class, 'id', 'service_provider_id');
    }

    public function crypto_address()
    {
        return $this->hasOne(CryptoPaymentInvoice::class, 'id', 'payment_id');
    }
}
