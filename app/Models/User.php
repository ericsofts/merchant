<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class User extends Model
{
    use HasFactory;

    const STATUS_BLOCKED = 0;

    protected $hidden = [
        'password',
        'remember_token',
    ];

}
