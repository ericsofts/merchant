<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TraderRate extends Model
{
    use HasFactory;

    public function fiat()
    {
        return $this->hasOne(TraderFiat::class, 'id', 'fiat_id');
    }

    public function currency()
    {
        return $this->hasOne(TraderCurrency::class, 'id', 'currency_id');
    }
}
