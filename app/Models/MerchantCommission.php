<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MerchantCommission extends Model
{
    use HasFactory;

    const API_TYPE_DEFAULT = 0;
    const API_TYPE_API1 = 1;
    const API_TYPE_API2 = 2;
    const API_TYPE_API3 = 3;
    const API_TYPE_API4 = 4;

    const MIN_COMMISSION_TYPE_0 = 0;
    const MIN_COMMISSION_TYPE_1 = 1;
    const MIN_COMMISSION_TYPE_2 = 2;

    const PROPERTY_TYPE_COMMISSION = 1;
    const PROPERTY_TYPE_WITHDRAWAL_COMMISSION = 2;

    protected $fillable = [
        'api_type', 'merchant_id', 'property_type', 'property', 'value'
    ];

    public static function getProperties($merchant_id, $api_type, $type_commission = self::PROPERTY_TYPE_COMMISSION, $service_provider_id = null)
    {
        $properties = [];

        $where = [
            'merchant_id' => $merchant_id,
            'api_type' => $api_type,
            'property_type' => $type_commission
        ];
        if ($service_provider_id) {
            $where['service_provider_id'] = $service_provider_id;
        }
        $properties = self::where($where)->get();

        foreach ($properties as $property) {
            $properties[$property->property] = $property->value;
        }
        return $properties;
    }
}
