<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Trader extends Model
{
    use HasFactory;

    const STATUS_BLOCKED = 0;

    protected $fillable = [
        'name', 'email', 'password', 'status', 'email_verified_at', 'remember_token'
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public static function boot()
    {
        parent::boot();
        self::created(function ($model) {
            foreach (TraderCurrency::where('status', '=', 1)->get() as $currency){
                $balance = TraderBalance::firstOrNew(
                    [
                        'trader_id' =>  $model->id,
                        'currency_id' => $currency->id,
                        'type' => TraderBalance::TYPE_NORMAL
                    ]
                );
                $balance->amount = 0;
                $balance->currency = $currency->asset;
                $balance->save();

                $balance = TraderBalance::firstOrNew(
                    [
                        'trader_id' =>  $model->id,
                        'currency_id' => $currency->id,
                        'type' => TraderBalance::TYPE_FROZEN
                    ]
                );
                $balance->amount = 0;
                $balance->currency = $currency->asset;
                $balance->save();
            }
        });
    }


    public static function generatePassword(): string
    {
        $key = Str::random(8);
        return $key;
    }

    public function getTotalBalance($currency_id = 1, $balanceType = TraderBalance::TYPE_NORMAL){
        $currency = TraderCurrency::find($currency_id);

        $balance = TraderBalance::firstOrCreate([
            'trader_id' => $this->id,
            'type' => $balanceType,
            'currency_id' => $currency_id,
            'currency' => $currency->asset
        ]);

        $balanceQueueCredit = TraderBalanceQueue::where([
            'balance_id' => $balance->id,
            'type' => TraderBalanceQueue::TYPE_DEBIT
        ])
            ->whereNotIn('status', [TraderBalanceQueue::STATUS_DONE, TraderBalanceQueue::STATUS_FAILED])
            ->sum('amount');

        return round($balance->amount - $balanceQueueCredit, 8);
    }

}
