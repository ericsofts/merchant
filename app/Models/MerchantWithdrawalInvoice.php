<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MerchantWithdrawalInvoice extends Model
{
    use HasFactory;

    const STATUS_CREATED = 0;
    const STATUS_PAYED = 1;
    const STATUS_TRADER_CONFIRMED_PAYMENT = 2;
    const STATUS_TRADER_CONFIRMED = 3;
    const STATUS_USER_SELECTED = 5;
    const STATUS_IN_PROCESS = 4;
    const STATUS_CANCELED_BY_TIMEOUT = 98;
    const STATUS_CANCELED = 99;

    const PAYMENT_SYSTEM_MANUALLY = 0;
    const PAYMENT_SYSTEM_CHATEX_LBC = 1;

    const API_TYPE_0 = 0;
    const API_TYPE_3 = 3;

    protected $fillable = [
        'merchant_id', 'amount', 'amount2pay', 'amount2service', 'amount2agent', 'amount2grow', 'commission_grow', 'commission_agent',
        'commission_service', 'addition_info', 'status', 'payed', 'api_type', 'payment_id', 'payment_system', 'checked_at',
        'checked_at', 'ps_amount', 'ps_currency', 'comment', 'fiat_amount', 'fiat_currency', 'account_info', 'merchant_expense', 'service_provider_id'
    ];

    public function attachments()
    {
        return $this->hasMany(MerchantWithdrawalInvoiceAttachment::class, 'invoice_id');
    }

}
