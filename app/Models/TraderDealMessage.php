<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class TraderDealMessage extends Model
{
    use HasFactory;

    const MERCHANT_USER = 0;
    const SD_USER = 1;
    const TRADER_USER = 2;
    const PS_USER = 3;

    protected $fillable = [
        'attachment_type',
        'deal_id',
        'msg',
        'sender_id',
        'sender_type'
    ];



    public function saveFile($file){

        $path = Storage::disk('trader_message_file')->putFile(
            '', $file
        );
        if($path){
            return $path;
        }
        return false;
    }

    /**
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function getFile($name){
        $file = Storage::disk('trader_message_file')->path($name);
        if($file){
            return $file;
        }
        return false;
    }
}
