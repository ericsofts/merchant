<?php


namespace App\Repositories;


use App\Models\Merchant;
use App\Models\MerchantInputInvoice;
use App\Models\MerchantWithdrawalInvoice;
use App\Models\PaymentInvoice;
use Illuminate\Support\Facades\DB;

class TransactionRepository
{

    public function transactions($conditions = [])
    {
        $invoice_types = $conditions['invoice_types'] ?? null;
        $status = $conditions['status'] ?? null;
        $from_date = $conditions['from_date'] ?? null;
        $to_date = $conditions['to_date'] ?? null;
        $merchant_id = $conditions['merchant_id'] ?? null;
        $statuses = [];
        if($status === 0){
            $statuses = [0,2,3];
        }elseif($status == 99){
            $statuses = [99,98];
        }elseif(!empty($status)){
            $statuses = [$status];
        }

        $paymentInvoiceQuery = PaymentInvoice::select([
            'id',
            'merchant_order_id',
            'created_at',
            'amount',
            'amount2pay',
            'status',
            DB::raw("0 AS invoice_type"),
        ]);

        $merchantWithdrawalInvoiceQuery = MerchantWithdrawalInvoice::select([
            'id',
            'merchant_order_id',
            'created_at',
            'amount',
            'amount2pay',
            'status',
            DB::raw("1 as invoice_type"),
        ]);

        $merchantInputInvoiceQuery = MerchantInputInvoice::select([
            'id',
            DB::raw("NULL as merchant_order_id"),
            'created_at',
            'amount',
            DB::raw("amount as amount2pay"),
            'status',
            DB::raw("2 as invoice_type"),
        ]);

        if ($from_date) {
            $paymentInvoiceQuery->where('created_at', '>=', $from_date);
            $merchantWithdrawalInvoiceQuery->where('created_at', '>=', $from_date);
            $merchantInputInvoiceQuery->where('created_at', '>=', $from_date);
        }

        if ($to_date) {
            $paymentInvoiceQuery->where('created_at', '<=', $to_date);
            $merchantWithdrawalInvoiceQuery->where('created_at', '<=', $to_date);
            $merchantInputInvoiceQuery->where('created_at', '<=', $to_date);
        }

        $paymentInvoiceQuery->where('merchant_id', '=', $merchant_id);
        $merchantWithdrawalInvoiceQuery->where('merchant_id', '=', $merchant_id);
        $merchantInputInvoiceQuery->where('merchant_id', '=', $merchant_id);

        if ($statuses) {
            $paymentInvoiceQuery->whereIn('status', $statuses);
            $merchantWithdrawalInvoiceQuery->whereIn('status', $statuses);
            $merchantInputInvoiceQuery->whereIn('status', $statuses);
        }

        if (empty($invoice_types)) {
            $paymentInvoiceQuery->union($merchantWithdrawalInvoiceQuery);
            $paymentInvoiceQuery->union($merchantInputInvoiceQuery);
            return $paymentInvoiceQuery;
        }

        $union = null;
        if (in_array(0, $invoice_types)) {
            $union = $paymentInvoiceQuery;
        }
        if (in_array(1, $invoice_types)) {
            if ($union) {
                $union->union($merchantWithdrawalInvoiceQuery);
            } else {
                $union = $merchantWithdrawalInvoiceQuery;
            }
        }

        if (in_array(2, $invoice_types)) {
            if ($union) {
                $union->union($merchantInputInvoiceQuery);
            } else {
                $union = $merchantInputInvoiceQuery;
            }
        }
        return $union;
    }

    public function transactions_total($conditions = [])
    {
        $invoice_types = $conditions['invoice_types'] ?? null;
        $from_date = $conditions['from_date'] ?? null;
        $to_date = $conditions['to_date'] ?? null;
        $merchant_id = $conditions['merchant_id'] ?? null;

        $paymentInvoiceQuery = PaymentInvoice::where('status', '=', PaymentInvoice::STATUS_PAYED);
        $merchantWithdrawalInvoiceQuery = MerchantWithdrawalInvoice::where('status', '=', PaymentInvoice::STATUS_PAYED);
        $merchantInputInvoiceQuery = MerchantInputInvoice::where('status', '=', PaymentInvoice::STATUS_PAYED);

        if ($from_date) {
            $paymentInvoiceQuery->where('created_at', '>=', $from_date);
            $merchantWithdrawalInvoiceQuery->where('created_at', '>=', $from_date);
            $merchantInputInvoiceQuery->where('created_at', '>=', $from_date);
        }

        if ($to_date) {
            $paymentInvoiceQuery->where('created_at', '<=', $to_date);
            $merchantWithdrawalInvoiceQuery->where('created_at', '<=', $to_date);
            $merchantInputInvoiceQuery->where('created_at', '<=', $to_date);
        }

        $paymentInvoiceQuery->where('merchant_id', '=', $merchant_id);
        $merchantWithdrawalInvoiceQuery->where('merchant_id', '=', $merchant_id);
        $merchantInputInvoiceQuery->where('merchant_id', '=', $merchant_id);

        $amount = $volume = $amount2pay = 0;
        if (!$invoice_types || in_array(0, $invoice_types)) {
            $totalPaymentInvoice = $paymentInvoiceQuery->select(
                DB::raw('SUM(amount) as total_amount'),
                DB::raw('SUM(amount2pay) as total_amount2pay')
            )
                ->first();
            $amount += $totalPaymentInvoice->total_amount;
            $volume += $totalPaymentInvoice->total_amount;
            $amount2pay += $totalPaymentInvoice->total_amount2pay;
        }
        if (!$invoice_types || in_array(1, $invoice_types)) {
            $totalMerchantWithdrawalInvoice = $merchantWithdrawalInvoiceQuery->select(
                DB::raw('SUM(amount) as total_amount'),
                DB::raw('SUM(amount) as total_amount2pay')
            )
                ->first();
            $amount -= $totalMerchantWithdrawalInvoice->total_amount;
            $volume += $totalMerchantWithdrawalInvoice->total_amount;
            $amount2pay -= $totalMerchantWithdrawalInvoice->total_amount2pay;
        }

        if (!$invoice_types || in_array(2, $invoice_types)) {
            $totalPMerchantInputInvoice = $merchantInputInvoiceQuery->select(
                DB::raw('SUM(amount) as total_amount'),
                DB::raw('SUM(amount) as total_amount2pay')
            )
                ->first();
            $amount += $totalPMerchantInputInvoice->total_amount;
            $volume += $totalPMerchantInputInvoice->total_amount;
            $amount2pay += $totalPMerchantInputInvoice->total_amount2pay;
        }

        return [
            'amount' => $amount,
            'volume' => $volume,
            'amount2pay' => $amount2pay,
        ];
    }

}
