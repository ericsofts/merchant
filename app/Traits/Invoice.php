<?php

namespace App\Traits;


use App\Http\Libs\Api\BankBalance;
use App\Lib\Chatex\ChatexApi;
use App\Models\AccountingEntrie;
use App\Models\InvoicesHistory;
use App\Models\MerchantProperty;
use App\Models\Property;
use App\Models\ServiceProvider;
use App\Models\ServiceProvidersProperty;
use App\Services\ContractManager;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;

trait Invoice
{
    use ServiceProviderTrait;

    protected function getAvailableMerchantCoins($merchant_id)
    {
        $available_coins = MerchantProperty::getProperty($merchant_id, 'available_coins', config('app.available_merchant_coins'));
        $available_coins = json_decode($available_coins, true);
        if (!$available_coins) {
            return false;
        }
        return $available_coins;
    }

    protected function checkMinInvoiceAmount($merchant, $amount)
    {
        $result['status'] = true;
        $service_provider = $merchant->default_service_provider();
        $count_service_provider = $merchant->service_providers()->count();
        $min_invoice_amount = config('app.min_payment_invoice_amount');
        if($count_service_provider == 1){
            $min_invoice_amount = MerchantProperty::getProperty($merchant->id, 'min_payment_invoice_amount', $min_invoice_amount, $service_provider->id);
        }else{
            $props_min_amount = MerchantProperty::getPropertiesByName($merchant->id, 'min_payment_invoice_amount');
            foreach ($props_min_amount as $value) {
                $min_invoice_amount = ($value < $min_invoice_amount) ? $value : $min_invoice_amount;
            }
        }

        $available_coins = $mProperties['available_coins'] ?? config('app.available_merchant_coins');
        $available_coins = json_decode($available_coins, true);
        if ($amount < $min_invoice_amount) {
            $result['message'] = __('The minimum amount must be greater than or equal to :amount', ['amount' => $min_invoice_amount]);
            $result['status'] = false;
            return $result;
        }

        if (!$available_coins) {
            $result['message'] = __('Whoops! Something went wrong. Contact Support');
            $result['status'] = false;
            return $result;
        }
        return $result;
    }

    protected function getAvailableUserCoins()
    {
        $available_coins = json_decode(Property::getProperty('users', '0', 'user_available_coins', '[]'), true);

        if (!$available_coins) {
            return false;
        }
        return $available_coins;
    }

    protected function getCoinRate($coin)
    {
        $coin_rate = null;
        if ($coin == 'btc') {
            $rate = Property::get('coinmarketcap2', 0, 'coin_rate');
            $coin_rate = $rate->property_value ?? null;
        } elseif ($coin == 'usdt') {
            //$rate = Property::get('coinmarketcap', 0, 'coin_rate');
            $coin_rate = 1;
        } elseif ($coin == 'usdt_trc20') {
            $coin_rate = 1;
        }
        return $coin_rate;
    }

    protected function getCoinRates($coins)
    {
        $rates = [];
        foreach ($coins as $coin) {
            if ($coin == 'btc') {
                $rates['btc'] = Property::getProperties('coinmarketcap2');
            } elseif ($coin == 'usdt') {
                //$rates['usdt'] = Property::getProperties('coinmarketcap');
                $rates['usdt']['coin_rate'] = 1;
            } elseif ($coin == 'usdt_trc20') {
                $rates['usdt_trc20']['coin_rate'] = 1;
            }
            if (!isset($rates[$coin]) && empty($rates[$coin]['coin_rate'])) {
                return false;
            }
        }

        return $rates;
    }


    protected function isAvalableCurrency($currency)
    {
        $available_currency = Property::getProperty('currency', 0, 'available_currency');
        if (in_array($currency, json_decode($available_currency))) {
            return true;
        }
        return false;
    }

    protected function getCurrencyRate($currency)
    {
        if ($currency == 'RUB') {
            $rate = Property::getProperty('currency_rate', 0, 'rub2usd');
            return floatval($rate);
        } elseif ($currency == 'UAH') {
            $rate = Property::getProperty('currency_rate', 0, 'uah2usd');
            return floatval($rate);
        } elseif ($currency == 'KZT') {
            $rate = Property::getProperty('currency_rate', 0, 'kzt2usd');
            return floatval($rate);
        } elseif ($currency == 'INR') {
            $rate = Property::getProperty('currency_rate', 0, 'inr2usd');
            return floatval($rate);
        } elseif ($currency == 'TRY') {
            $rate = Property::getProperty('currency_rate', 0, 'try2usd');
            return floatval($rate);
        } elseif ($currency == 'USD') {
            return 1;
        } else {
            return 0;
        }
    }

    protected function isAvailableCoins($ad, $available_coins, $invoice = false): bool
    {
        $coin = strtolower($ad['coin']);
        if (!$coin || !$available_coins || !in_array($coin, $available_coins)) {
            if ($invoice) {
                InvoicesHistory::addHistoryRecord(
                    __METHOD__,
                    $invoice->id,
                    AccountingEntrie::getInvoiceTypeByInstance($invoice),
                    1,
                    InvoicesHistory::getStatus(0),
                    InvoicesHistory::getSource(0),
                    InvoicesHistory::getSource(3),
                    json_encode(['ad' => $ad, 'available_coins' => $available_coins]),
                    'Not Available Coin'
                );
                Log::channel($ad['log'])->info('AD|COIN|ERROR|MERCHANT_ID|' . $invoice->merchant_id, ['invoice_id' => $invoice->id, 'ad' => $ad, 'available_coins' => $available_coins]);
            }
            return false;
        }
        return true;

    }

    protected function isValidCardNumber($ad, $invoice = false)
    {
        $currencies_without_card_number = json_decode(ServiceProvidersProperty::getProperty($ad['service_provider_type'], $ad['service_provider_id'], 'currencies_without_card_number', '[]'), true);
        $priority_traders = json_decode(ServiceProvidersProperty::getProperty($ad['service_provider_type'], $ad['service_provider_id'], 'priority_traders', '[]'), true);
        if (($ad['service_provider_type'] != ServiceProvider::CRYPTO_PROVIDER) && empty($ad['card_number']) &&
            (!in_array($ad['currency'], $currencies_without_card_number) || (!$priority_traders || !in_array($ad['name'], $priority_traders)))
        ) {
            if ($invoice) {
                InvoicesHistory::addHistoryRecord(
                    __METHOD__,
                    $invoice->id,
                    AccountingEntrie::getInvoiceTypeByInstance($invoice),
                    1,
                    InvoicesHistory::getStatus(0),
                    InvoicesHistory::getSource(0),
                    InvoicesHistory::getSource(3),
                    json_encode([
                        'ad' => $ad,
                        'currencies_without_card_number' => $currencies_without_card_number,
                        'priority_traders' => $priority_traders
                    ]),
                    'Ad without card number'
                );
                Log::channel($ad['log'])->info('AD|COIN|ERROR|MERCHANT_ID|' . $invoice->merchant_id, ['invoice_id' => $invoice->id, 'ad' => $ad]);
            }
            return false;
        }
        return true;
    }

    protected function isAvalableCoinRate($ad, $invoice = false)
    {
        if (!$coin_rate = $this->getCoinRate(strtolower($ad['coin']))) {
            if ($invoice) {
                InvoicesHistory::addHistoryRecord(
                    __METHOD__,
                    $invoice->id,
                    AccountingEntrie::getInvoiceTypeByInstance($invoice),
                    1,
                    InvoicesHistory::getStatus(0),
                    InvoicesHistory::getSource(0),
                    InvoicesHistory::getSource(3),
                    json_encode(['ad' => $ad, 'coin_rate' => $coin_rate]),
                    'Not Available Coin Rate'
                );
                Log::channel($ad['log'])->info('AD|COIN_RATE|ERROR|MERCHANT_ID|' . $invoice->merchant_id, ['invoice_id' => $invoice->id, 'ad' => $ad, 'coin_rate' => $coin_rate]);
            }
            return false;
        }
        return true;
    }

    protected function getLogChannel($invoice)
    {
        if ($providerId = $invoice->service_provider_id) {
            $provider = $this->getProviderById($providerId);
            switch ($provider->type) {
                case ChatexApi::OBJECT_NAME:
                    return 'chatex_lbc_requests';
                case ServiceProvider::TRADER_PROVIDER:
                    return 'trader_requests';
            }
        }
        return '';


    }

    private function getGrowTokenRate()
    {
        return Cache::remember('bank_host_api_cache_uniswap_rate', config('app.bank_host_api_cache', 30), function () {
            $assetResponce = $this->growBankApi->getAssetsInfo();
            if ($assetResponce) {
                Property::setProperty('uniswap', '0', 'rate', $assetResponce['uniswap_price'] / 1000000000000000000000000);
            }
            $rate = Property::getProperty('uniswap', '0', 'rate', 0);
            return $rate;
        });
    }

    protected function setLocaleByCurrency($currency = null)
    {
        $locale = match ($currency) {
            'RUB', 'UAH', 'KZT' => 'ru',
            'TRY', 'INR' => 'en',
            default => config()->get('app.locale'),
        };
        session()->put('locale', $locale);
        app()->setLocale($locale);
    }

}
