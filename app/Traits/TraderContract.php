<?php

namespace App\Traits;

use App\Models\AccountingEntrie;
use App\Models\InputInvoice;
use App\Models\InvoicesHistory;
use App\Models\MerchantWithdrawalInvoice;
use App\Models\PaymentInvoice;
use App\Models\ServiceProvider;
use App\Models\TraderAd;
use App\Models\TraderBalance;
use App\Models\TraderDeal;
use App\Models\TraderDealHistory;
use App\Models\TraderDealMessage;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

trait TraderContract
{
    use TraderAds;

    private $invoice;

    public function createTraderContract($ad, $currency_amount, $user = false, $invoice = false, $message = null, $payment_info = null)
    {

        $trader_ad = TraderAd::where(['id' => $ad['id'], 'status' => TraderAd::STATUS_ACTIVE])->first();

        if ($trader_ad && $user) {
            if ($trader_ad->min_amount > $currency_amount || $trader_ad->max_amount < $currency_amount) {
                Log::channel($ad['log'])->info('CONTACT_CREATE|CREATE|ERROR|MERCHANT_ID|' . $user->id, ['message' => 'Your amount is not in ad limits', 'ad' => $ad]);
                return false;
            }

            if($ad['type'] != 'BUY'){
                $trader_balance = $trader_ad->trader->getTotalBalance($trader_ad->currency_id, TraderBalance::TYPE_NORMAL);
                if (!$trader_balance || $trader_balance * $ad['temp_price'] < $currency_amount) {
                    Log::channel($ad['log'])->info('CONTACT_CREATE|CREATE|ERROR|MERCHANT_ID|' . $user->id, [
                        'message' => "Insufficient funds on the trader's balance",
                        'ad' => $ad,
                        'trader_balance' => $trader_balance,
                        'currency_amount' => $currency_amount
                    ]);
                    return false;
                }
            }

            $data = [
                'description' => 'create deal',
                'customer_id' => $user->id,
                'ad_id' => $trader_ad->id,
                'trader_id' => $trader_ad->trader_id,
                'type' => $trader_ad->type,
                'fiat_id' => $trader_ad->fiat_id,
                'currency_id' => $trader_ad->currency_id,
                'payment_system_id' => $trader_ad->payment_system_id,
                'card_number' => $trader_ad->card_number,
                'payment_info' => $trader_ad->payment_info,
                'amount_currency' => $currency_amount / $ad['temp_price'],
                'amount_fiat' => $currency_amount,
                'rate' => $ad['temp_price'],
                'status' => TraderDeal::STATUS_CREATE,
                'ad_json' => json_encode($trader_ad),
                'customer_type' => TraderDeal::getCustomerTypeByUser($user)
            ];

            if ($trader_ad->type == TraderDeal::TYPE_BUY) {
                $data['card_number'] = $message;
                $data['payment_info'] = $payment_info;
            }

            $traderDeal = TraderDeal::create($data);
        }

        if (isset($traderDeal) && $traderDeal) {
            return ['id' => $traderDeal->id, 'service_provider_type' => $ad['service_provider_type']];
        } elseif ($invoice) {
            InvoicesHistory::addHistoryRecord(
                __METHOD__,
                $invoice->id,
                AccountingEntrie::INVOICE_TYPE_PAYMENT_INVOICE,
                1,
                InvoicesHistory::getStatus(0),
                InvoicesHistory::getSource(0),
                InvoicesHistory::getSource(2),
                json_encode($traderDeal ?? null),
                'Service Provider response incorrect'
            );
            Log::channel($ad['log'])->info('CONTACT_CREATE|CREATE|ERROR|MERCHANT_ID|' . $invoice->merchant_id, ['deal' => $traderDeal ?? null]);
        }
        return false;
    }

    public function infoTraderContract($id)
    {

        $contractInfo = DB::table('trader_deals')
            ->join('trader_currencies as currencies', 'trader_deals.currency_id', '=', 'currencies.id')
            ->join('trader_ads as ads', 'trader_deals.ad_id', '=', 'ads.id')
            ->join('trader_payment_systems as payments', 'trader_deals.payment_system_id', '=', 'payments.id')
            ->join('traders as traders', 'trader_deals.trader_id', '=', 'traders.id')
            ->join('trader_fiats as fiats', 'trader_deals.fiat_id', '=', 'fiats.id')
            ->join('trader_deal_histories as history', 'trader_deals.last_history_id', '=', 'history.id')
            ->select(
                'trader_deals.*',
                'currencies.asset as coin',
                'payments.name as bank_name',
                'fiats.name as fiat_name',
                'fiats.asset as currency',
                'traders.name as name',
                'history.status as history_status',
                'history.created_at as history_created_at'
            )
            ->where('trader_deals.id', '=', $id)
            ->first();

        $funded_at = null;
        if ($historyFunded = TraderDealHistory::where(['deal_id' => $id, 'status' => TraderDeal::STATUS_TRADER_FUNDED])->first()) {
            $funded_at = $historyFunded->created_at;
        }
        $payment_completed_at = null;
        if ($historyCompleted = TraderDealHistory::where(['deal_id' => $id, 'status' => TraderDeal::STATUS_COMPLETED])->first()) {
            $payment_completed_at = $historyCompleted->created_at;
        }

        if ($contractInfo) {
            if (($contractInfo->status == TraderDeal::STATUS_CANCELED_SD) ||
                ($contractInfo->status == TraderDeal::STATUS_CANCELED_TRADER) ||
                ($contractInfo->status == TraderDeal::STATUS_CANCELED_USER) ||
                ($contractInfo->status == TraderDeal::STATUS_CANCELED)) {
                $status = self::STATUS_CANCEL;
            } elseif ($contractInfo->status == TraderDeal::STATUS_COMPLETED) {
                $status = self::STATUS_COMPLETE;
            } elseif (($contractInfo->status == TraderDeal::STATUS_CANCELED_BY_REPLY_TIMEOUT) ||
                ($contractInfo->status == TraderDeal::STATUS_CANCELED_TIMEOUT)) {
                $status = self::STATUS_CLOSED;
            } elseif ($contractInfo->status == TraderDeal::STATUS_TRADER_FUNDED) {
                $status = self::STATUS_FUNDED;
                $this->updateInvoiceStatusByTrader($contractInfo, $status, $id, $funded_at, $payment_completed_at);
            } elseif ($contractInfo->status == TraderDeal::STATUS_DISPUTED) {
                $status = self::STATUS_DISPUTED;
            } elseif (in_array($contractInfo->status, [TraderDeal::STATUS_PAYMENT_COMPLETED_TRADER, TraderDeal::STATUS_PAYMENT_COMPLETED_TRADER])) {
                $status = self::STATUS_PAYMENT_COMPLETED;
            } else {
                $status = self::STATUS_DEFAULT;
            }

            return $this->getFormatedTraderInfo($contractInfo, $status, $funded_at, $payment_completed_at);
        }
        return false;

    }

    public function cancelTraderContract($id)
    {
        $deal = TraderDeal::find($id);
        if ($deal) {
            $deal->status = TraderDeal::STATUS_CANCELED_USER;
            $deal->description = 'Change status to Canceled User';
            $deal->customer_type = TraderDeal::MERCHANT_USER;
            $deal->save();
            return true;
        }
        return false;
    }

    public function markAsPaidTraderContract($id)
    {
        $deal = TraderDeal::find($id);
        if ($deal) {
            $deal->status = TraderDeal::STATUS_PAYMENT_COMPLETED_USER;
            $deal->description = 'Change status to Mark as Paid';
            $deal->customer_type = TraderDeal::MERCHANT_USER;
            $deal->save();
            return true;
        }
        return false;
    }

    public function releaseTraderContract($id)
    {
        $deal = TraderDeal::find($id);
        if ($deal) {
            $deal->status = TraderDeal::STATUS_COMPLETED;
            $deal->description = 'Change status to Completed';
            $deal->save();
            return true;
        }
        return false;
    }

    public function messagePostTraderContract($document)
    {
        $path = Storage::disk('trader_message_file')->putFile(
            '', $document
        );
//        $path = $this->dealMessageModel->saveFile($document);
        if ($path) {
            return $path;
        }
        return false;
    }

    public function messagePostRemoteTraderContract($document)
    {

        $filePath = $this->traderClient->saveFile($document, $document->getClientOriginalName());

        if ($filePath) {
            return $filePath;
        }
        return false;
    }

    public function contactMessagesTraderContract($id, $remote)
    {
        $dealMessages = TraderDealMessage::where('deal_id', '=', $id)->orderBy('created_at', 'desc')->get();
        $messages = [];
        foreach ($dealMessages as $message) {
            if($remote){
                $url = $message->attachment_type == 1 ? $this->traderClient->getPath(['name' => $message->msg]): null;
            }else{
                $url = $message->attachment_type == 1 ? $this->dealMessageModel->getFile($message->msg): null;
            }
            $messages[] = [
                'id' => $message->id,
                'msg' => $message->msg,
                'attachment_type' => $message->attachment_type == 1 ? 'image' : null,
                'attachment_url' => $url,
                'created_at' => $message->created_at
            ];
        }
        return $messages;
    }

    public function getFormatedTraderInfo($contractInfo, $status, $funded_at, $payment_completed_at){
        return [
            'id' => $contractInfo->id,
            'amount_btc' => $contractInfo->amount_currency,
            'amount' => round($contractInfo->amount_fiat, 2),
            'currency' => $contractInfo->currency,
            'status' => $status,
            'funded_at' => $funded_at ?? null,
            'payment_completed_at' => $payment_completed_at ?? null,
            'account_info' => $contractInfo->card_number ?? null,
            'payment_info' => $contractInfo->payment_info,
        ];
    }

    public function updateInvoiceStatusByTrader($contractInfo, $status, $id, $funded_at, $payment_completed_at){
            $invoice = $this->getInvoiceToTrader($id, $contractInfo);
            if($invoice && ($invoice->status != $invoice::class::STATUS_TRADER_CONFIRMED) ) {
                if ($contractInfo->type == TraderDeal::TYPE_SELL) {
                    $contract = $this->getFormatedTraderInfo($contractInfo, $status, $funded_at, $payment_completed_at);
                    $ad = $this->getTraderAd($contractInfo->ad_id);
                    if ($ad) {
                        $card_number = $ad['card_number'];
                        $invoice->addition_info = json_encode([
                            'contact' => $contract,
                            'ad' => $ad
                        ]);
                        if ($invoice->getConnection()
                            ->getSchemaBuilder()
                            ->hasColumn($invoice->getTable(), 'account_info')) {
                            $invoice->account_info = $card_number;
                        }
                        $invoice->status = $invoice::class::STATUS_TRADER_CONFIRMED;
                        $invoice->save();
                    }
                } elseif ($contractInfo->type == TraderDeal::TYPE_BUY) {
                    $invoice->status = $invoice::class::STATUS_TRADER_CONFIRMED;
                    $invoice->save();
                }
            }
    }

    public function getInvoiceToTrader($id, $contractInfo){
        if(!$this->invoice || ($this->invoice->payment_id != $id)){
            $serviceProviderIds = ServiceProvider::where('type', ServiceProvider::TRADER_PROVIDER)->pluck('id')->toArray();
            if(count($serviceProviderIds)){
                if($contractInfo->type == TraderDeal::TYPE_SELL){
                    $this->invoice = PaymentInvoice::where('payment_id', $id)->whereIn('service_provider_id', $serviceProviderIds)->first();
                    if(!$this->invoice){
                        $this->invoice = InputInvoice::where('payment_id', $id)->whereIn('service_provider_id', $serviceProviderIds)->first();
                    }
                }elseif ($contractInfo->type == TraderDeal::TYPE_BUY){
                    $this->invoice = MerchantWithdrawalInvoice::where('payment_id', $id)->whereIn('service_provider_id', $serviceProviderIds)->first();
                }

            }
        }
        return $this->invoice;
    }
}
