<?php

namespace App\Traits;

use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

trait UploadTrait
{
    public function uploadOne(UploadedFile $uploadedFile, $folder = null, $disk = 'public', $filename = null, $copy = false)
    {
        $name = !is_null($filename) ? $filename : Str::random(25);

        $file = $uploadedFile->storeAs($folder, $name.'.'.$uploadedFile->getClientOriginalExtension(), $disk);

        if($copy){
            $this->copyImage($file);
        }

        return $file;
    }

    public function copyImage($file)
    {
        $path = config('whitelabel.copy_path').'/storage/app/public/whitelabel/';
        if (!File::exists($path)) {
            $result = File::makeDirectory($path);
        }

        File::copy(storage_path('app/public/whitelabel/').$file, $path.$file);

    }
}
