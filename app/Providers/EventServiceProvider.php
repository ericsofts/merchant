<?php

namespace App\Providers;

use App\Models\MerchantProperty;
use App\Models\MerchantWithdrawalInvoice;
use App\Models\TraderDeal;
use App\Observers\MerchantPropertyObserver;
use App\Observers\MerchantWithdrawalInvoiceObserver;
use App\Observers\TraderDealObserver;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        MerchantWithdrawalInvoice::observe(MerchantWithdrawalInvoiceObserver::class);
        MerchantProperty::observe(MerchantPropertyObserver::class);
        TraderDeal::observe(TraderDealObserver::class);
    }
}
