<?php


namespace App\Lib\Chatex;


use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class ChatexApi
{

    const OBJECT_NAME = 'chatex';
    const ORDER_STATUS_UNASSIGNED = 'UNASSIGNED'; //новый
    const ORDER_STATUS_ACTIVE = 'ACTIVE'; // пользователь перешел в чат
    const ORDER_STATUS_COMPLETED = 'COMPLETED'; // оплачен
    const ORDER_STATUS_CANCELED = 'CANCELED'; // отменен

    private const CHATEX_FILE = "chatex.txt";
    protected $base_url;  // URL for API requests
    protected $refresh_token; // For refresh access token
    protected $access_token; //For requests

    public function __construct()
    {
        $params = func_get_args();
        switch (count($params)) {
            case 2:
                $this->base_url = $params[0];
                $this->refresh_token = $params[1];
                break;
            default:
                throw new \Exception("Invalid constructor. Please refer to the documentation or technical support.");
        }
    }

    public function get_coins()
    {
        return $this->httpRequest(
            'GET',
            [],
            'coins'
        );
    }

    public function get_coin($id)
    {
        return $this->httpRequest(
            'GET',
            [],
            'coins' . '/' . $id
        );
    }

    public function get_payment_systems($params)
    {

        return $this->httpRequest(
            'POST',
            $params,
            'payment-systems/estimate'
        );
    }

    public function get_payment_system($id)
    {
        return $this->httpRequest(
            'GET',
            [],
            'payment-systems/' . $id
        );
    }

    public function get_invoices()
    {
        return $this->httpRequest(
            'GET',
            [],
            'invoices'
        );
    }

    public function get_invoice($id)
    {
        return $this->httpRequest(
            'GET',
            [],
            'invoices/' . $id
        );
    }

    public function create_invoice($params)
    {
        return $this->httpRequest(
            'POST',
            $params,
            'invoices'
        );
    }

    public function buyBitcoinsOnline()
    {
        return $this->httpRequest(
            'GET',
            [],
            'sell-bitcoins-online/.json'
        );
    }

    public function wallet($coin = null)
    {
        try{
            $wallets = $this->httpRequest(
                'GET',
                [],
                'wallet'
            );
            if ($coin) {
                if (!empty($wallets)) {
                    foreach ($wallets as $wallet) {
                        if (!empty($wallet['coin']) && $coin == $wallet['coin']) {
                            return $wallet;
                        }
                    }
                }
                return null;
            }
            return $wallets;
        }catch (Exception $e){
            return null;
        }
    }

    public function walletHistory($offset = 0, $limit = 50, $types = null)
    {
        $url = "wallet/history?offset=$offset&limit=$limit";
        if ($types) {
            $url .= "&types=$types";
        }
        return $this->httpRequest(
            'GET',
            [],
            $url
        );
    }

    private function httpRequest($request_type, array $params = [], $url = 'application-data', $refresh_token = false)
    {
        if (function_exists('curl_init') === false) {
            throw new \Exception("cURL is not installed.");
        }

        $uri = $this->base_url . $url;
        $headers = [];

        if ($refresh_token == true) {
            $headers[] = 'Authorization: Bearer ' . $this->refresh_token;
            $headers[] = 'Content-Type: application/json';
        } else {
            if ($this->needRefreshToken()) {
                $this->httpRequest(
                    'POST',
                    [],
                    'auth/access-token',
                    true
                );
            }

            $headers[] = 'Authorization: Bearer ' . $this->access_token;
            $headers[] = "Content-Type: application/json";
        }
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $uri,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $request_type,
            CURLOPT_POSTFIELDS => !empty($params) ? json_encode($params, JSON_HEX_QUOT) : '',
            CURLOPT_HTTPHEADER => $headers,
        ));

        $output = curl_exec($curl);

        $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        if (curl_errno($curl)) {
            throw new \Exception(curl_error($curl));
        }

        if ($status != 200 && $status != 201) {
            $this->problemWithAnswer($status, $output);
        }

        curl_close($curl);

        $answer = json_decode($output, true);

        if ($refresh_token) {
            $this->access_token = $answer['access_token'];
            Storage::put(self::CHATEX_FILE, $output);
        }

        return $answer;

    }

    private function problemWithAnswer($status, $output)
    {
        switch ($status) {
            case 400:
                // Check data
                return json_decode($output, true);
            case 401:
                // get new token and save it
                $this->httpRequest(
                    'POST',
                    [],
                    'auth/access-token',
                    true
                );
                break;
            case 403:
                // Contact Support
                Log::channel('chatex')->info(print_r($output, true) . 'Status: '.$status);
                throw new \Exception("App inactive.");

            default:
                // do something
                Log::channel('chatex')->info(print_r($output, true) . 'Status: '. $status);
                throw new \Exception("Error. HTTP code: " . $status . '. Output: ' . print_r($output, true), $status);
        }
    }

    private function needRefreshToken()
    {
        if (Storage::exists(self::CHATEX_FILE)) {
            $contents = Storage::get(self::CHATEX_FILE);

            try {
                $context = json_decode($contents);
                $current_timestamp = Carbon::now()->timestamp;
                if ($current_timestamp > $context->expires_at) {
                    return true;
                } else {
                    $this->access_token = $context->access_token;
                    return false;
                }
            } catch (\Exception $exception) {
                Log::error($exception->getMessage());
                return true;
            }
        } else {
            return true;
        }
    }

    private function prepare_params($params)
    {
        return !empty($params) ? [json_encode($params, JSON_HEX_QUOT)] : '';
    }

}
