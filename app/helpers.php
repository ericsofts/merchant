<?php

use App\Lib\LuhnAlgorithm;
use App\Models\MerchantProperty;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;

if (! function_exists('price_format')) {
    function price_format($price)
    {
        //return str_replace('.00', '', number_format($price, 2, '.', ''));
        return number_format($price, 2, '.', '')    ;
    }
}

if (!function_exists('input_invoice_status')) {
    function input_invoice_status($status)
    {
        $status_name = '';
        switch ($status){
            case 0:
                $status_name = __('Created');
                break;
            case 99:
                $status_name = __('Canceled');
                break;
            case 2:
                $status_name = __('Confirmed');
                break;
            case 1:
                $status_name = __('Paid');
                break;
        }
        return $status_name;
    }
}

if (!function_exists('payment_invoice_status')) {
    function payment_invoice_status($status)
    {
        $status_name = '';
        switch ($status){
            case 0:
                $status_name = __('Created');
                break;
            case 99:
                $status_name = __('Canceled');
                break;
            case 98:
                $status_name = __('Expired');
                break;
            case 2:
                $status_name = __('Confirmed');
                break;
            case 1:
                $status_name = __('Paid');
                break;
        }
        return $status_name;
    }
}

if (!function_exists('datetimeFormat')) {
    function datetimeFormat($datetime = null, $format = null)
    {
        if (!$datetime) {
            return;
        }
        if (!$format) {
            $format = 'd.m.Y H:i:s';
        }
        try {
            return Carbon::parse($datetime)->setTimezone(config('app.default.timezone'))->format($format);
        } catch (Throwable $t) {
            return $datetime;
        }
    }
}

if (! function_exists('isAvalableInvoiceAdd')) {
    function isAvalableInvoiceAdd()
    {
        $mProperties = MerchantProperty::getProperties(Auth::user()->id);
        $availableChantexCoins = json_decode($mProperties['available_coins'] ?? config('app.available_merchant_coins'), true);
        if (!$availableChantexCoins) {
            return false;
        }
        return true;
    }
}

if (!function_exists('invoice_type')) {
    function invoice_type($type)
    {
        $name = '';
        switch ($type) {
            case 0:
                $name = __('Payment invoice');
                break;
            case 1:
                $name = __('Withdrawal');
                break;
            case 2:
                $name = __('Input');
                break;
        }
        return $name;
    }
}

if (!function_exists('invoice_status')) {
    function invoice_status($status)
    {
        $status_name = '';
        switch ($status) {
            case 0:
                $status_name = __('Created');
                break;
            case 99:
                $status_name = __('Canceled');
                break;
            case 98:
                $status_name = __('Canceled by timeout');
                break;
            case 2:
                $status_name = __('Confirmed');
                break;
            case 3:
                $status_name = __('Confirmed (Trader)');
                break;
            case 1:
                $status_name = __('Paid');
                break;
        }
        return $status_name;
    }
}

if (!function_exists('mask_credit_card')) {
    function mask_credit_card($string, $replacement = "*")
    {
        $regex = '/(?:\d[ \t-]*?){13,19}/m';
        $matches = [];
        preg_match_all($regex, $string, $matches);

        if (!isset($matches[0]) || empty($matches[0])) {
            return $string;
        }
        $luhn = new LuhnAlgorithm();
        foreach ($matches as $matchGroup) {
            foreach ($matchGroup as $match) {
                $strippedMatch = preg_replace('/[^\d]/', '', $match);
                if (false === $luhn->isValid($strippedMatch)) {
                    continue;
                }
                $cardLength = strlen($strippedMatch);
                $replacement_string = substr($strippedMatch, 0, 6) . str_pad('', $cardLength - 10, $replacement) . substr($strippedMatch, -4);
                $string = str_replace($match, $replacement_string, $string);
            }
        }
        return $string;
    }
}
