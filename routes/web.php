<?php

use App\Http\Controllers\AccountSettingsController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\IpSecurityController;
use App\Http\Controllers\MerchantController;
use App\Http\Controllers\PaymentInvoiceController;
use App\Http\Controllers\TransactionController;
use App\Http\Controllers\TwoFAController;
use App\Http\Controllers\AdsSelectController;
use App\Http\Controllers\WhiteLabelController;
use App\Http\Controllers\WithdrawalController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('login');
});

// Locale
Route::get('setlocale/{locale}', function ($locale) {
    if (in_array($locale, config()->get('app.locales'))) {
        session()->put('locale', $locale);
    }
    return redirect()->back()->cookie('locale', $locale, 60*24*360);
});

Route::middleware(['auth', 'verified'])->group(function() {
    Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard');
    Route::get('/invoice-history', [PaymentInvoiceController::class, 'index'])->name('payment_invoice.index');
    Route::get('/transactions', [TransactionController::class, 'allInvoices'])->name('transactions.all_invoices');

    Route::get('/payment/add', [PaymentInvoiceController::class, 'addPayment'])->name('payment_invoice.add');
    Route::post('/payment/checkout/{payment_method}', [PaymentInvoiceController::class, 'checkoutPayment'])->name('payment_invoice.checkout');
    Route::post('/payment/checkout', [PaymentInvoiceController::class, 'checkoutPayment'])->name('payment_invoice.checkout');
    Route::get('/payment/checkout', [PaymentInvoiceController::class, 'checkoutPayment'])->name('payment_invoice.checkout');
    Route::get('/payment/apply/{payment_invoice_id}', [PaymentInvoiceController::class, 'checkoutApply'])->name('payment_invoice.apply');
    Route::post('/payment/confirm', [PaymentInvoiceController::class, 'checkoutConfirm'])->name('payment_invoice.confirm');
    Route::post('/payment/check', [PaymentInvoiceController::class, 'checkoutStatusCheck'])->name('payment_invoice.check');
    Route::post('/payment/status', [PaymentInvoiceController::class, 'checkoutStatus'])->name('payment_invoice.status');
    Route::get('/payment/qr/{payment_invoice_id?}', [PaymentInvoiceController::class, 'getQr'])->name('payment_invoice.qr');
    Route::get('/payment/cancel', [PaymentInvoiceController::class, 'invoicePostCheckoutDecline'])->name('payment_invoice.cancel');
    Route::get('/payment/success', [PaymentInvoiceController::class, 'invoicePostCheckoutComplete'])->name('payment_invoice.success');

    Route::get('/withdrawal/create/bank-card/confirm/{id}', [WithdrawalController::class, 'chatex_lbc_contact_confirm_show'])->name('withdrawal.create.chatex_lbc_contact_confirm_show');
    Route::post('/withdrawal/create/bank-card/confirm', [WithdrawalController::class, 'chatex_lbc_contact_confirm'])->name('withdrawal.create.chatex_lbc_contact_confirm');
    Route::post('/withdrawal/create/bank-card/check/{id}', [WithdrawalController::class, 'chatex_lbc_contact_check'])->name('withdrawal.create.chatex_lbc_contact_check');
    Route::post('/withdrawal/create/bank-card', [WithdrawalController::class, 'chatex_lbc_contact_create'])->name('withdrawal.create.chatex_lbc_contact_create');
    Route::get('/withdrawal/create/{payment_method}', [WithdrawalController::class, 'paymentMethod'])->name('withdrawal.create.payment_method1');

    Route::get('/merchant/available_ads', [AdsSelectController::class, 'index'])->name('merchant.available_ads');

    Route::post('/withdrawal/create', [WithdrawalController::class, 'paymentMethod'])->name('withdrawal.create.payment_method');
    Route::get('/withdrawal/attachment/{id}', [WithdrawalController::class, 'attachment'])->name('withdrawal.attachment');
    Route::get('/withdrawal/{id}', [WithdrawalController::class, 'view'])->name('withdrawal.view');
    Route::get('/withdrawal', [WithdrawalController::class, 'create'])->name('withdrawal.create');

    Route::get('/withdrawal-history', [WithdrawalController::class, 'index'])->name('withdrawal.index');
    Route::get('/profile', [MerchantController::class, 'profile'])->name('merchant.profile');
    Route::patch('/profile', [MerchantController::class, 'profile_update'])->name('merchant.profile.update');

    Route::get('2fa', [TwoFAController::class, 'index'])->name('2fa.index');
    Route::get('2fa/status', [TwoFAController::class, 'status'])->name('2fa.status');
    Route::post('2fa/configure', [TwoFAController::class, 'configure'])->name('2fa.configure');
    Route::post('2fa/disable', [TwoFAController::class, 'disable'])->name('2fa.disable');
    Route::post('2fa/generate_new', [TwoFAController::class, 'generateNew'])->name('2fa.generate_new');

    Route::get('/whitelabel', [WhiteLabelController::class, 'whitelabel'])->name('merchant.whitelabel');
    Route::get('/whitelabel/preview', [WhiteLabelController::class, 'preview'])->name('merchant.whitelabel.preview');
    Route::patch('/whitelabel', [WhiteLabelController::class, 'whitelabel_update'])->name('merchant.whitelabel.update');

    Route::patch('/ipsecurity/update', [IpSecurityController::class, 'update'])->name('merchant.ip_security.update');
    Route::get('/ipsecurity', [IpSecurityController::class, 'index'])->name('merchant.ip_security');

    Route::post('/settings/account', [AccountSettingsController::class, 'update'])->name('merchant.settings.account');
    Route::get('/settings/account', [AccountSettingsController::class, 'index'])->name('merchant.settings.account');
});

require __DIR__.'/auth.php';
