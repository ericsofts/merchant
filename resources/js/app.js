import {Modal} from 'bootstrap';
import $ from 'jquery';
import 'daterangepicker'
import moment from 'moment';
import Swal from 'sweetalert2'
window.toastr = require('toastr');
//window.$ = window.jQuery = $;
window.$ = window.jQuery = require('jquery');
window.Swal = Swal;
import 'bootstrap-colorpicker/dist/js/bootstrap-colorpicker';

require('select2')

$(function () {

    $('.select2').select2({
        width: 'resolve',
        theme: 'bootstrap-5',
    })

    $('.datepicker').daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        autoApply: true,
        timePicker:true,
        timePicker24Hour:true,
        minYear: 1901,
        maxYear: parseInt(moment().add(1, 'years').format('YYYY'), 10),
        autoUpdateInput: false,
        locale: {
            format: 'YYYY-MM-DD HH:mm',
        }
    }, function (start, end, label) {

    });

    $('.datepicker').on('apply.daterangepicker', function (ev, picker) {
        $(this).val(picker.startDate.format('YYYY-MM-DD HH:mm'));
    });

    $('.datepicker').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
    });
    $('#fiat_name').change(function () {
        $('#payment_system_id').find('option').remove();
        var fiat = $(this).val();
        if (FiatsArray[fiat]) {
            FiatsArray[fiat].forEach(function (estimate) {
                $('#payment_system_id')
                    .append($("<option></option>")
                        .attr("value", estimate.payment_system_id)
                        .text(estimate.payment_system_name + ' - ' + estimate.fiat_amount));
            });
        }
    });

    $('#currency').change(function () {
        $('#payment_system_id').find('option').remove();
        var fiat = $(this).val();
        if (FiatsArray[fiat]) {
            FiatsArray[fiat].forEach(function (estimate) {
                $('#payment_system_id')
                    .append($("<option></option>")
                        .attr("value", estimate.payment_system_id)
                        .text(estimate.payment_system_name + ' - ' + estimate.fiat_amount));
            });
        }
    });

    $(document).on('change', '.enable-2fa', function (event) {
        event.preventDefault();
        var that = this;
        if (this.checked) {
            $('.block-form-2fa').show();
            $('#enable_2fa').val(1);
        } else {
            $(that).attr('disabled', true);
            $.ajax({
                url: '/2fa/status',
                type: 'GET'
            }).done(function (data) {
                if (data.status) {
                    $(that).prop('checked', true);
                    var modal = new Modal(document.getElementById('modal-2fa-disable'))
                    modal.show()
                } else {
                    $('.block-form-2fa').hide();
                }
            }).always(function () {
                $(that).attr('disabled', false);
            })
        }
        return false;
    })

    $(document).on('submit', 'form#disable-2fa', function (event) {
        event.preventDefault();
        var form = $(this).closest('.modal-content').find('form');
        var that = this;
        $(that).attr('disabled', true);
        $.ajax({
            url: form.attr('action'),
            type: form.attr('method'),
            data: form.serialize()
        }).done(function (data) {
            if (data.status) {
                toastr.success(data.message);
                window.location.reload(true);
            } else {
                toastr.error(data.message);
                $.each(data.errors, function (index, value) {
                    form.find('#' + index).addClass('is-invalid');
                    form.find('#' + index).siblings('.invalid-feedback').text(value[0]);
                });
            }
        }).always(function () {
            $(that).attr('disabled', false);
            form.find("input[type=text]").val("");
        })
        return false;
    });

    $(document).on('click', '.btn-regenerate-2fa', function (event) {
        event.preventDefault();
        var modal = new Modal(document.getElementById('modal-2fa-generate'))
        modal.show()
        return false;
    })
    $(document).on('submit', 'form#2fa-generate', function (event) {
        event.preventDefault();
        var form = $(this).closest('.modal-content').find('form');
        var that = this;
        $(that).attr('disabled', true);
        $.ajax({
            url: form.attr('action'),
            type: form.attr('method'),
            data: form.serialize()
        }).done(function (data) {
            if (data.status) {
                toastr.success(data.message);
                window.location.reload(true);
            } else {
                toastr.error(data.message);
                $.each(data.errors, function (index, value) {
                    form.find('#' + index).addClass('is-invalid');
                    form.find('#' + index).siblings('.invalid-feedback').text(value[0]);
                });
            }
        }).always(function () {
            $(that).attr('disabled', false);
            form.find("input[type=text]").val("");
        })
        return false;
    });
});

