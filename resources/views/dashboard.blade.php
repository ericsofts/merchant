@extends('layouts.app')
@section('content')
    <div class="page-header">
        <h1>
            {{ __('Dashboard') }}
        </h1>
    </div>
    <p>{{__('Welcome')}}, {{request()->user()->name}}</p>

    <div class="row row-cols-1 row-cols-md-3 g-4">
        <div class="col">
            <div class="card text-white bg-dark mb-3">
                <div class="card-body">
                    <h5 class="card-title">{{__('Balance')}}</h5>
                    <p class="card-text fs-2">${{price_format($balance_total)}}</p>
                </div>
            </div>
        </div>
{{--        <div class="col">--}}
{{--            <div class="card text-white bg-dark mb-3">--}}
{{--                <div class="card-body">--}}
{{--                    <h5 class="card-title">&nbsp;</h5>--}}
{{--                    <p class="card-text fs-2">&nbsp;</p>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="col">--}}
{{--            <div class="card text-white bg-dark mb-3">--}}
{{--                <div class="card-body">--}}
{{--                    <h5 class="card-title">&nbsp;</h5>--}}
{{--                    <p class="card-text fs-2">&nbsp;</p>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
    </div>
    @if($invoice_histories)
        <p>{{__('Invoice history')}}</p>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">{{__('Invoice ID')}}</th>
                <th scope="col">{{__('Order ID')}}</th>
                <th scope="col">{{__('Date')}}</th>
                <th scope="col">{{__('Amount')}}</th>
                <th scope="col">{{__('Amount to pay')}}</th>
                <th scope="col">{{__('Status')}}</th>
            </tr>
            </thead>
            <tbody>
            @foreach($invoice_histories as $i => $item)
                <tr>
                    <th scope="row">{{$i+1}}</th>
                    <td>{{$item->id}}</td>
                    <td>{{$item->merchant_order_id}}</td>
                    <td>{{datetimeFormat($item->created_at)}}</td>
                    <td>{{$fmt->formatCurrency(price_format($item->amount), "USD")}}</td>
                    <td>
                        @if($item->status == 1)
                            {{$fmt->formatCurrency(price_format($item->amount2pay), "USD")}}
                        @endif
                    </td>
                    <td>{{payment_invoice_status($item->status)}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div class="d-grid gap-2 col-12 mx-auto">
            <a href="{{route('payment_invoice.index')}}" class="btn btn-grow">{{__('More')}}</a>
        </div>
    @endif
@endsection
