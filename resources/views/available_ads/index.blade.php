<?php
use App\Models\TraderAd;
use App\Models\TraderPaymentAccount;
?>
@extends('layouts.app')
@section('content')
    <nav aria-label="breadcrumb" class="pt-3">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">{{__('Dashboard')}}</a></li>
            <li class="breadcrumb-item active">{{__('Available Ads')}}</li>
        </ol>
    </nav>
    <h2 class="font-semibold text-xl text-gray-800 leading-tight">
        {{ __('Available Ads') }}
    </h2>


    <div class="table-responsive">
        <table class="table">
            <thead>
            <tr>
                <th scope="col">{{__('ID')}}</th>
                <th scope="col">{{__('Min Amount')}}</th>
                <th scope="col">{{__('Max Amount')}}</th>
                <th scope="col">{{__('Status')}}</th>

            </tr>
            </thead>
            <tbody>
            @foreach($ads as $item)

                <tr>
                    <td>{{$item['id']}}</td>

                    <td>{{price_format($item['min_amount'], 2)}}</td>
                    <td>{{price_format($item['max_amount'], 2)}}</td>
                    <td>{{__(['InActive', 'Active'][$item['status']])}}</td>

                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
