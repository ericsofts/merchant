@extends('layouts.app')
@section('content')
    <nav aria-label="breadcrumb" class="pt-3">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">{{__('Dashboard')}}</a></li>
            <li class="breadcrumb-item active">{{__('Whitelable')}}</li>
        </ol>
    </nav>
    <div class="page-header">
        <h1>
            {{ __('Whitelable') }}
        </h1>
    </div>

    <div class="content-wrapper">
        <div class="page-preview mb-3 p-3">
            <a href="{{route('merchant.whitelabel.preview')}}">
                <span>
                    {{ __('Whitelable Preview') }}
                </span>
            </a>
        </div>
        <form action="{{ route('merchant.whitelabel') }}" method="POST" enctype="multipart/form-data">
            @method('PATCH')
            @csrf
            <div class="form-check p-3">
                <div class="form-check form-switch">
                    <input class="form-check-input enable @error('disable-name') is-invalid @enderror"
                           type="checkbox"
                           id="disable-name"
                           name="disable-name"
                           value="1"
                           @if($disableName)
                           checked
                        @endif
                    >
                    @error('disable-name')
                    <div class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </div>
                    @enderror
                    <label class="form-check-label"
                           for="enable"><strong>{{__('Disable show merchant name on API page')}}</strong></label>
                </div>
            </div>
            <div class="form-check p-3">
                <div class="form-check form-switch">
                    <input class="form-check-input enable @error('enable') is-invalid @enderror"
                           type="checkbox"
                           id="enable"
                           name="enable"
                           value="1"
                           @if($whitelabel->enable)
                           checked
                        @endif
                    >
                    @error('enable')
                    <div class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </div>
                    @enderror
                    <label class="form-check-label"
                           for="enable"><strong>{{__('Enable')}}</strong></label>
                </div>
            </div>

            <div class="p-3">
                <button type="submit" name="action" value="update" class="btn btn-grow">{{__('Update')}}</button>
                <button type="submit" name="action" value="reset" class="btn btn-grow">{{__('Reset')}}</button>
            </div>

            <label for="page" class="p-3 input-color-bloc">{{__('Page')}}</label>

            <div class="input-group mb-3 p-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addont1">{{__('Theme')}}</span>
                </div>
                <select type="text" class="form-control @error('page-view_theme') is-invalid @enderror"
                        value="{{$whitelabel->getPageValue('view_theme')}}"
                        name="page-view_theme"
                        aria-describedby="basic-addont1">
                    @foreach ($themes as $unit)
                        <option value="{{$unit->id}}" @if($whitelabel->getPageValue('view_theme') == $unit->id) selected @endif>{{$unit->name}}</option>
                    @endforeach
                </select>
                @error('page-view_theme')
                <div class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </div>
                @enderror
            </div>

            <div class="input-group mb-3 p-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon3">{{__('Logo Image')}}</span>
                </div>
                <input type="file" class="form-control @error('page-image_logo') is-invalid @enderror"
                       name="page-image_logo" aria-describedby="basic-addon3"
                       value="{{$whitelabel->getImageLogoValue('image_logo')}}">
                @error('page-image_logo')
                <div class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </div>
                @enderror
                @if ($whitelabel->getImageLogoValue('image_logo'))
                    <div id="page-image_logo">
                        <img src="{{ $whitelabel->getImageLogoValue('image_logo') }}"/>
                    </div>
                @endif
            </div>

            <div class="input-group mb-3 p-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon3">{{__('Header Label')}}</span>
                </div>
                <input type="text" class="form-control @error('page-header_label') is-invalid @enderror"
                       name="page-header_label" id="page"
                       aria-describedby="basic-addon3" value="{{$whitelabel->getPageValue('header_label')}}">
                @error('page-header_label')
                <div class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </div>
                @enderror
            </div>

            <div class="input-group mb-3 p-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon3">{{__('Header Text')}}</span>
                </div>
                <input type="text" class="form-control @error('page-header_text') is-invalid @enderror"
                       name="page-header_text"
                       aria-describedby="basic-addon3" value="{{$whitelabel->getPageValue('header_text')}}">
                @error('page-header_text')
                <div class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </div>
                @enderror
            </div>

            <div class="input-group mb-3 p-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon3">{{__('Footer Text')}}</span>
                </div>
                <input type="text" class="form-control @error('page-footer_text') is-invalid @enderror"
                       name="page-footer_text"
                       aria-describedby="basic-addon3" value="{{$whitelabel->getPageValue('footer_text')}}">
                @error('page-footer_text')
                <div class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </div>
                @enderror
            </div>


            <div class="input-group mb-3 p-3 input-color-bloc">
                <div class="input-group-prepend">
                    <span class="input-group-text">
                          <span class='color-box'
                                style="background: {{$whitelabel->getPageValue('background_color')}}"></span><span
                            class="color-contrast">{{__('Background Color')}}</span></span>
                </div>
                <input type="text" class="form-control @error('page-background_color') is-invalid @enderror"
                       name="page-background_color"
                       aria-describedby="basic-addon3" value="{{$whitelabel->getPageValue('background_color')}}">
                @error('page-background_color')
                <div class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </div>
                @enderror
            </div>

            <div class="input-group mb-3 p-3 input-color-bloc">
                <div class="input-group-prepend">
                    <span class="input-group-text">
                          <span class='color-box'
                                style="background: {{$whitelabel->getPageValue('primary_font_color')}}"></span><span
                            class="color-contrast">{{__('Primary Font Color')}}</span></span>
                </div>
                <input type="text" class="form-control @error('page-primary_font_color') is-invalid @enderror"
                       name="page-primary_font_color"
                       aria-describedby="basic-addon3" value="{{$whitelabel->getPageValue('primary_font_color')}}">
                @error('page-primary_font_color')
                <div class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </div>
                @enderror
            </div>
            <div class="input-group mb-3 p-3 input-color-bloc">
                <div class="input-group-prepend">
                    <span class="input-group-text">
                          <span class='color-box'
                                style="background: {{$whitelabel->getPageValue('secondary_font_color')}}"></span><span
                            class="color-contrast">{{__('Secondary Font Color')}}</span></span>
                </div>
                <input type="text" class="form-control @error('page-secondary_font_color') is-invalid @enderror"
                       name="page-secondary_font_color"
                       aria-describedby="basic-addon3" value="{{$whitelabel->getPageValue('secondary_font_color')}}">
                @error('page-secondary_font_color')
                <div class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </div>
                @enderror
            </div>
            <div class="input-group mb-3 p-3 input-color-bloc input-color-bloc">
                <div class="input-group-prepend">
                    <span class="input-group-text">
                          <span class='color-box'
                                style="background: {{$whitelabel->getPageValue('danger_font_color')}}"></span><span
                            class="color-contrast">{{__('Danger Font Color')}}</span></span>
                </div>
                <input type="text" class="form-control @error('page-danger_font_color') is-invalid @enderror"
                       name="page-danger_font_color"
                       aria-describedby="basic-addon3" value="{{$whitelabel->getPageValue('danger_font_color')}}">
                @error('page-danger_font_color')
                <div class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </div>
                @enderror
            </div>
            <div class="input-group mb-3 p-3 input-color-bloc">
                <div class="input-group-prepend">
                    <span class="input-group-text">
                          <span class='color-box'
                                style="background: {{$whitelabel->getPageValue('btn_primary_font_color')}}"></span><span
                            class="color-contrast">{{__('Button Primary Font Color')}}</span></span>
                </div>
                <input type="text" class="form-control @error('page-btn_primary_font_color') is-invalid @enderror"
                       name="page-btn_primary_font_color"
                       aria-describedby="basic-addon3" value="{{$whitelabel->getPageValue('btn_primary_font_color')}}">
                @error('page-btn_primary_font_color')
                <div class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </div>
                @enderror
            </div>
            <div class="input-group mb-3 p-3 input-color-bloc">
                <div class="input-group-prepend">
                    <span class="input-group-text">
                          <span class='color-box'
                                style="background: {{$whitelabel->getPageValue('btn_primary_background_color')}}"></span><span
                            class="color-contrast">{{__('Button Primary Background Color')}}</span></span>
                </div>
                <input type="text" class="form-control @error('page-btn_primary_background_color') is-invalid @enderror"
                       name="page-btn_primary_background_color"
                       aria-describedby="basic-addon3"
                       value="{{$whitelabel->getPageValue('btn_primary_background_color')}}">
                @error('page-btn_primary_background_color')
                <div class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </div>
                @enderror
            </div>
            <div class="input-group mb-3 p-3 input-color-bloc">
                <div class="input-group-prepend">
                    <span class="input-group-text">
                          <span class='color-box'
                                style="background: {{$whitelabel->getPageValue('btn_primary_border_color')}}"></span><span
                            class="color-contrast">{{__('Button Primary Border Color')}}</span></span>
                </div>
                <input type="text" class="form-control @error('page-btn_primary_border_color') is-invalid @enderror"
                       name="page-btn_primary_border_color"
                       aria-describedby="basic-addon3"
                       value="{{$whitelabel->getPageValue('btn_primary_border_color')}}">
                @error('page-btn_primary_border_color')
                <div class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </div>
                @enderror
            </div>
            <div class="input-group mb-3 p-3 input-color-bloc">
                <div class="input-group-prepend">
                    <span class="input-group-text">
                          <span class='color-box'
                                style="background: {{$whitelabel->getPageValue('btn_primary_hover_font_color')}}"></span><span
                            class="color-contrast">{{__('Button Primary Hover Font Color')}}</span></span>
                </div>
                <input type="text" class="form-control @error('page-btn_primary_hover_font_color') is-invalid @enderror"
                       name="page-btn_primary_hover_font_color"
                       aria-describedby="basic-addon3"
                       value="{{$whitelabel->getPageValue('btn_primary_hover_font_color')}}">
                @error('page-btn_primary_hover_font_color')
                <div class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </div>
                @enderror
            </div>
            <div class="input-group mb-3 p-3 input-color-bloc">
                <div class="input-group-prepend">
                    <span class="input-group-text">
                          <span class='color-box'
                                style="background: {{$whitelabel->getPageValue('btn_primary_hover_background_color')}}"></span><span
                            class="color-contrast">{{__('Button Primary Hover Background Color')}}</span></span>
                </div>
                <input type="text"
                       class="form-control @error('page-btn_primary_hover_background_color') is-invalid @enderror"
                       name="page-btn_primary_hover_background_color"
                       aria-describedby="basic-addon3"
                       value="{{$whitelabel->getPageValue('btn_primary_hover_background_color')}}">
                @error('page-btn_primary_hover_background_color')
                <div class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </div>
                @enderror
            </div>
            <div class="input-group mb-3 p-3 input-color-bloc">
                <div class="input-group-prepend">
                    <span class="input-group-text">
                          <span class='color-box'
                                style="background: {{$whitelabel->getPageValue('btn_primary_hover_border_color')}}"></span><span
                            class="color-contrast">{{__('Button Primary Hover Border Color')}}</span></span>
                </div>
                <input type="text"
                       class="form-control @error('page-btn_primary_hover_border_color') is-invalid @enderror"
                       name="page-btn_primary_hover_border_color"
                       aria-describedby="basic-addon3"
                       value="{{$whitelabel->getPageValue('btn_primary_hover_border_color')}}">
                @error('page-btn_primary_hover_border_color')
                <div class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </div>
                @enderror
            </div>
            <div class="input-group mb-3 p-3 input-color-bloc">
                <div class="input-group-prepend">
                    <span class="input-group-text">
                          <span class='color-box'
                                style="background: {{$whitelabel->getPageValue('btn_secondary_font_color')}}"></span><span
                            class="color-contrast">{{__('Button Secondary Font Color')}}</span></span>
                </div>
                <input type="text" class="form-control @error('page-btn_secondary_font_color') is-invalid @enderror"
                       name="page-btn_secondary_font_color"
                       aria-describedby="basic-addon3"
                       value="{{$whitelabel->getPageValue('btn_secondary_font_color')}}">
                @error('page-btn_secondary_font_color')
                <div class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </div>
                @enderror
            </div>
            <div class="input-group mb-3 p-3 input-color-bloc">
                <div class="input-group-prepend">
                    <span class="input-group-text">
                          <span class='color-box'
                                style="background: {{$whitelabel->getPageValue('btn_secondary_background_color')}}"></span><span
                            class="color-contrast">{{__('Button Secondary Background Color')}}</span></span>
                </div>
                <input type="text"
                       class="form-control @error('page-btn_secondary_background_color') is-invalid @enderror"
                       name="page-btn_secondary_background_color"
                       aria-describedby="basic-addon3"
                       value="{{$whitelabel->getPageValue('btn_secondary_background_color')}}">
                @error('page-btn_secondary_background_color')
                <div class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </div>
                @enderror
            </div>
            <div class="input-group mb-3 p-3 input-color-bloc">
                <div class="input-group-prepend">
                    <span class="input-group-text">
                          <span class='color-box'
                                style="background: {{$whitelabel->getPageValue('btn_secondary_border_color')}}"></span><span
                            class="color-contrast">{{__('Button Secondary Border Color')}}</span></span>
                </div>
                <input type="text" class="form-control @error('page-btn_secondary_border_color') is-invalid @enderror"
                       name="page-btn_secondary_border_color"
                       aria-describedby="basic-addon3"
                       value="{{$whitelabel->getPageValue('btn_secondary_border_color')}}">
                @error('page-btn_secondary_border_color')
                <div class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </div>
                @enderror
            </div>
            <div class="input-group mb-3 p-3 input-color-bloc">
                <div class="input-group-prepend">
                    <span class="input-group-text">
                          <span class='color-box'
                                style="background: {{$whitelabel->getPageValue('btn_secondary_hover_font_color')}}"></span><span
                            class="color-contrast">{{__('Button Secondary Hover Font Color')}}</span></span>
                </div>
                <input type="text"
                       class="form-control @error('page-btn_secondary_hover_font_color') is-invalid @enderror"
                       name="page-btn_secondary_hover_font_color"
                       aria-describedby="basic-addon3"
                       value="{{$whitelabel->getPageValue('btn_secondary_hover_font_color')}}">
                @error('page-btn_secondary_hover_font_color')
                <div class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </div>
                @enderror
            </div>
            <div class="input-group mb-3 p-3 input-color-bloc">
                <div class="input-group-prepend">
                    <span class="input-group-text">
                          <span class='color-box'
                                style="background: {{$whitelabel->getPageValue('btn_secondary_hover_background_color')}}"></span><span
                            class="color-contrast">{{__('Button Secondary Hover Background Color')}}</span></span>
                </div>
                <input type="text"
                       class="form-control @error('page-btn_secondary_hover_background_color') is-invalid @enderror"
                       name="page-btn_secondary_hover_background_color"
                       aria-describedby="basic-addon3"
                       value="{{$whitelabel->getPageValue('btn_secondary_hover_background_color')}}">
                @error('page-btn_secondary_hover_background_color')
                <div class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </div>
                @enderror
            </div>
            <div class="input-group mb-3 p-3 input-color-bloc">
                <div class="input-group-prepend">
                    <span class="input-group-text">
                          <span class='color-box'
                                style="background: {{$whitelabel->getPageValue('btn_secondary_hover_border_color')}}"></span><span
                            class="color-contrast">{{__('Button Secondary Hover Border Color')}}</span></span>
                </div>
                <input type="text"
                       class="form-control @error('page-btn_secondary_hover_border_color') is-invalid @enderror"
                       name="page-btn_secondary_hover_border_color"
                       aria-describedby="basic-addon3"
                       value="{{$whitelabel->getPageValue('btn_secondary_hover_border_color')}}">
                @error('page-btn_secondary_hover_border_color')
                <div class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </div>
                @enderror
            </div>
            <div class="input-group mb-3 p-3 input-color-bloc">
                <div class="input-group-prepend">
                    <span class="input-group-text">
                          <span class='color-box'
                                style="background: {{$whitelabel->getPageValue('alert_background')}}"></span><span
                            class="color-contrast">{{__('Alert Background')}}</span></span>
                </div>
                <input type="text" class="form-control @error('page-alert_background') is-invalid @enderror"
                       name="page-alert_background"
                       aria-describedby="basic-addon3" value="{{$whitelabel->getPageValue('alert_background')}}">
                @error('page-alert_background')
                <div class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </div>
                @enderror
            </div>
            <div class="input-group mb-3 p-3 input-color-bloc">
                <div class="input-group-prepend">
                    <span class="input-group-text">
                          <span class='color-box'
                                style="background: {{$whitelabel->getPageValue('alert_color')}}"></span><span
                            class="color-contrast">{{__('Alert Color')}}</span></span>
                </div>
                <input type="text" class="form-control @error('page-alert_color') is-invalid @enderror"
                       name="page-alert_color"
                       aria-describedby="basic-addon3" value="{{$whitelabel->getPageValue('alert_color')}}">
                @error('page-alert_color')
                <div class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </div>
                @enderror
            </div>
            <div class="input-group mb-3 p-3 input-color-bloc">
                <div class="input-group-prepend">
                    <span class="input-group-text">
                          <span class='color-box'
                                style="background: {{$whitelabel->getPageValue('alert_border')}}"></span><span
                            class="color-contrast">{{__('Alert Border')}}</span></span>
                </div>
                <input type="text" class="form-control @error('page-alert_border') is-invalid @enderror"
                       name="page-alert_border"
                       aria-describedby="basic-addon3" value="{{$whitelabel->getPageValue('alert_border')}}">
                @error('page-alert_border')
                <div class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </div>
                @enderror
            </div>

            <label for="modal" class="p-3 input-color-bloc">{{__('Modal')}}</label>

            <div class="input-group mb-3 p-3 input-color-bloc">
                <div class="input-group-prepend">
                    <span class="input-group-text">
                          <span class='color-box'
                                style="background: {{$whitelabel->getModalValue('background_color')}}"></span><span
                            class="color-contrast">{{__('Background Color')}}</span></span>
                </div>
                <input type="text" class="form-control @error('modal-background_color') is-invalid @enderror"
                       name="modal-background_color" id="modal"
                       aria-describedby="basic-addon3" value="{{$whitelabel->getModalValue('background_color')}}">
                @error('modal-background_color')
                <div class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </div>
                @enderror
            </div>
            <div class="input-group mb-3 p-3 input-color-bloc">
                <div class="input-group-prepend">
                    <span class="input-group-text">
                          <span class='color-box'
                                style="background: {{$whitelabel->getModalValue('warning_color')}}"></span><span
                            class="color-contrast">{{__('Warning Color')}}</span></span>
                </div>
                <input type="text" class="form-control @error('modal-warning_color') is-invalid @enderror"
                       name="modal-warning_color"
                       aria-describedby="basic-addon3" value="{{$whitelabel->getModalValue('warning_color')}}">
                @error('modal-warning_color')
                <div class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </div>
                @enderror
            </div>
            <div class="input-group mb-3 p-3 input-color-bloc">
                <div class="input-group-prepend">
                    <span class="input-group-text">
                          <span class='color-box'
                                style="background: {{$whitelabel->getModalValue('danger_font_color')}}"></span><span
                            class="color-contrast">{{__('Danger Font Color')}}</span></span>
                </div>
                <input type="text" class="form-control @error('modal-danger_font_color') is-invalid @enderror"
                       name="modal-danger_font_color"
                       aria-describedby="basic-addon3" value="{{$whitelabel->getModalValue('danger_font_color')}}">
                @error('modal-danger_font_color')
                <div class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </div>
                @enderror
            </div>
            <div class="input-group mb-3 p-3 input-color-bloc">
                <div class="input-group-prepend">
                    <span class="input-group-text">
                          <span class='color-box'
                                style="background: {{$whitelabel->getModalValue('btn_primary_font_color')}}"></span><span
                            class="color-contrast">{{__('Button Primary Font Color')}}</span></span>
                </div>
                <input type="text" class="form-control @error('modal-btn_primary_font_color') is-invalid @enderror"
                       name="modal-btn_primary_font_color"
                       aria-describedby="basic-addon3" value="{{$whitelabel->getModalValue('btn_primary_font_color')}}">
                @error('modal-btn_primary_font_color')
                <div class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </div>
                @enderror
            </div>
            <div class="input-group mb-3 p-3 input-color-bloc">
                <div class="input-group-prepend">
                    <span class="input-group-text">
                          <span class='color-box'
                                style="background: {{$whitelabel->getModalValue('btn_primary_background_color')}}"></span><span
                            class="color-contrast">{{__('Button Primary Background Color')}}</span></span>
                </div>
                <input type="text"
                       class="form-control @error('modal-btn_primary_background_color') is-invalid @enderror"
                       name="modal-btn_primary_background_color"
                       aria-describedby="basic-addon3"
                       value="{{$whitelabel->getModalValue('btn_primary_background_color')}}">
                @error('btn_primary_background_color')
                <div class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </div>
                @enderror
            </div>
            <div class="input-group mb-3 p-3 input-color-bloc">
                <div class="input-group-prepend">
                    <span class="input-group-text">
                          <span class='color-box'
                                style="background: {{$whitelabel->getModalValue('btn_primary_border_color')}}"></span><span
                            class="color-contrast">{{__('Button Primary Border Color')}}</span></span>
                </div>
                <input type="text" class="form-control @error('modal-btn_primary_border_color') is-invalid @enderror"
                       name="modal-btn_primary_border_color"
                       aria-describedby="basic-addon3"
                       value="{{$whitelabel->getModalValue('btn_primary_border_color')}}">
                @error('modal-btn_primary_border_color')
                <div class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </div>
                @enderror
            </div>
            <div class="input-group mb-3 p-3 input-color-bloc">
                <div class="input-group-prepend">
                    <span class="input-group-text">
                          <span class='color-box'
                                style="background: {{$whitelabel->getModalValue('btn_primary_hover_font_color')}}"></span><span
                            class="color-contrast">{{__('Button Primary Hover Font Color')}}</span></span>
                </div>
                <input type="text"
                       class="form-control @error('modal-btn_primary_hover_font_color') is-invalid @enderror"
                       name="modal-btn_primary_hover_font_color"
                       aria-describedby="basic-addon3"
                       value="{{$whitelabel->getModalValue('btn_primary_hover_font_color')}}">
                @error('modal-btn_primary_hover_font_color')
                <div class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </div>
                @enderror
            </div>
            <div class="input-group mb-3 p-3 input-color-bloc">
                <div class="input-group-prepend">
                    <span class="input-group-text">
                          <span class='color-box'
                                style="background: {{$whitelabel->getModalValue('btn_primary_hover_background_color')}}"></span><span
                            class="color-contrast">{{__('Button Primary Hover Background Color')}}</span></span>
                </div>
                <input type="text"
                       class="form-control @error('modal-btn_primary_hover_background_color') is-invalid @enderror"
                       name="modal-btn_primary_hover_background_color"
                       aria-describedby="basic-addon3"
                       value="{{$whitelabel->getModalValue('btn_primary_hover_background_color')}}">
                @error('modal-btn_primary_hover_background_color')
                <div class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </div>
                @enderror
            </div>
            <div class="input-group mb-3 p-3 input-color-bloc">
                <div class="input-group-prepend">
                    <span class="input-group-text">
                          <span class='color-box'
                                style="background: {{$whitelabel->getModalValue('btn_primary_hover_border_color')}}"></span><span
                            class="color-contrast">{{__('Button Primary Hover Border Color')}}</span></span>
                </div>
                <input type="text"
                       class="form-control @error('modal-btn_primary_hover_border_color') is-invalid @enderror"
                       name="modal-btn_primary_hover_border_color"
                       aria-describedby="basic-addon3"
                       value="{{$whitelabel->getModalValue('btn_primary_hover_border_color')}}">
                @error('modal-btn_primary_hover_border_color')
                <div class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </div>
                @enderror
            </div>
            <div class="input-group mb-3 p-3 input-color-bloc">
                <div class="input-group-prepend">
                    <span class="input-group-text">
                          <span class='color-box'
                                style="background: {{$whitelabel->getModalValue('btn_secondary_font_color')}}"></span><span
                            class="color-contrast">{{__('Button Secondary Font Color')}}</span></span>
                </div>
                <input type="text" class="form-control @error('modal-btn_secondary_font_color') is-invalid @enderror"
                       name="modal-btn_secondary_font_color"
                       aria-describedby="basic-addon3"
                       value="{{$whitelabel->getModalValue('btn_secondary_font_color')}}">
                @error('modal-btn_secondary_font_color')
                <div class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </div>
                @enderror
            </div>
            <div class="input-group mb-3 p-3 input-color-bloc">
                <div class="input-group-prepend">
                    <span class="input-group-text">
                          <span class='color-box'
                                style="background: {{$whitelabel->getModalValue('btn_secondary_background_color')}}"></span><span
                            class="color-contrast">{{__('Button Secondary Background Color')}}</span></span>
                </div>
                <input type="text"
                       class="form-control @error('modal-btn_secondary_background_color') is-invalid @enderror"
                       name="modal-btn_secondary_background_color"
                       aria-describedby="basic-addon3"
                       value="{{$whitelabel->getModalValue('btn_secondary_background_color')}}">
                @error('modal-btn_secondary_background_color')
                <div class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </div>
                @enderror
            </div>
            <div class="input-group mb-3 p-3 input-color-bloc">
                <div class="input-group-prepend">
                    <span class="input-group-text">
                          <span class='color-box'
                                style="background: {{$whitelabel->getModalValue('btn_secondary_border_color')}}"></span><span
                            class="color-contrast">{{__('Button Secondary Border Color')}}</span></span>
                </div>
                <input type="text" class="form-control @error('modal-btn_secondary_border_color') is-invalid @enderror"
                       name="modal-btn_secondary_border_color"
                       aria-describedby="basic-addon3"
                       value="{{$whitelabel->getModalValue('btn_secondary_border_color')}}">
                @error('modal-btn_secondary_border_color')
                <div class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </div>
                @enderror
            </div>
            <div class="input-group mb-3 p-3 input-color-bloc">
                <div class="input-group-prepend">
                    <span class="input-group-text">
                          <span class='color-box'
                                style="background: {{$whitelabel->getModalValue('btn_secondary_hover_font_color')}}"></span><span
                            class="color-contrast">{{__('Button Secondary Hover Font Color')}}</span></span>
                </div>
                <input type="text"
                       class="form-control @error('modal-btn_secondary_hover_font_color') is-invalid @enderror"
                       name="modal-btn_secondary_hover_font_color"
                       aria-describedby="basic-addon3"
                       value="{{$whitelabel->getModalValue('btn_secondary_hover_font_color')}}">
                @error('modal-btn_secondary_hover_font_color')
                <div class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </div>
                @enderror
            </div>
            <div class="input-group mb-3 p-3 input-color-bloc">
                <div class="input-group-prepend">
                    <span class="input-group-text">
                          <span class='color-box'
                                style="background: {{$whitelabel->getModalValue('btn_secondary_hover_background_color')}}"></span><span
                            class="color-contrast">{{__('Button Secondary Hover Background Color')}}</span></span>
                </div>
                <input type="text"
                       class="form-control @error('modal-btn_secondary_hover_background_color') is-invalid @enderror"
                       name="modal-btn_secondary_hover_background_color"
                       aria-describedby="basic-addon3"
                       value="{{$whitelabel->getModalValue('btn_secondary_hover_background_color')}}">
                @error('modal-btn_secondary_hover_background_color')
                <div class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </div>
                @enderror
            </div>
            <div class="input-group mb-3 p-3 input-color-bloc">
                <div class="input-group-prepend">
                    <span class="input-group-text">
                          <span class='color-box'
                                style="background: {{$whitelabel->getModalValue('btn_secondary_hover_border_color')}}"></span><span
                            class="color-contrast">{{__('Button Secondary Hover Border Color')}}</span></span>
                </div>
                <input type="text"
                       class="form-control @error('modal-btn_secondary_hover_border_color') is-invalid @enderror"
                       name="modal-btn_secondary_hover_border_color"
                       aria-describedby="basic-addon3"
                       value="{{$whitelabel->getModalValue('btn_secondary_hover_border_color')}}">
                @error('modal-btn_secondary_hover_border_color')
                <div class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </div>
                @enderror
            </div>
            <div class="input-group mb-3 p-3 input-color-bloc">
                <div class="input-group-prepend">
                    <span class="input-group-text">{{__('Button Secondary Underline')}}</span>
                </div>
                <select type="text" class="form-control @error('modal-btn_secondary_underline') is-invalid @enderror"
                        name="modal-btn_secondary_underline"
                        aria-describedby="basic-addon3">
                    <option value='underline'
                            @if($whitelabel->getModalValue('btn_secondary_underline') == 'underline') selected @endif>{{__('underline')}}</option>
                    <option value='none'
                            @if($whitelabel->getModalValue('btn_secondary_underline') == 'none') selected @endif>{{__('none')}}</option>
                </select>
                @error('modal-btn_secondary_underline')
                <div class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </div>
                @enderror
            </div>
        </form>
    </div>
@endsection
@section('script-bottom')
    <script>
        window.onload = function () {
            $(function () {
                $('.input-color-bloc input').colorpicker();
            });
            $('.input-color-bloc input').on('changeColor', function (event) {
                $(event.target).closest('.input-color-bloc').find('.color-box').css('background-color', event.color.toString());
            });
        }
    </script>
@endsection
