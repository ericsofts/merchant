@extends('layouts.app')
@section('content')
    <nav aria-label="breadcrumb" class="pt-3">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">{{__('Dashboard')}}</a></li>
            <li class="breadcrumb-item active">{{__('IP Security')}}</li>
        </ol>
    </nav>
    <div class="page-header">
        <h1>
            {{ __('IP Security') }}
        </h1>
    </div>

    <div class="content-wrapper">

        <form action="{{ route('merchant.ip_security.update') }}" method="POST" enctype="multipart/form-data">
            @method('PATCH')
            @csrf
            <div class="form-check p-3">
                <div class="form-check form-switch">
                    <input class="form-check-input enable @error('enable') is-invalid @enderror"
                           type="checkbox"
                           id="enable"
                           name="enable"
                           value="1"
                           @if(old('enable', $enable))
                           checked
                        @endif
                    >
                    @error('enable')
                    <div class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </div>
                    @enderror
                    <label class="form-check-label"
                           for="enable"><strong>{{__('Enable')}}</strong></label>
                </div>
            </div>
            <div class="input-group mb-3 pt-3 px-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon3">{{__('Permitted IP Addresses')}}</span>
                </div>
                <input type="text" class="form-control @error('ip-list') is-invalid @enderror"
                       name="ip-list" id="page"
                       aria-describedby="basic-addon3" value="{{old('ip-list',$ip_list)}}">
                @error('ip-list')
                <div class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </div>
                @enderror

            </div>
            <div class="row pb-3 px-3">
                <span>{{__('List Permitted IP Addresses divided by ;')}}</span>
            </div>
            <div class="p-3">
                <button type="submit" name="action" value="update" class="btn btn-grow">{{__('Update')}}</button>
            </div>

        </form>
    </div>
@endsection

