@extends('layouts.api')
@section('body-class', 'd-flex h-100 api')
@section('html-class', 'h-100')
@if($merchant ?? '')
    @section('prepare-model', $whiteLabelModel = $whiteLabelModel->getWhiteLabelById($merchant))
@endif
@section('title', $whiteLabelModel->getPageText('header_label'))

@section('css-whitelabel')
    {!! $whiteLabelModel->getCss() !!}
@endsection

@section('content')
    <main class="form-1">

            <div class="alert alert-grow alert-dismissible fade show" role="alert">
                {{ __('Status') }}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>

            <div class="alert alert-grow-w alert-dismissible fade show" role="alert">
                {{ __('Error') }}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>


            <div class="text-center">
            <span>
                {{ $whiteLabelModel->getPageText('header_text')}}
            </span>
            </div>
            <div class="text-center">
                <img class="mb-4 img-fluid" src="{{ $whiteLabelModel->getImageLogo() }}">
                @if(!$disableName)
                <h2 class="h3 mb-3 fw-normal">{{auth()->user()->name}}</h2>
                @endif
            </div>
            <div class="mb-3 mt-4 d-flex justify-content-center">
                <dl class="row">
                    <dt class="col-sm-5">{{__('Internal order #')}}:</dt>
                    <dd class="col-sm-7">{{__('100')}}</dd>

                    <dt class="col-sm-5">{{__('Order #')}}:</dt>
                    <dd class="col-sm-7">{{__('100')}}</dd>


                    <dt class="col-sm-5"></dt>
                    <dd class="col-sm-5">{{__('Test')}}</dd>

                    <dt class="col-sm-5">{{__('Amount')}}:</dt>
                        <dd class="col-sm-7">$15.00</dd>

                </dl>
            </div>
            <form method="POST" action="">

                <div class="mb-3">
                    <label for="currency" class="form-label">{{__('Select currency')}}</label>
                    <select class="form-select" id="currency" name="currency" required>

                        <option value="USD">USD</option>
                        <option value="RUB">RUB</option>
                    </select>
                </div>
                <div class="mb-3">
                    <label for="payment_system_id" class="form-label">{{__('Select payment system')}}</label>
                    <select class="form-select" id="payment_system_id" name="payment_system_id" required>

                            <option value="bank">Any Russian bank - 1078.51</option>
                            <option value="bank2">Alfa-Bank - 1089.13</option>
                    </select>
                    <div id="passwordHelpBlock" class="form-text secondary-whitelable-text">
                        {{__('Please choose from the list only the bank whose card you have and from which you plan to pay.')}}
                    </div>
                </div>
                <div class="mb-3 mt-3 d-flex justify-content-center">
                    <button type="submit" class="btn btn-secondary mx-2 btn-confirm btn-primary-whitelabel"
                            name="action"
                            value="1">
                        {{__('Confirm')}}
                    </button>
                </div>
            </form>
            <div class="text-center">
            <span>
                {{ $whiteLabelModel->getPageText('footer_text')}}
            </span>
            </div>
            <div class="text-center">
                <p class="fw-bold mb-0 danger-whitelable-text">{{__('If you made a payment and did not click the "Payment completed" button, it is not guaranteed to be credited.')}}</p>
                <p class="fw-bold danger-whitelable-text">{{__('If you made a payment and clicked the "Cancel" button, the payment will not be credited.')}}</p>
            </div>

        <p class="mt-5 mb-3 text-muted text-center">2020–{{date('Y')}}</p>
        <div class="page-back">
            <a class="btn btn-secondary mx-2 btn-primary-whitelabel" href="{{route('merchant.whitelabel')}}">
                <span>
                    {{ __('Back To Config') }}
                </span>
            </a>
        </div>
    </main>
@endsection

@section('script-bottom')
    <script>

        window.onload = function () {
            $(function () {
                var swalWithBootstrapButtons = Swal.mixin({
                    customClass: {
                        container: 'swal2-grow',
                        content: 'text-danger fw-bold',
                        confirmButton: 'btn btn-grow',
                        cancelButton: 'btn btn-link'
                    },
                    buttonsStyling: false
                })

                $(document).on("click", ".btn-confirm", function (e) {
                    e.preventDefault();
                    var html = "{{__('Press the "Payment completed" button only after the actual payment. Otherwise, the company reserves the right to block your account.')}}";
                    swalWithBootstrapButtons.fire({
                        html: html,
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonText: '{{__('Payment completed')}}',
                        cancelButtonText: '{{__('Cancel')}}'
                    }).then((result) => {
                        if (result.isConfirmed) {
                            form_clicked = true;
                        }
                    })
                    return false;
                });

            });
        }
    </script>
@endsection
