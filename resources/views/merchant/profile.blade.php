@extends('layouts.app')
@section('content')
    <nav aria-label="breadcrumb" class="pt-3">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">{{__('Dashboard')}}</a></li>
            <li class="breadcrumb-item active">{{__('Profile')}}</li>
        </ol>
    </nav>
    <div class="page-header">
        <h1>
            {{ __('Profile') }}
        </h1>
    </div>
    <div class="content-wrapper">
        <form method="post" action="{{ route('merchant.profile.update') }}">
            @method('PATCH')
            @csrf
            <div class="p-3">
                <label class="form-label">{{__('Name')}}</label>
                <input class="form-control @error('name') is-invalid @enderror" name="name" type="text" required value="{{request()->user()->name}}">
                @error('name')
                <div class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </div>
                @enderror
            </div>
            <div class="p-3">
                <div class="form-switch">
                    <input type="hidden" name="rounding_input_amount" value="0">
                    <input class="form-check-input"
                           type="checkbox"
                           id="rounding_input_amount"
                           name="rounding_input_amount"
                           value="1"
                           @if(request('rounding_input_amount', $rounding_input_amount)) checked @endif
                    >
                    <label class="form-label"
                           for="rounding_input_amount"><strong>{{__('Rounding the input amount to an integer')}}</strong></label>
                </div>
            </div>
            <div class="p-3">
                <button type="submit" class="btn btn-grow">{{__('Update')}}</button>
            </div>
            <div class="p-3">
                <label class="form-label">{{__('Email')}}</label>
                <input class="form-control" type="text" disabled value="{{request()->user()->email}}">
            </div>
        </form>
        <div class="p-3">
            <label class="form-label">{{__('Merchant ID')}}</label>
            <input class="form-control" type="text" disabled value="{{request()->user()->id}}">
        </div>
        @if(!empty(request()->user()->api3_key))
            <div class="p-3">
                <label class="form-label">{{__('Api3 key')}}</label>
                <input class="form-control" type="text" disabled value="{{request()->user()->api3_key}}">
            </div>
        @endif
        @if(!empty(request()->user()->api5_key))
            <div class="p-3">
                <label class="form-label">{{__('Api5 key')}}</label>
                <input class="form-control" type="text" disabled value="{{request()->user()->api5_key}}">
            </div>
        @endif
    </div>
@endsection
