@extends('layouts.app')
@section('content')
    <nav aria-label="breadcrumb" class="pt-3">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">{{__('Dashboard')}}</a></li>
            <li class="breadcrumb-item active">{{__('Account')}}</li>
        </ol>
    </nav>
    <div class="page-header">
        <h1>
            {{ __('Account') }}
        </h1>
    </div>
    <div class="content-wrapper">
        <form action="{{ route('merchant.settings.account') }}" class="p-3" method="POST">
            @method('POST')
            @csrf
            <p class="fw-bold">{{__('You can switch the account currency when the balance is zero and there are no active trades')}}</p>
            <p>{{__('Balance')}}: <b>{{$balance_total}}</b></p>
            <p>{{__('Active trades')}}: <b>{{$activeCountInvoices}}</b></p>
            <div class="mb-3">
                <label for="currency" class="form-label">{{__('Currency')}}</label>
                <select class="form-select @error('currency') is-invalid @enderror"
                        name="currency"
                        required
                        @if($balance_total > 0 || !$enable_2fa || $activeCountInvoices) disabled="disabled" @endif
                >
                    <option value=""></option>
                    @foreach($available_coins as $coin)
                            <option value="{{$coin}}"
                                    @if($coin == $merchant_coin) selected @endif
                            >{{$coin}}</option>
                    @endforeach
                </select>
                @error('currency')
                <div class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </div>
                @enderror
            </div>
            <p class="mb-3 fw-normal">{{ __('Please enter the code from the mobile Two-factor Authentication application (2FA)') }}</p>
            <div class="mb-3">
                <input type="text" class="form-control @error('code') is-invalid @enderror"
                       id="code" name="code"
                       placeholder="{{__('Code')}}"
                       value="{{old('code')}}"
                       required
                       @if($balance_total > 0 || !$enable_2fa || $activeCountInvoices) disabled="disabled" @endif
                >
                @error('code')
                <div class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </div>
                @enderror
                @if(!$enable_2fa)
                    <p class="fw-bold mb-0">{{ __('Please activate Two-factor Authentication for change currency of the account') }}</p>
                    <a href="{{route('2fa.index')}}">
                        {{__('Security options')}}
                    </a>
                @endif
            </div>
            <div>
                <button
                    type="submit"
                    name="action"
                    class="btn btn-grow btn-confirm"
                    @if($balance_total > 0 || $activeCountInvoices || !$enable_2fa) disabled="disabled" @endif
                >{{__('Update')}}</button>
            </div>

        </form>
    </div>
@endsection

@section('script-bottom')
    <script>
            $(function () {
                var swalWithBootstrapButtons = Swal.mixin({
                    customClass: {
                        container: 'swal2-grow',
                        content: 'text-danger fw-bold',
                        confirmButton: 'btn btn-grow',
                        cancelButton: 'btn btn-link'
                    },
                    buttonsStyling: false
                })
                $(document).on("click", ".btn-confirm", function (e) {
                    e.preventDefault();
                    var form = $(this).closest('form');
                    var html = "{{__('Are you sure you want to change the currency of the account?')}}";
                    swalWithBootstrapButtons.fire({
                        html: html,
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonText: '{{__('Submit')}}',
                        cancelButtonText: '{{__('Cancel')}}'
                    }).then((result) => {
                        if (result.isConfirmed) {
                            form.submit();
                        }
                    })
                    return false;
                });

            });
    </script>
@endsection


