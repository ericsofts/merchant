@extends('layouts.app')
@section('content')
    <nav aria-label="breadcrumb" class="pt-3">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">{{__('Dashboard')}}</a></li>
            <li class="breadcrumb-item active">{{__('Withdrawal history')}}</li>
        </ol>
    </nav>
    <div class="page-header">
        <h1>
            {{ __('Withdrawal history') }}
        </h1>
    </div>
    @if($invoices)
        <div class="row">
            <div class="col">
                <dl class="row">
                    <dt class="col-sm-4">{{__('Total amount')}}:</dt>
                    <dd class="col-sm-8">{{$fmt->formatCurrency(price_format($total), "USD")}}</dd>
                    <dt class="col-sm-4">{{__('Total page amount')}}:</dt>
                    <dd class="col-sm-8">{{$fmt->formatCurrency(price_format($total_page), "USD")}}</dd>
                </dl>
            </div>
        </div>
        <form method="GET" action="{{ route('withdrawal.index') }}" class="row g-3 mb-3">
            <div class="col-md-3">
                <div class="row">
                    <label class="col-sm-2 col-form-label">{{__('From')}}:</label>
                    <div class="col-sm-10">
                        <div class="input-group">
                            <span class="input-group-text"><i class="bi bi-calendar"></i></span>
                            <input type="text" class="form-control datepicker" name="from_date" placeholder="{{__('From')}}" value="{{$from_date}}" autocomplete="off">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="row">
                    <label class="col-sm-2 col-form-label">{{__('To')}}:</label>
                    <div class="col-sm-10">
                        <div class="input-group">
                            <span class="input-group-text"><i class="bi bi-calendar"></i></span>
                            <input type="text" class="form-control datepicker" name="to_date" placeholder="{{__('To')}}" value="{{$to_date}}" autocomplete="off">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="row">
                    <label class="col-sm-3 col-form-label">{{__('Status')}}: </label>
                    <div class="col-sm-9">
                        <select class="form-select" name="status">
                            <option value="">{{__('Choose')}}</option>
                            <option value="1" @if($status === "1") selected @endif>{{__('Paid')}}</option>
                            <option value="0" @if($status === "0") selected @endif>{{__('Created')}}</option>
                            <option value="2" @if($status === "2") selected @endif>{{__('Confirmed')}}</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <button type="submit" class="btn btn-grow">{{__('Submit')}}</button>
                <button type="submit" name="btnExport" class="btn btn-grow">{{__('Export')}}</button>
            </div>
        </form>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">{{__('ID')}}</th>
                <th scope="col">{{__('Order ID')}}</th>
                <th scope="col">{{__('Date')}}</th>
                <th scope="col">{{__('Amount')}}</th>
                <th scope="col">{{__('Fiat amount')}}</th>
                <th scope="col">{{__('Paid amount')}}</th>
                <th scope="col">{{__('Card Number')}}</th>
                <th scope="col">{{__('Comment')}}</th>
                <th scope="col">{{__('Status')}}</th>
                <th scope="col">{{__('Action')}}</th>
            </tr>
            </thead>
            <tbody>
            @foreach($invoices as $i => $item)
                <tr>
                    <th scope="row">{{$i+1}}</th>
                    <td>{{$item->id}}</td>
                    <td>{{$item->merchant_order_id}}</td>
                    <td>{{datetimeFormat($item->created_at)}}</td>
                    <td>{{$fmt->formatCurrency(price_format($item->amount), "USD")}}</td>
                    <td>
                        @if($item->input_currency != 'USD')
                            {{round($item->input_amount_value, 2)}}
                            {{strtoupper($item->input_currency)}}
                        @endif
                    </td>
                    <td>
                        {{round($item->fiat_amount, 2)}}
                        {{strtoupper($item->fiat_currency)}}
                    </td>
                    <td>{{mask_credit_card($item->account_info)}}</td>
                    <td>{!! nl2br($item->comment) !!}</td>
                    <td>{{payment_invoice_status($item->status)}}</td>
                    <td>
                        <a href="{{route('withdrawal.view', $item->id)}}">
                            <i class="bi bi-info-circle"></i>
                        </a>
                        @if($item->api_type == 0 && in_array($item->status, [0,2]))
                            <a href="{{route('withdrawal.create.chatex_lbc_contact_confirm_show', $item->id)}}">
                                <i class="bi bi-arrow-up-circle"></i>
                            </a>
                        @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div class="float-end">{{ $invoices->links() }}</div>
    @endif
@endsection
