@extends('layouts.app')
@section('content')
    <nav aria-label="breadcrumb" class="pt-3">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">{{__('Dashboard')}}</a></li>
            <li class="breadcrumb-item"><a href="{{route('withdrawal.index')}}">{{__('Withdrawal history')}}</a></li>
            <li class="breadcrumb-item active">{{__('Withdrawal')}} #{{$invoice->id}}</li>
        </ol>
    </nav>
    <div class="page-header">
        <h1>
            {{ __('Withdrawal') }} #{{$invoice->id}}
        </h1>
    </div>
    <div class="p-3 mb-3">
        <div class="row invoice-info">
            <div class="col-md-6 col-lg-4">
                <dl class="row">
                    <dt class="col-sm-4">{{__('ID')}}</dt>
                    <dd class="col-sm-8">{{$invoice->id}}</dd>
                    @if($invoice->merchant_order_id)
                        <dt class="col-sm-4">{{__('Order ID')}}</dt>
                        <dd class="col-sm-8">{{$invoice->merchant_order_id}}</dd>
                    @endif
                    <dt class="col-sm-4">{{__('Amount')}}</dt>
                    <dd class="col-sm-8">{{price_format($invoice->amount)}}</dd>
                    <dt class="col-sm-4">{{__('Status')}}</dt>
                    <dd class="col-sm-8">{{payment_invoice_status($invoice->status)}}</dd>
                    <dt class="col-sm-4">{{__('Created')}}</dt>
                    <dd class="col-sm-8">{{datetimeFormat($invoice->created_at)}}</dd>

                </dl>
            </div>
        </div>
        @if($attachments->count())
            <hr>
            <div class="row list-attachments">
                @foreach($attachments as $attachment)
                    <div class="col-12 col-sm-6 col-lg-3">
                        <a href="{{route('withdrawal.attachment', $attachment->id)}}" target="_blank">
                            <img src="{{route('withdrawal.attachment', $attachment->id)}}"
                                 class="float-start img-thumbnail w-100">
                        </a>
                    </div>
                @endforeach
            </div>
        @endif
    </div>
@endsection
