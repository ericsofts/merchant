@extends('layouts.app')
@section('content')
    <nav aria-label="breadcrumb" class="pt-3">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">{{__('Dashboard')}}</a></li>
            <li class="breadcrumb-item active">{{__('Withdraw')}}</li>
        </ol>
    </nav>
    <h2 class="font-semibold text-xl text-gray-800 leading-tight">
        {{ __('Withdraw') }}
    </h2>
    <form method="POST" action="{{ route('withdrawal.create.payment_method') }}">
        @csrf
        @if ($errors->any())
            <div class="alert alert-grow alert-dismissible fade show" role="alert">
                {{ __('Whoops! Something went wrong.') }}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        @endif
        @if($status = session('status'))
            <div class="alert alert-grow alert-dismissible fade show" role="alert">
                {{ $status }}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        @endif
        <p>{{__('Balance')}}: <span class="fw-bold">{{$fmt->formatCurrency(price_format($balance), "USD")}}</p>
        <div class="mb-3">
            <div class="form-switch">
                <input class="form-check-input merchant_expense"
                       type="checkbox"
                       id="merchant_expense"
                       name="merchant_expense"
                       value="1"
                >
                <label class="form-label"
                       for="merchant_expense"><strong>{{__('Commission at your expense')}}</strong></label>
            </div>
        </div>
        <div class="mb-3">
            <label for="amount" class="form-label">{{__('Amount')}}</label>
            <div class="input-group">
                <span class="input-group-text">$</span>
                <input type="text" class="form-control @error('amount') is-invalid @enderror" id="amount" name="amount" placeholder="{{__('Amount')}}"
                       value="{{old('amount')}}" required autofocus
                >
                @error('amount')
                <div class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </div>
                @enderror
            </div>
        </div>
        <div class="mb-3">
            <label for="withdrawal_method" class="form-label">{{__('Withdrawal method')}}</label>
            <select class="form-select @error('withdrawal_method') is-invalid @enderror" id="payment_method" name="payment_method" required>
                <option value="bank_card">{{__('Bank Card')}}</option>
            </select>
            @error('withdrawal_method')
            <div class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </div>
            @enderror
        </div>
        <div class="mb-3">
            <label for="amount" class="form-label">{{__('Comment')}}</label>
            <textarea class="form-control @error('comment') is-invalid @enderror" id="comment" name="comment" placeholder="{{__('Comment')}}">{{old('comment')}}</textarea>
            @error('comment')
            <div class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </div>
            @enderror

        </div>
        <div class="mb-3 float-end">
            <button type="submit" class="btn btn-grow" onclick="this.disabled=true;this.form.submit();">
                {{__('Next')}}
                <i class="bi bi-arrow-right"></i>
            </button>
        </div>
    </form>
@endsection

