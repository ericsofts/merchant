@extends('layouts.app')
@section('content')
    <nav aria-label="breadcrumb" class="pt-3">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">{{__('Dashboard')}}</a></li>
            <li class="breadcrumb-item"><a href="{{route('withdrawal.create')}}">{{__('Withdraw')}}</a></li>
            <li class="breadcrumb-item active">{{__('Bank Card')}}</li>
        </ol>
    </nav>
    <h2 class="font-semibold text-xl text-gray-800 leading-tight">
        {{ __('Withdraw') }}. {{ __('Bank Card') }}
    </h2>
    <form method="POST" action="{{ route('withdrawal.create.chatex_lbc_contact_create') }}">
        @csrf
        <input type="hidden" name="amount" value="{{ $amount }}">
        <input type="hidden" name="merchant_expense" value="{{ request('merchant_expense') }}">
        <input type="hidden" name="comment" value="{{ request('comment') }}">
        @if ($errors->any())
            <div class="alert alert-grow alert-dismissible fade show" role="alert">
                {{ __('Whoops! Something went wrong.') }}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        @endif
        @if($status = session('status'))
            <div class="alert alert-grow alert-dismissible fade show" role="alert">
                {{ $status }}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        @endif
        <div class="mb-3">
            <div class="form-switch">
                <input class="form-check-input merchant_expense"
                       type="checkbox"
                       value="1"
                       @if(request('merchant_expense')) checked @endif
                       disabled
                >
                <label class="form-label"
                       for="merchant_expense"><strong>{{__('Commission at your expense')}}</strong></label>
            </div>
        </div>
        <div class="mb-3">
            <label for="amount" class="form-label">{{__('Charge amount')}}</label>
            <div class="input-group">
                <span class="input-group-text" id="basic-addon1">$</span>
                <input type="text" class="form-control @error('amount') is-invalid @enderror"
                       value="{{$amount2merchant}}" disabled
                >
            </div>
            @error('amount')
            <div class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </div>
            @enderror
        </div>
        <div class="mb-3">
            <label for="amount" class="form-label">{{__('Amount due')}}</label>
            <div class="input-group">
                <span class="input-group-text" id="basic-addon1">$</span>
                <input type="text" class="form-control"
                       value="{{$amount2pay}}" disabled
                >
            </div>
        </div>
        <div class="mb-3">
            <label for="currency" class="form-label">{{__('Select currency')}}</label>
            <select class="form-select @error('currency') is-invalid @enderror" id="currency" name="currency" required>
                @foreach($currencies as $currency)
                    <option value="{{$currency}}">{{$currency}}</option>
                @endforeach
            </select>
            @error('currency')
            <div class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </div>
            @enderror
        </div>
        <div class="mb-3">
            <label for="payment_system_id" class="form-label">{{__('Select payment system')}}</label>
            <select class="form-select @error('payment_system_id') is-invalid @enderror" id="payment_system_id" name="payment_system_id" required>
                @foreach($buy_coins[$currencies[0]] as $item)
                    <option value="{{$item['ad_id']}}">{{$item['bank_name']}} - {{price_format($item['gps_temp_amount'])}}</option>
                @endforeach
            </select>
            @error('payment_system_id')
            <div class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </div>
            @enderror
            <div class="form-text">
                {{__('Please choose from the list only the bank whose card you have and to which you plan to receive funds.')}}
            </div>
        </div>
        <div class="mb-3">
            <label for="message" class="form-label">{{__('Enter bank card number')}}</label>
            <input type="text" class="form-control @error('message') is-invalid @enderror" id="message" name="message" placeholder="{{__('Enter bank card number')}}"
                   value="{{old('message')}}" required
            >
            @error('message')
            <div class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </div>
            @enderror
        </div>
        <div class="mb-3 float-end">
            <button type="submit" class="btn btn-grow" onclick="this.disabled=true;this.form.submit();">
                {{__('Next')}}
                <i class="bi bi-arrow-right"></i>
            </button>
        </div>
        <div class="mb-3 float-start">
            <a href="{{route('withdrawal.create')}}">
                <i class="bi bi-arrow-left"></i>
                {{__('Back')}}
            </a>
        </div>
    </form>
@endsection
@section('script-bottom')
    @if (isset($buy_coins))
        <script>
            var FiatsArray = [];
            @foreach ($buy_coins as $k => $v)
                var estimates_arr = [];
                @foreach ($v as $item)
                    estimates_arr[estimates_arr.length] = {
                    payment_system_id: '{{ $item['ad_id'] }}',
                    payment_system_name: '{{ $item['bank_name'] }}',
                    fiat_amount: '{{ price_format($item['gps_temp_amount']) }}'
                };
                @endforeach
                FiatsArray['{{ $k }}'] = estimates_arr;
            @endforeach
        </script>
    @endif
@endsection
