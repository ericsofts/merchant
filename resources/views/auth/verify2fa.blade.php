@extends('layouts.guest')
@section('body-class', 'd-flex h-100 text-white bg-black')
@section('html-class', 'h-100')

@section('content')
    <main class="form-1">
        <div class="text-center">
            <img class="mb-4" src="{{ asset('images/logo.png') }}">
            <p class="mb-3 fw-normal">{{ __('Please enter the code from the mobile Two-factor Authentication application (2FA)') }}</p>
        </div>
        <form method="POST" action="{{ route('login.verify2fa') }}">
            @csrf
            <div class="mb-3">
                <input type="text" class="form-control @error('code') is-invalid @enderror" id="code" name="code" placeholder="{{__('Code')}}"
                       value="{{old('code')}}" required
                >
                @error('code')
                <div class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </div>
                @enderror
            </div>
            <button class="w-100 btn btn-lg btn-secondary fw-bold border-white bg-white" type="submit">{{__('Confirm')}}</button>
        </form>
    </main>
@endsection
