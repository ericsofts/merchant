@extends('layouts.guest')
@section('body-class', 'd-flex h-100 text-center text-white bg-black')
@section('html-class', 'h-100')

@section('content')
    <main class="form-signin">
        <form method="POST" action="{{ route('login') }}">
            @csrf
            <img class="mb-4" src="{{ asset('images/logo.png') }}">
            <h1 class="h3 mb-3 fw-normal">{{__('Please sign in')}}</h1>
            @if($status = session()->pull('status'))
                <div class="alert alert-grow-w alert-dismissible fade show" role="alert">
                    {{ $status }}
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @elseif($errors->any())
                <div class="alert alert-grow-w alert-dismissible fade show" role="alert">
                    {{ __('Whoops! Something went wrong.') }}
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif
            <label for="inputEmail" class="visually-hidden">{{__('Email address')}}</label>
            <input type="email" id="email" class="form-control"
                   name="email"
                   placeholder="{{__('Email address')}}" required autofocus
                   value="{{ old('email') }}"
            >
            <label for="inputPassword" class="visually-hidden">{{__('Password')}}</label>
            <input type="password" id="password" class="form-control"
                   name="password"
                   placeholder="{{__('Password')}}" required
                   value="{{ old('password') }}"
            >
            @error('email')
            <div class="is-invalid"></div>
            <div class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </div>
            @enderror
            <div class="checkbox mb-3">
                <label>
                    <input name="remember"
                           id="remember"
                           type="checkbox"
                        {{ old('remember') ? 'checked' : '' }}> {{__('Remember me')}}
                </label>
            </div>
            <button class="w-100 btn btn-lg btn-secondary fw-bold border-white bg-white" type="submit">{{__('Sign in')}}</button>
            <ul class="list-inline mt-1">
                <li class="list-inline-item">
                </li>
                @if (Route::has('password.request'))
                    <li class="list-inline-item">
                        <a class="text-muted" href="{{ route('password.request') }}">
                            {{ __('Forgot password?') }}
                        </a>
                    </li>
                @endif
            </ul>
            <p class="mt-5 mb-3 text-muted">&copy; 2020–{{date('Y')}}</p>
        </form>
    </main>
@endsection
