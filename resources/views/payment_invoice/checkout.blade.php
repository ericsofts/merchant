@extends('layouts.app')
@section('content')
    <nav aria-label="breadcrumb" class="pt-3">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">{{__('Dashboard')}}</a></li>
            <li class="breadcrumb-item"><a href="{{route('payment_invoice.add')}}">{{__('Account Top-Up')}}</a></li>
            <li class="breadcrumb-item active">{{__('Bank Card')}}</li>
        </ol>
    </nav>
    <h2 class="font-semibold text-xl text-gray-800 leading-tight">
        {{ __('Account Top-Up') }}. {{ __('Bank Card') }}
    </h2>
    <form method="POST" action="{{ route('payment_invoice.confirm') }}">
        @csrf
        <input type="hidden" name="amount" value="{{ $amount }}">
        @if ($errors->any())
            <div class="alert alert-grow alert-dismissible fade show" role="alert">
                {{ __('Whoops! Something went wrong.') }}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        @endif
        @if($status = session('status'))
            <div class="alert alert-grow alert-dismissible fade show" role="alert">
                {{ $status }}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        @endif
        <div class="mb-3">
            <label for="amount" class="form-label">{{__('Amount')}}</label>
            <div class="input-group">
                <span class="input-group-text" id="basic-addon1">$</span>
                <input type="text" class="form-control @error('amount') is-invalid @enderror"
                       value="{{$amount}}" disabled
                >
            </div>
            @error('amount')
            <div class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </div>
            @enderror
        </div>
        <div class="mb-3">
            <span class="form-label">{{__('To be credited to the account:')}}</span>
            <span class="form-label">{{$fmt->formatCurrency(price_format($merchant_amount), "USD")}}</span>
        </div>
        <div class="mb-3">
            <label for="currency" class="form-label">{{__('Select currency')}}</label>
            <select class="form-select @error('currency') is-invalid @enderror" id="currency" name="currency" required>
                @foreach($currencies as $currency)
                    <option value="{{$currency}}">{{$currency}}</option>
                @endforeach
            </select>
            @error('currency')
            <div class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </div>
            @enderror
        </div>
        <div class="mb-3">
            <label for="payment_system_id" class="form-label">{{__('Select payment system')}}</label>
            <select class="form-select @error('payment_system_id') is-invalid @enderror" id="payment_system_id" name="payment_system_id" required>
                @foreach($sell_coins[$currencies[0]] as $item)
                    <option value="{{$item['ad_id']}}">{{$item['bank_name']}} - {{price_format($item['gps_temp_amount'])}}</option>
                @endforeach
            </select>
            @error('payment_system_id')
            <div class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </div>
            @enderror
            <div id="passwordHelpBlock" class="form-text">
                {{__('Please choose from the list only the bank whose card you have and from which you plan to pay.')}}
            </div>
        </div>
        <div class="mb-3 float-end">
            <button type="submit" class="btn btn-grow" onclick="this.disabled=true;this.form.submit();">
                {{__('Next')}}
                <i class="bi bi-arrow-right"></i>
            </button>
        </div>
        <div class="mb-3 float-start">
            <a href="{{route('payment_invoice.add')}}">
                <i class="bi bi-arrow-left"></i>
                {{__('Back')}}
            </a>
        </div>
    </form>
@endsection
@section('script-bottom')
    @if (isset($sell_coins))
        <script>
            var FiatsArray = [];
            @foreach ($sell_coins as $k => $v)
                var estimates_arr = [];
                @foreach ($v as $item)
                    estimates_arr[estimates_arr.length] = {
                    payment_system_id: '{{ $item['ad_id'] }}',
                    payment_system_name: '{{ $item['bank_name'] }}',
                    fiat_amount: '{{ price_format($item['gps_temp_amount']) }}'
                };
                @endforeach
                FiatsArray['{{ $k }}'] = estimates_arr;
            @endforeach
        </script>
    @endif
@endsection
