@extends('layouts.app')
@section('content')
    <nav aria-label="breadcrumb" class="pt-3">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">{{__('Dashboard')}}</a></li>
            <li class="breadcrumb-item active">{{__('Account Top-Up')}}</li>
        </ol>
    </nav>
    <h2 class="font-semibold text-xl text-gray-800 leading-tight">
        {{ __('Account Top-Up') }}
    </h2>
    <form method="POST" action="{{ route('payment_invoice.checkout') }}">
        @csrf
        @if ($errors->any())
            <div class="alert alert-grow alert-dismissible fade show" role="alert">
                {{ __('Whoops! Something went wrong.') }}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        @endif
        @if($status = session('status'))
            <div class="alert alert-grow alert-dismissible fade show" role="alert">
                {{ $status }}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        @endif
        <div class="mb-3">
            <label for="amount" class="form-label">{{__('Amount')}}</label>
            <div class="input-group">
                <span class="input-group-text">$</span>
                <input type="text" class="form-control @error('amount') is-invalid @enderror" id="amount" name="amount" placeholder="{{__('Amount')}}"
                       value="{{old('amount')}}" required autofocus
                >
                @error('amount')
                <div class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </div>
                @enderror
            </div>
        </div>
        <input name="payment_method" value="bank_card" hidden/>
{{--        <div class="mb-3">--}}
{{--            <label for="payment_method" class="form-label">{{__('Payment method')}}</label>--}}
{{--            <select class="form-select @error('payment_method') is-invalid @enderror" id="payment_method" name="payment_method" required>--}}
{{--                <option value=""></option>--}}
{{--                @if(in_array('bank_card', $ps))--}}
{{--                    <option value="bank_card">{{__('Bank Card')}}</option>--}}
{{--                @endif--}}
{{--            </select>--}}
{{--            @error('payment_method')--}}
{{--            <div class="invalid-feedback" role="alert">--}}
{{--                <strong>{{ $message }}</strong>--}}
{{--            </div>--}}
{{--            @enderror--}}
{{--        </div>--}}
        <div class="mb-3 float-end">
            <button type="submit" class="btn btn-grow" onclick="this.disabled=true;this.form.submit();">
                {{__('Next')}}
                <i class="bi bi-arrow-right"></i>
            </button>
        </div>
    </form>
@endsection
