@extends('layouts.app')
@section('content')
    <nav aria-label="breadcrumb" class="pt-3">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">{{__('Dashboard')}}</a></li>
            <li class="breadcrumb-item active">{{__('Invoice history')}}</li>
        </ol>
    </nav>
    <div class="page-header">
        <h1>
            {{ __('Invoice history') }}
        </h1>
    </div>
    @if($invoices)
        <div class="row">
            <div class="col">
                <dl class="row">
                    <dt class="col-sm-4">{{__('Total amount')}}:</dt>
                    <dd class="col-sm-8">{{$fmt->formatCurrency(price_format($total->total_amount), "USD")}}</dd>
                    <dt class="col-sm-4">{{__('Total amount to pay')}}:</dt>
                    <dd class="col-sm-8">{{$fmt->formatCurrency(price_format($total->total_amount2pay), "USD")}}</dd>
                </dl>
            </div>
            <div class="col">
                <dl class="row">
                    <dt class="col-sm-4">{{__('Total page amount')}}:</dt>
                    <dd class="col-sm-8">{{$fmt->formatCurrency(price_format($total_page['amount']), "USD")}}</dd>
                    <dt class="col-sm-4">{{__('Total page amount to pay')}}:</dt>
                    <dd class="col-sm-8">{{$fmt->formatCurrency(price_format($total_page['amount2pay']), "USD")}}</dd>
                </dl>
            </div>
        </div>
        <form method="GET" action="{{ route('payment_invoice.index') }}" class="row g-3 mb-3">
            <div class="col-md-3">
                <div class="row">
                    <label class="col-sm-2 col-form-label">{{__('From')}}:</label>
                    <div class="col-sm-10">
                        <div class="input-group">
                            <span class="input-group-text"><i class="bi bi-calendar"></i></span>
                            <input type="text" class="form-control datepicker" name="from_date" placeholder="{{__('From')}}" value="{{$from_date}}" autocomplete="off">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="row">
                    <label class="col-sm-2 col-form-label">{{__('To')}}:</label>
                    <div class="col-sm-10">
                        <div class="input-group">
                            <span class="input-group-text"><i class="bi bi-calendar"></i></span>
                            <input type="text" class="form-control datepicker" name="to_date" placeholder="{{__('To')}}" value="{{$to_date}}" autocomplete="off">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="row">
                    <label class="col-sm-3 col-form-label">{{__('Status')}}: </label>
                    <div class="col-sm-9">
                        <select class="form-select" name="status">
                            <option value="">{{__('Choose')}}</option>
                            <option value="1" @if($status === "1") selected @endif>{{__('Paid')}}</option>
                            <option value="0" @if($status === "0") selected @endif>{{__('Created')}}</option>
                            <option value=99 @if($status === "99") selected @endif>{{__('Canceled')}}</option>
                            <option value=98 @if($status === "98") selected @endif>{{__('Expired')}}</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <button type="submit" class="btn btn-grow">{{__('Submit')}}</button>
                <button type="submit" name="btnExport" class="btn btn-grow">{{__('Export')}}</button>
            </div>
        </form>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">{{__('Invoice ID')}}</th>
                <th scope="col">{{__('Order ID')}}</th>
                <th scope="col">{{__('Date')}}</th>
                <th scope="col">{{__('Amount')}}</th>
                <th scope="col">{{__('Fiat amount')}}</th>
                <th scope="col">{{__('Amount to pay')}}</th>
                <th scope="col">{{__('Card Number')}}</th>
                <th scope="col">{{__('Status')}}</th>
            </tr>
            </thead>
            <tbody>
            @foreach($invoices as $i => $item)
                <tr>
                    <th scope="row">{{$i+1}}</th>
                    <td>{{$item->id}}</td>
                    <td>{{$item->merchant_order_id}}</td>
                    <td>{{datetimeFormat($item->created_at)}}</td>
                    <td>{{$fmt->formatCurrency(price_format($item->amount), "USD")}}</td>
                    <td>
                        @if($item->input_currency != 'USD')
                            {{round($item->input_amount_value, 2)}}
                            {{strtoupper($item->input_currency)}}
                        @endif
                    </td>
                    <td>
                        @if($item->status == 1)
                            {{$fmt->formatCurrency(price_format($item->amount2pay), "USD")}}
                        @endif
                    </td>
                    <td>{{mask_credit_card($item->account_info)}}</td>
                    @if($item->isLinked(auth()->user()))
                        <td><a href="{{$item->getLink()}}">{{payment_invoice_status($item->status)}}</a></td>
                    @else
                        <td>{{payment_invoice_status($item->status)}}</td>
                    @endif
                </tr>
            @endforeach
            </tbody>
        </table>
        <div class="float-end">{{ $invoices->links() }}</div>
    @endif
@endsection
