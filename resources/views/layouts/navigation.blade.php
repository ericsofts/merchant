<nav id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block bg-light sidebar collapse sidebar-menu">
    <div class="position-sticky pt-3">
        <ul class="nav flex-column">
            <li class="nav-item">
                <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mb-1 text-muted">
                    {{ Auth::user()->name }}
                </h6>
            </li>
            <li class="nav-item">
                <a class="nav-link @if(request()->routeIs('dashboard')) active @endif" href="{{route('dashboard')}}">
                    <i class="bi bi-house"></i>
                    {{__('Dashboard')}}
                </a>
            </li>
            @if(isAvalableInvoiceAdd())
            <li class="nav-item">
                <a class="nav-link @if(request()->routeIs('payment_invoice.add')) active @endif" href="{{route('payment_invoice.add')}}">
                    <i class="bi bi-card-list"></i>
                    {{__('Making a payment')}}
                </a>
            </li>
            @endif
            <li class="nav-item">
                <a class="nav-link @if(request()->routeIs('payment_invoice.index')) active @endif" href="{{route('payment_invoice.index')}}">
                    <i class="bi bi-card-list"></i>
                    {{__('Invoice history')}}
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link @if(request()->routeIs('withdrawal.create')) active @endif" href="{{route('withdrawal.create')}}">
                    <i class="bi bi-card-list"></i>
                    {{__('Withdraw')}}
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link @if(request()->routeIs('withdrawal.index')) active @endif" href="{{route('withdrawal.index')}}">
                    <i class="bi bi-card-list"></i>
                    {{__('Withdrawal history')}}
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link @if(request()->routeIs('transactions.all_invoices')) active @endif" href="{{route('transactions.all_invoices')}}">
                    <i class="bi bi-card-list"></i>
                    {{__('Transactions')}}
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link @if(request()->routeIs('merchant.available_ads')) active @endif" href="{{route('merchant.available_ads')}}">
                    <i class="bi bi-card-list"></i>
                    {{__('Available Ads')}}
                </a>
            </li>
        </ul>

        <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
            <span>{{__('Settings')}}</span>
            <i class="bi bi-gear"></i>
        </h6>
        <ul class="nav flex-column mb-2">
            <li class="nav-item">
                <a class="nav-link @if(request()->routeIs('merchant.profile')) active @endif" href="{{route('merchant.profile')}}">
                    <i class="bi bi-person"></i>
                    {{__('Profile')}}
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link @if(request()->routeIs('2fa.index')) active @endif" href="{{route('2fa.index')}}">
                    <i class="bi bi-shield-lock"></i>
                    {{__('Security options')}}
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link @if(request()->routeIs('merchant.whitelabel')) active @endif" href="{{route('merchant.whitelabel')}}">
                    <i class="bi bi-shield-lock"></i>
                    {{__('WhiteLable')}}
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link @if(request()->routeIs('merchant.ip_security')) active @endif" href="{{route('merchant.ip_security')}}">
                    <i class="bi bi-shield-lock"></i>
                    {{__('IP Security')}}
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link @if(request()->routeIs('merchant.settings.account')) active @endif" href="{{route('merchant.settings.account')}}">
                    <i class="bi bi-coin"></i>
                    {{__('Account')}}
                </a>
            </li>
        </ul>
    </div>
</nav>
