<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" class="@yield('html-class')">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>@yield('title')</title>

        <!-- Fonts -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">

        <!-- Styles -->
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">

        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}" defer></script>


        @yield('script-head')
        @yield('css-whitelabel')
    </head>
    <body class="flex-column @yield('body-class')">
    <header class="navbar flex-md-nowrap p-0 shadow">
        <span class="col-md-3 me-0 px-3">@yield('title')</span>
        <button class="navbar-toggler position-absolute d-md-none collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#sidebarMenu" aria-controls="sidebarMenu" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="d-flex justify-content-end">
            <ul class="nav px-3 nav-locales">
                @foreach (config()->get('app.locales') as $locale)
                    <li class="nav-item{{ $locale == app()->getLocale() ? " active" : "" }}">
                        <a class="nav-link"
                           href="/setlocale/{{ $locale }}"><img class="img-flag"
                                                                src="{{ asset("images/flags/$locale.png") }}"></a>
                    </li>
                @endforeach
            </ul>
        </div>
    </header>
    @yield('content')

    @yield('script-bottom')
    </body>
</html>
