@extends('layouts.app')
@section('content')
    <nav aria-label="breadcrumb" class="pt-3">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">{{__('Dashboard')}}</a></li>
            <li class="breadcrumb-item active">{{__('Transactions')}}</li>
        </ol>
    </nav>
    <div class="page-header">
        <h1>
            {{ __('Transactions') }}
        </h1>
    </div>
    @if($invoices)
        <form method="GET" action="{{ route('transactions.all_invoices') }}" class="row g-3 mb-3">
            <div class="row">
                <div class="col">
                    <div class="row">
                        <label class="col-sm-2 col-form-label">{{__('From')}}:</label>
                        <div class="col-sm-10">
                            <div class="input-group">
                                <span class="input-group-text"><i class="bi bi-calendar"></i></span>
                                <input type="text" class="form-control datepicker" name="from_date" placeholder="{{__('From')}}"
                                       value="{{$from_date}}" autocomplete="off">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="row">
                        <label class="col-sm-2 col-form-label">{{__('To')}}:</label>
                        <div class="col-sm-10">
                            <div class="input-group">
                                <span class="input-group-text"><i class="bi bi-calendar"></i></span>
                                <input type="text" class="form-control datepicker" name="to_date" placeholder="{{__('To')}}"
                                       value="{{$to_date}}" autocomplete="off">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="row">
                        <label class="col-sm-2 col-form-label">{{__('Status')}}:</label>
                        <div class="col-sm-10">
                            <select class="form-select" name="status">
                                <option value="">{{__('Choose')}}</option>
                                <option value="1" @if($status === "1") selected @endif>{{__('Paid')}}</option>
                                <option value="0" @if($status === "0") selected @endif>{{__('In progress')}}</option>
                                <option value="99" @if($status === "99") selected @endif>{{__('Canceled')}}</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mt-2">
                <div class="col">
                    <div class="row">
                        <label class="col-sm-4 col-form-label">{{__('Type')}}:</label>
                        <div class="col-sm-8">
                            <select class="form-select select2" name="invoice_types[]" multiple="multiple">
                                <option value="">{{__('Choose')}}</option>
                                <option value="0" @if(in_array(0, request('invoice_types', []))) selected @endif>{{invoice_type(0)}}</option>
                                <option value="1" @if(in_array(1, request('invoice_types', []))) selected @endif>{{invoice_type(1)}}</option>
                                <option value="2" @if(in_array(2, request('invoice_types', []))) selected @endif>{{invoice_type(2)}}</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <button type="submit" class="btn btn-grow">{{__('Submit')}}</button>
                    <button type="submit" name="btnExport" class="btn btn-grow">{{__('Export')}}</button>
                </div>
            </div>
        </form>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">{{__('Invoice ID')}}</th>
                <th scope="col">{{__('Order ID')}}</th>
                <th scope="col">{{__('Type')}}</th>
                <th scope="col">{{__('Date')}}</th>
                <th scope="col">{{__('Volume')}}</th>
                <th scope="col">{{__('Amount')}}</th>
                <th scope="col">{{__('Amount to pay')}}</th>
                <th scope="col">{{__('Comment')}}</th>
                <th scope="col">{{__('Status')}}</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td colspan="4" class="fw-bold text-end">{{__('Total')}}:</td>
                <td class="fw-bold">{{price_format($total['volume'])}}</td>
                <td class="fw-bold">{{price_format($total['amount'])}}</td>
                <td class="fw-bold">{{price_format($total['amount2pay'])}}</td>
                <td></td>
                <td></td>
            </tr>
            @foreach($invoices as $i => $item)
                <tr>
                    <td>{{$item->id}}</td>
                    <td>{{$item->merchant_order_id}}</td>
                    <td>{{invoice_type($item->invoice_type)}}</td>
                    <td>{{datetimeFormat($item->created_at)}}</td>
                    <td>{{price_format($item->amount)}}</td>
                    <td>@if($item->invoice_type==1)-@endif{{price_format($item->amount)}}</td>
                    <td>@if($item->invoice_type==1)-@endif{{price_format($item->amount2pay)}}</td>
                    <td>{!! nl2br($item->comment) !!}</td>
                    <td>{{invoice_status($item->status)}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div class="float-end">{{ $invoices->links() }}</div>
    @endif
@endsection
